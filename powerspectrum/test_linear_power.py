#!/usr/bin/python
"""Test the linear power spectrum calculation using CosmoloPy."""

import numpy as np
import cosmolopy as cp
import pylab as P

# Define cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.72,
		 'n' : 1.0,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}

cosmo2 = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.72,
		 'n' : 1.0,
		 'sigma_8' : 0.79,
		 'baryonic_effects': False
		} 

# Calculate linear power spectrum, P(k)
z = 0.0
k = np.logspace(-4., 1., 1000)
ps = cp.perturbation.power_spectrum(k, z, **cosmo)
ps2 = cp.perturbation.power_spectrum(k, z, **cosmo2)


# Plot results
P.subplot(111)
P.plot(k, ps2, 'r-')
P.plot(k, ps, 'b-')
P.xlabel("k")
P.ylabel("P(k)")
P.xscale('log')
P.yscale('log')

P.show()
