#!/usr/bin/python
"""Test number counts."""

import numpy as np
import cosmolopy as cp
import scipy.integrate
import pylab as P
from cosmology import Cosmology

# Use LCDM cosmology from Table 1 of Jenkins et al. (2000).
cosmo = {
		 'omega_M_0' : 0.30,
		 'omega_lambda_0' : 0.70,
		 'omega_b_0' : 0.040,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 1.0,
		 'sigma_8' : 0.9,
		 'baryonic_effects': True
	}

C = Cosmology(cosmo)

# Plot theory curve
#M = np.logspace(11.5, 16., 1e2)
M = np.logspace(np.log10(5e13), 17., 2e2)
dn_dlogM = np.array( [C.dn_dlnM(mm) for mm in M] )

# Plot data from Jenkins et al., Fig. 1
dat = np.genfromtxt("JenkinsFig1.png.dat", delimiter="  ").T


# Test number count with Hubble volume (using Table 2 from Evrard et al., 
# arXiv:astro-ph/0110246v2)
# From Table 2: M200 > 1e14 Msun.
volH = 7.6e10 # Mpc^3
dlogM = np.log(M)
print "\n\tNtot(M>M*):", scipy.integrate.simps(dn_dlogM, dlogM) * volH
# Results given are correct order, e.g. ~600 for M > 10^15, ~440k for M > 10^14
# (but mind factors of h)

h = 0.7
jenkinsM = h * 10**(dat[0])
jenkinsdn = h**3. * 10**(dat[1])

P.subplot(111)
b = 1.5
P.plot(M, b*dn_dlogM, 'r-', label="My code (bias="+str(b)+")")
P.plot(jenkinsM, jenkinsdn, 'bx', label="Jenkins et al. (Fig. 1)")
P.xscale('log')
P.yscale('log')
P.xlabel("M")
P.ylabel("dn/dlogM")
P.grid(True)
P.legend(loc="lower left")
P.show()
