#!/usr/bin/python
"""Test code to calculate variance of matter field."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import realise_matterpower as R
np.seterr(all='ignore') # Because of some annoying div/0 warnings.

# Define default cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}

# Get power spectrum
#k = np.linspace(1e-6, 1e1, 1e4)
k = np.logspace(-6, 1, 1e4)
pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)

def window(k, RR):
	"""Window function, which filters the FT. Used in sigma8 calculation."""
	# See Weinberg, Eq.8.1.46
	x = k*RR
	f = (3. / x**3.) * ( np.sin(x) - x*np.cos(x) )
	return f

def sigma8(k, pk):
	"""Calculate sigma8 for a realisation of the power spectrum."""
	RR = 8. / 0.7 # 8 h^-1 Mpc # FIXME: h is hard-coded here.
	y = pk * (k * window(k, RR))**2. # Integrand samples
	#y = np.nan_to_num(y) # Remove NaNs
	s8_sq = scipy.integrate.simps(y, k) / (2. * np.pi**2.)
	return np.sqrt(s8_sq)


# Realise
NSIDE = 64
SCALE = 1e4
x, kk, delta_x, delta_k, ps, norm = R.realise(scale=SCALE, nsamp=NSIDE)
N = x.size
L = x[-1] - x[0]
exp_pk = np.abs(delta_k)**2. / N**3.
exp_pk2 = np.abs(delta_k)**2. / L**3.

print "Volume:\t", (x[-1] - x[0])**3., (x[-1] - x[0])**(1.)
print "N^3:\t", N**3.


s1 = sigma8(k, pk)
s2 = cp.perturbation.sigma_r(r=8.0/0.7, z=0.0, **cosmo)[0]
s3 = R.discrete_realisation_sigma8(kk, ps)
s4 = R.discrete_realisation_sigma8(kk, exp_pk)
s5 = R.discrete_realisation_sigma8(kk, exp_pk2)
print "Calculated sigma8:\t", s1
print "Cosmolopy sigma8:\t", s2
print "Discrete sigma8:\t", s3 / N**3.
print "Disc. sigma8 real.:\t", s4 / N**3.
print "Disc. s8 vol. real.:\t", s5


