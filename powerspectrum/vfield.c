/*
Reconstruct a velocity field by calculating the peculiar acceleration 
vector over a box of density contrasts. See Peebles Chs. 14 and 15 
(esp. Eqs. 14.2 and 14.8) for an expression for vec{g}(x).
Do this in pure C, rather than C++, for speed.
*/

/// NOTE: Thisn is obsolete. You can achieve the same thing with a simple 
/// Fourier transform!

#include "math.h"
#include "stdio.h"
#include "stdlib.h"

double* load_density_field(char* fname, int* N){
	/// Load 3D density field from file
	
	// Open file and get file size
	FILE *f; int fsize;
	f = fopen(fname, "rb");
	fseek(f, 0L, SEEK_END);
	fsize = ftell(f);
	rewind(f);
	
	// Calculate no. of elements in array and allocate memory
	*N = fsize / sizeof(double);
	double *delta = (double*)malloc(fsize);
	
	// Load all data into array and return pointer
	fread(delta, sizeof(double), *N, f);
	fclose(f);	
	return delta;
}

int idx1(int i, int j, int k, int* Nside){
	/// Get the 1D (flattened) array index, given 3D indices.
	return *Nside * (*Nside * i + j) + k;
}

void idx3(int idx, int* i, int* j, int* k, int* Nside){
	/// Get 3D array indices, given 1D (flattened) indices.
	int N2 = (*Nside * *Nside);
	*i = idx / N2;
	*j = (idx % N2) / *Nside;
	*k = (idx - *i * N2 - *j * *Nside);
}

double dist(int i0, int j0, int k0, int i, int j, int k, int* Nside){
	/// Distance between cells
	return sqrt( (i0-i)*(i0-i) + (j0-j)*(j0-j) + (k0-k)*(k0-k) );
}

void pec_accel(double* delta, double* gx, double* gy, double* gz, int* Nside){
	/// Calculate the (unscaled) peculiar acceleration for the 
	/// density field in a periodic box. (See Peebles Eq. 14.8.)
	//*(delta+idx(0, 0, i, Nside))
	double r;
	int N = (*Nside)*(*Nside)*(*Nside);
	
	int a; int i0, j0, k0; int i, j, k;
	// Loop through each grid cell
	for(a=0; a<N; a++){
		idx3(a, &i0, &j0, &k0, Nside); // Get origin
		
		idx1()
		for
		*(gx+a) += ;
		*(gy+a) += ;
		*(gz+a) += ;
		
		//g(x) = integral dx'^3 delta(x', t) (vec{x} - vec{x'}) / |x - x'|^3
	}
}

int main(int argc, char* argv[]){
	// Get filename for density field box
	char* fname; int Nside;
	if(argc > 2){
		fname = argv[1];
		Nside = atoi(argv[2]);
	}else{ 
		printf("ERROR: No filename/Nside given. Exiting...\n"); exit(1);
	}
	printf("\tFilename: %s\n", fname);
	printf("\tNside: %d\n", Nside);
	
	// Load the density field
	int N;
	double *delta = load_density_field(fname, &N);
	printf("\tArray size: %d\n", N);
	
	// Test array reconstruction
	int idx = idx1(3, 54, 8, &Nside);
	int ii, jj, kk;
	idx3(idx, &ii, &jj, &kk, &Nside);
	printf("Indices (3, 54, 8)\n");
	printf("idx1: %d\n", idx);
	printf("idx3: %d, %d, %d\n", ii, jj, kk);
	
	// Allocate arrays for (x,y,z)-components of g
	double *gx = (double*)malloc(sizeof(double)*N);
	double *gy = (double*)malloc(sizeof(double)*N);
	double *gz = (double*)malloc(sizeof(double)*N);
	
	// Calculate peculiar accelerations
	pec_accel(delta, gx, gy, gz, &Nside);
	
	// Free arrays and return
	free(delta); free(gx); free(gy); free(gz);
	return 0;
}
