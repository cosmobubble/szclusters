#!/usr/bin/python
"""Manipulate the matter power spectrum."""

import numpy as np
import scipy.integrate
import pylab as P
import realise_matterpower as R
import scipy.fftpack as fft
import cosmolopy as cp
#np.random.seed(12)
np.random.seed(18)
np.seterr(all='ignore')

# Get realisation of power spectrum
x, kk, delta_x, delta_k, ps, norm = R.realise(scale=1e4, nsamp=32)

# Get binned power spectrum for realisation
k, psexp = R.realisation_ps(x, delta_x, bins=40)

# Get P(k) samples s.t. k is monotonically-increasing (ready for integration)
k_mono, idxs = np.unique(kk.flatten(), return_index=True)
pk = (ps.flatten())[idxs]



################################################################################
# Report on statistics
################################################################################

# Sigma 8
s8_exp = R.realisation_sigma8(k, psexp)
s30_exp = R.realisation_sigmaR(k, psexp, 30.)
s8_th, _k, _y = R.calc_sigma8()
s30_th = R.calc_sigmaR(30.)

# Correction factor, to get the normalisation right.
factor = (0.79/s8_exp)
factor2 = (s30_th/s30_exp)
delta_x *= factor

print "\n\nPower spectrum normalisation"
print "\nMean:", np.mean(delta_x)
print "Max.:", np.max(delta_x)
print "Min.:", np.min(delta_x)
print "Var(delta_x):", np.std(delta_x)**2.
print "sigma400:", R.calc_sigmaR(400.)
print "\nsigma8(exp):\t", s8_exp
print "sigma8(th):\t", s8_th
print "Norm. Fac.:\t", norm
print "Ratio:", (s8_th/s8_exp)**2.
print "Factor:", factor, factor2



################################################################################
# Plot results
################################################################################

"""
P.subplot(111)
P.imshow(delta_x[0], interpolation='bilinear', extent=(min(x), max(x), min(x), max(x)))
"""

P.subplot(211)
P.plot(k_mono, pk, 'b-')
P.plot(k[1:], (factor**2. * psexp)[1:], 'g.')
P.plot(k[1:], (factor2**2. * psexp)[1:], 'r.')
P.xscale('log')
P.yscale('log')
P.axvline(2.*np.pi*0.7/8.0, ls='solid', color='k')
#P.ylim((5e-2, 1.1))
#P.xlim((4e-3, 2e-1))


# Correlation function
def corr(th_r, th_k, th_pk):
	th_corr = []
	for rr in th_r:
		fk = th_k * th_pk * np.sin(th_k*rr) / (rr) # Integrand
		_corr = scipy.integrate.simps(fk, th_k) / (2.*np.pi**2.)
		th_corr.append(_corr)
	return np.array(th_corr)

th_r = np.linspace(1., 200./0.7, 60)
corr_exp = corr(th_r, k, factor**2. * psexp)

# Theory
##corr_th = corr(th_r/0.7, k_mono, pk)
_kk = np.logspace(-3, 1.5, 1e4)
th_pk = cp.perturbation.power_spectrum(_kk, z=0.0, **R.DEFAULT_COSMOLOGY)	
corr_th = corr(th_r, _kk, th_pk)

"""
P.subplot(212)
P.plot(th_r*0.7, corr_exp, 'g+')
P.plot(th_r*0.7, corr_th, 'b-')
P.ylim((-0.005, 0.02))
P.xlim((40., 180.))
"""


# Integrand for sigma8 calculation
_k = np.logspace(-3, 1, 5e3)
pk = cp.perturbation.power_spectrum(_k, z=0.0, **R.DEFAULT_COSMOLOGY)
y = pk * _k**2. * R.window(_k, 8.0/0.7) # Integrand samples
y2 = pk * _k**2. * R.window(_k, 30.0/0.7) # Integrand samples
y = np.nan_to_num(y) # Remove NaNs
y2 = np.nan_to_num(y2) # Remove NaNs

P.subplot(212)
P.plot(_k, y, 'g-') # Integrand
P.plot(_k, y2, 'r-') # Integrand, 30Mpc
P.axvline(np.max(k), ls='dotted', color='b') # Max. k
P.axvline(np.min(k), ls='dotted', color='b') # Min. k
P.xscale('log')





# TESTING
# Get sigma8 for initial gaussian field
x = np.linspace(-2e3, 2e3, 32) # 3D grid coords, in Mpc
delta_xi = np.random.randn(x.size, x.size, x.size)
delta_ki = fft.fftn(delta_xi) # 3D discrete FFT
k = R.fft_sample_spacing(x).flatten()
pk = (np.abs(delta_ki)**2.).flatten()

# Bin the resulting power spectrum
# Get bin masks
bins = 20
edges = np.linspace(0.0, np.max(k), bins)
dk = edges[1] - edges[0]
centroids = [0.5 * (edges[i+1] + edges[i]) for i in range(edges.size - 1)]
m = [np.where((k >= edges[i]) & (k < edges[i+1]), True, False) for i in range(edges.size - 1)]
# Get averaged value in each bin
hist_pk = np.array(  [np.sum(pk[mm]) / np.sum(mm) for mm in m]  )
hist_k = np.array(centroids)

y = hist_pk * hist_k**2. * R.window(hist_k, 8.0/0.7) # Integrand samples
y = np.nan_to_num(y) # Remove NaNs


s8_sq = scipy.integrate.simps(y, hist_k) / (2. * np.pi**2.)
print "Gaussian Random field, sigma8:", np.sqrt(s8_sq)

"""
P.subplot(212)
P.plot(hist_k, y, 'g-')
P.xscale('log')
P.yscale('log')
P.ylabel("P(k)|Gaussian random")
"""

"""
P.subplot(122)
#P.hist(delta_x.flatten(), bins=100)
#P.plot(k_mono, y, 'b-')
#P.plot(_k, _y)

xx = np.logspace(-4, 3, 30)
yy = map(lambda x: R.calc_sigmaR(x), xx)
P.plot(xx, yy, 'g+')
P.yscale('log')
P.xscale('log')
"""

P.show()
