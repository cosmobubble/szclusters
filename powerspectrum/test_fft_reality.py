#!/usr/bin/python
"""Test the reality conditions on FFTs."""

import numpy as np
import scipy.fftpack as fft

np.random.seed(11)
N = 4

def fft_sample_spacing():
	# Need slightly special FFT freq. numbering order, given by fftfreq()
	NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
	
	# Get sample spacing. (1D): k_n = 2pi*n / (N*dx), N is array length
	# (3D): k_n1,n2,n3 = [2pi / (N*dx)] * sqrt(n1^2 + n2^2 + n3^2)
	kk = [[[np.sqrt(i**2. + j**2. + k**2.) for k in NN] for j in NN] for i in NN]
	kk = np.array(kk) # * (2.*np.pi/self.L)
	return kk

# Get FFT and sample spacing
k = fft_sample_spacing()
f = np.random.uniform(size=(N, N, N))
F = fft.fftn(f) / N**(3./2.)

# Get x,y,z components in k array
kx = np.zeros((N,N,N)); ky = np.zeros((N,N,N)); kz = np.zeros((N,N,N))
NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
for i in NN:
	kx[i,:,:] = i
	ky[:,i,:] = i
	kz[:,:,i] = i

# Get indices were highest freq. in any dimension is used and set to zero (muwahaha)
mx = np.where(kx == np.min(kx))
my = np.where(ky == np.min(ky))
mz = np.where(kz == np.min(kz))
F[mx] = 0.0; F[my] = 0.0; F[mz] = 0.0

# Multiply by appropriate factors
Fx = F * kx; Fy = F * ky; Fz = F * kz

print "-"*50
print kx[mx]

print "-"*50
print ky

print "-"*50
print kz

# Do inverse transform
fx = 1j * fft.ifftn(Fx) * N**(3./2.)
fy = 1j * fft.ifftn(Fy) * N**(3./2.)
fz = 1j * fft.ifftn(Fz) * N**(3./2.)
		
print fz.real

