#!/usr/bin/python
"""Test the populate_clusters code."""

import numpy as np
import pylab as P
from populate_clusters import *
import cProfile
import time
tstart = time.time()

#BOX_NAME = "testbox-64-1000"
BOX_NAME = "box-128-3000"
OUT_NAME = "catalogue-" + BOX_NAME

CS = ClusterSample(BOX_NAME)

# Testing
#cProfile.run("CS.place_clusters_melin(100, CS.get_bin_density_field(0, 1))")

# Generate catalogue
cat = CS.generate_catalogue(CS.origin)
print "\nCatalogue:", np.shape(cat), cat.size
np.save(OUT_NAME, cat)

print "Run took", round(time.time() - tstart, 1), "sec."
