#!/usr/bin/python
"""Manipulate the matter power spectrum."""

import numpy as np
import scipy.integrate
import realise_matterpower as R
import scipy.fftpack as fft
import cosmolopy as cp
#np.random.seed(12)
np.random.seed(18)
np.seterr(all='ignore')


# Real-space box sizes to trial
Nsamp = [16, 32, 48, 64, 128]*8
scals = [1e3, 1e3, 1e3, 1e3, 1e3,
		 2e3, 2e3, 2e3, 2e3, 2e3,
		 5e3, 5e3, 5e3, 5e3, 5e3,
		 7e3, 7e3, 7e3, 7e3, 7e3,
		 1e4, 1e4, 1e4, 1e4, 1e4,
		 1.2e4, 1.2e4, 1.2e4, 1.2e4, 1.2e4,
		 1.6e4, 1.6e4, 1.6e4, 1.6e4, 1.6e4,
		 2e4, 2e4, 2e4, 2e4, 2e4]


# Loop through all box sizes
values = []
print "i\tBoxScale\tNsamp\ts8_real\ts30_real\ts8_th\ts30_th"
for i in range(len(Nsamp)):
	print i, "/", len(Nsamp)
	
	# Get realisation of power spectrum
	x, kk, delta_x, delta_k, ps, norm = R.realise(scale=scals[i], nsamp=Nsamp[i])
	
	# Get binned power spectrum for realisation
	k, psexp = R.realisation_ps(x, delta_x, bins=30)

	# Get P(k) samples s.t. k is monotonically-increasing (ready for integration)
	k_mono, idxs = np.unique(kk.flatten(), return_index=True)
	pk = (ps.flatten())[idxs]
	
	# Sigma 8/30 from realisation
	s8_exp = R.realisation_sigma8(k, psexp)
	s30_exp = R.realisation_sigmaR(k, psexp, 30.)
	
	# Sigma8/30 from theory
	s8_th, _k, _y = R.calc_sigma8()
	s30_th = R.calc_sigmaR(30.)
	
	# Prepare values for output
	vals = [i, scals[i], Nsamp[i], s8_exp, s30_exp, s8_th, s30_th]
	values.append(vals)
	#print "\t".join([str(v) for v in vals])

# Output results
values = np.array(values)
np.savetxt("NormTest.dat", values, delimiter="\t")
