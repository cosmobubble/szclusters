#!/usr/bin/python
"""Test finding the velocity field."""

import numpy as np
import cosmolopy as cp

BOX_NAME = "testbox-64-4000"


################################################################################
# Load data files
################################################################################

def load_config(config_filename):
	"""Load the configuration file for a realised box, and return a 
	   dictionary."""
	f = open(config_filename, 'r')
	config = {}
	for line in f.readlines():
		# Check for comments
		if "#" not in line:
			line = line.split(":")
			if len(line) == 2:
				key = line[0]
				val = line[1][:-1]
				try:
					val = float(val)
				except:
					val = str(val)
				config[key] = val
	return config
	
def load_box(BOX_NAME):
	"""Load box containing realised density field, plus its Fourier 
	   transform and k values."""
	# Define filenames
	deltax_filename = BOX_NAME + ".delta_x.npy"
	deltak_filename = BOX_NAME + ".delta_k.npy"
	ksamp_filename = BOX_NAME + ".ksamp.npy"
	config_filename = BOX_NAME + ".config"
	
	# Load data (could take up quite a lot of memory!)
	delta_x = np.load(deltax_filename) # Load density field
	delta_k = np.load(deltak_filename) # Load FT of density field
	k = np.load(ksamp_filename) # Load k array
	config = load_config(config_filename) # Load config file/settings
	
	return delta_x, delta_k, k, config


################################################################################
# Calculate velocities
################################################################################

# Load data
delta_x, delta_k, k, config = load_box(BOX_NAME)

# Calculate linear growth factor etc.
f = cp.perturbation.fgrowth(z=0.0, omega_M_0=cosmo['omega_M_0'])
H = cp.distance.hubble_z(z=0.0, **cosmo)

# Velocity in Fourier space, v(k), from Dodelson Eq. 9.18
# v(k) = i*f*a*H(z) * delta(k, z) / k (in \hat{k} direction)


