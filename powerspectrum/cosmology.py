#!/usr/bin/python
"""Define a cosmological model, and methods for calculating variables of interest."""

import numpy as np
import scipy.integrate
import cosmolopy as cp
from units import *

# Define default cosmology
DEFAULT_COSMOLOGY = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.8,
		 'baryonic_effects': True
		}


class Cosmology(object):
	
	def __init__(self, cosmo=DEFAULT_COSMOLOGY):
		"""Initialise the cosmological model, using the same dictionary 
		   structure as CosmoloPy."""
		
		self.cosmo = cosmo
		self.h = cosmo['h']
		self.Om = cosmo['omega_M_0']
		
		# Set constants (for integreation, differentiation, bin widths etc.)
		self.dlnM = 1e-2 # Resolution in logarithmic cluster mass
		
		# Precompute some values
		self.rho_m = self._rho_m() # Background matter density
		self.rho_crit = self._rho_crit() # Background critical density
		
		# Matter power spectrum P(k) at z=0, sampled in log(k) space
		self.k = np.logspace(-5., 1., 1e4)
		self.pk = cp.perturbation.power_spectrum(self.k, z=0.0, **self.cosmo)


	############################################################################
	# Background cosmological variables
	############################################################################

	def _rho_m(self):
		"""Mean matter density today, in solar masses per Mpc^3."""
		return 3. * (self.h*fH0)**2. * self.Om / (8. * np.pi * G)
	
	def _rho_crit(self):
		"""Background critical density today, in solar masses per Mpc^3."""
		return 3. * (self.h*fH0)**2. / (8. * np.pi * G)
	
	def z_comov(self, x):
		"""Redshift out to a comoving distance x."""
		# FIXME
		print "WARNING: z_comov() not written yet."
		return 0.0
	
	############################################################################
	# Functions for smoothing the density field
	############################################################################
	
	def window(self, k, R):
		"""Fourier transform of tophat window function, used to calculate 
		   sigmaR etc. See "Cosmology", S. Weinberg, Eq.8.1.46."""
		x = k*R
		f = (3. / x**3.) * ( np.sin(x) - x*np.cos(x) )
		return f**2.

	def sigmaR(self, R):
		"""Get variance of matter perturbations, smoothed with a tophat filter 
		   of radius R h^-1 Mpc."""
		# FIXME: Worry about h^-1 Mpc units
	
		# Discretely-sampled integrand, y
		y = self.k**2. * self.pk * self.window(self.k, R/self.h)
		I = scipy.integrate.simps(y, self.k)
	
		# Return sigma_R (note factor of 4pi / (2pi)^3 from integration)
		return np.sqrt( I / (2. * np.pi**2.) )

	def sigmaM(self, M):
		"""Get variance of matter perturbations, smoothed with tophat that encloses 
		   mass M at mean matter density today, rho_m. (See Hu.)"""
		# Convert enclosed mass M to smoothing radius R
		R = ( (3. * M) / (4. * np.pi * self.rho_m) )**(1./3.)
		return self.sigmaR(R) # Get corresponding sigma(R)
	
	def DeltaC(self, z):
		"""Critical overdensity, used to find cluster virialisation scale. See 
		   Nakamura and Suto, arXiv:astro-ph/9612074v1, App. C. (1996) for full 
		   expression."""
		# FIXME
		return 100.

	############################################################################
	# Number counts
	############################################################################

	def dn_dlnM(self, logM):
		"""Get halo number counts as a function of mass scale (see Hu, Eq. 1, for 
		   fitting fn.). Uses Hu's fitting function (astro-ph/0301416)."""
		# Get masses and variances
		M = np.exp(logM)
		M2 = np.exp(np.log(M) + self.dlnM)
		sm = self.sigmaM(M); sm2 = self.sigmaM(M2)
		# Calculate derivative dln(sigma^-1)/dlnM
		dd = -1. * ( np.log(sm2) - np.log(sm) ) / (self.dlnM)
		return (0.3*self.rho_m / M) * dd * np.exp(-np.abs(0.64 - np.log(sm))**3.82)
	
	def dn_dlogM_z(self, logM, z):
		"""Get halo number counts as a function of mass scale and redshift."""
		
		# Get masses and variances
		M = np.exp(logM)
		M2 = np.exp(np.log(M) + self.dlnM)
		R1 = cp.perturbation.mass_to_radius(M, **self.cosmo)
		R2 = cp.perturbation.mass_to_radius(M2, **self.cosmo)
		sm, err = cp.perturbation.sigma_r(R1, z, **self.cosmo)
		sm2, err = cp.perturbation.sigma_r(R2, z, **self.cosmo)
	
		# Calculate derivative dln(sigma^-1)/dlnM
		dd = -1. * ( np.log(sm2) - np.log(sm) ) / (self.dlnM)
		dn = (0.3*self.rho_m / M) * dd * np.exp(-np.abs(0.64 - np.log(sm))**3.82)
		return dn
		
