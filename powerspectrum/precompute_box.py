#!/usr/bin/python
"""Generate a realisation of some power spectrum and save it to disk."""

import sys, time
import numpy as np
import pylab as P
import cosmolopy as cp
import scipy.integrate
from generate_box import CosmoBox
tstart = time.time()


# Determine whether to run in "dry run" mode or not
if len(sys.argv) > 2 and sys.argv[1].lower() == "generate":
	# Actually generate a box and save it
	print "\n\t*** Generating box..."
	DRY_RUN = False
	BOX_NAME = sys.argv[2]
else:
	# Just do a dry-run, don't actually start generating the box
	print "\n\t*** Performing dry-run..."
	print "\t    To actually generate a box, type './precompute_box.py generate box_name'."
	DRY_RUN = True
	BOX_NAME = "testbox"


# Define cosmology
cosmo = {
		 'omega_M_0': 		0.27,
		 'omega_lambda_0': 	0.73,
		 'omega_b_0': 		0.045,
		 'omega_n_0':		0.0,
		 'omega_k_0':		0.0,
		 'N_nu':			0,
		 'h':				0.7,
		 'n':				0.96,
		 'sigma_8':			0.8,
		 'baryonic_effects': True
		}

# Define box parameters
#L = 4e3 # Box side, Mpc # SEGFAULTED!
#N = 256 # Samples per side
L = 3e3
N = 128

# Calculate kmax, kmin
kmin = 2.*np.pi / L
kmax = 2.*np.pi*0.5*np.sqrt(3.) * N / L

# Report on basic box parameters
print ""
print "\tN =\t", N
print "\tL =\t", L, "Mpc"
print "\tkmin =\t", kmin, "Mpc^-1"
print "\tkmax =\t", kmax, "Mpc^-1"

# Set filenames
fname = BOX_NAME + "-" + str(N) + "-" + str(int(L)) # Box filename
cfname = fname + ".config"

# Perform either dry-run, or full calculation
if DRY_RUN:
	# Display a graph showing the theoretical power spectrum, and compare sigma8
	
	# Calculate theoretical sigma8 in same k-window as realisation
	window = lambda k, R: (  (3. / (k*R)**3.) * ( np.sin(k*R) - (k*R)*np.cos(k*R) ) )**2.
	_k = np.linspace(kmin, kmax, 5e3)
	_pk = cp.perturbation.power_spectrum(_k, z=0.0, **cosmo)
	_y = _k**2. * _pk * window(_k, 8.0/cosmo['h'])
	_y = np.nan_to_num(_y)
	s8_th_win = np.sqrt( scipy.integrate.simps(_y, _k) / (2. * np.pi**2.) )
	
	# Reference P(k)
	k = np.logspace(-3, 0, 5e2)
	pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
	
	# Output diagnostic information on sigma8
	print "\nDIAGNOSTICS:"
	print "\tsigma8 (window):\t", s8_th_win
	print "\tsigma8 (cosmo.):\t", cosmo['sigma_8']
	
	# Plot the resulting power spectrum, and k-window
	P.subplot(111)
	P.plot(k, pk, 'k-')
	P.axvline(kmin, ls="solid", color="r")
	P.axvline(kmax, ls="solid", color="r")
	P.xlabel("k [Mpc^-1]")
	P.ylabel("P(k)")
	P.xscale('log')
	P.yscale('log')
	
	
else:

	# Generate box (can take a while; about 10mins to do a 256^3 box on Dell laptop)
	box = CosmoBox(cosmo=cosmo, scale=L, nsamp=N)
	print "\tFinished generating box."
	
	# Save density and velocity fields
	delta_x = box.delta_x # Save the realised matter field
	delta_k = box.delta_k
	kvx, kvy, kvz = box.velocity_k
	
	np.save(fname+".delta_x", delta_x)
	np.save(fname+".delta_k", delta_k)
	np.save(fname+".kvx", kvx)
	np.save(fname+".kvy", kvy)
	np.save(fname+".kvz", kvz)
	
	print "\tSaved box as", fname
	
	# Save box parameters
	f = open(cfname, 'w')
	f.write("# COSMOLOGY\n")
	for key in cosmo.keys():
		line = str(key) + ":" + str(cosmo[key]) + "\n"
		f.write(line)
	f.write("# BOX INFO\n")
	f.write("N:" + str(N)+"\n")
	f.write("L:" + str(L)+"\n")
	f.write("kmin:" + str(kmin)+"\n")
	f.write("kmax:" + str(kmax)+"\n")
	f.write("Box_filename:" + str(fname)+"\n")
	f.write("Config_filename:" + str(cfname)+"\n")
	print "\tSaved configuration as", cfname



# Print timing info
print "Run took", round(time.time() - tstart, 2), "sec."
if DRY_RUN:
	P.show()
