#!/usr/bin/python
"""Test the normalisation that I derived for the discrete FFT power spectrum."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import realise_matterpower as R
import pylab as P

#np.random.seed(17)

# Define default cosmology
DEFAULT_COSMOLOGY = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}

def fft_sample_spacing(x):
	"""Calculate the sample spacing in Fourier space, given some symmetric 3D 
	   box in real space, with 1D grid point coordinates 'x'."""
	N = x.size # Dimension of grid
	fac = 2.*np.pi/(x[1]-x[0]) # Overall factor (Mpc^-1)
	
	# Need slightly special FFT freq. numbering order, given by fftfreq()
	NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
	
	# Get sample spacing. In 1D, k_n = 2pi*n / (N*dx), where N is length of 1D array
	# In 3D, k_n1,n2,n3 = [2pi / (N*dx)] * sqrt(n1^2 + n2^2 + n3^2)
	kk = [[[np.sqrt(i**2. + j**2. + k**2.) for i in NN] for j in NN] for k in NN]
	kk = fac * np.array(kk)
	return kk


def realise(cosmo=DEFAULT_COSMOLOGY, scale=2e3, nsamp=32):
	"""Create realisation of the matter power spectrum for some regular grid of 
	   side NSAMP, with cosmology 'cosmo'."""
	
	# Generate 3D Gaussian random field in real space
	x = np.linspace(-scale, scale, nsamp) # 3D grid coords, in Mpc
	delta_xi = np.random.randn(x.size, x.size, x.size)

	# FT the field, respecting reality conditions on FFT (Ch12, Num. Rec.)
	delta_ki = fft.fftn(delta_xi) # 3D discrete FFT
	kk = fft_sample_spacing(x) # Get grid coordinates in k-space
	
	# Calculate matter power spectrum
	k = kk.flatten() # FIXME: Lots of duplicate values. Can be more efficient.
	ps = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
	norm = cp.perturbation.norm_power(**cosmo)
	ps = np.reshape(ps, np.shape(kk))
	ps = np.nan_to_num(ps) # Remove NaN at k=0 (and any others...)

	# Multiply by sqrt of power spectrum and transform back to real space
	# FIXME: 'norm' should be here, or not?
	delta_k = np.sqrt(ps) * delta_ki
	delta_x = np.real(  fft.ifftn(delta_k)  ) # Discard small imag. values
	
	return x, kk, delta_x, delta_k, delta_xi, delta_ki, ps, norm


# Get realisation
NSIDE = 32
NSIDE2 = 16
SCALE = 1.0e4
SCALE2 = 1.2e3
L = 2. * SCALE
x, k, delta_x, delta_k, delta_xi, delta_ki, ps, norm = realise(scale=SCALE, nsamp=NSIDE)
x2, k2, delta_x2, delta_k2, delta_xi2, delta_ki2, ps2, norm2 = realise(scale=SCALE2, nsamp=NSIDE2)

N = x.size; N2 = x2.size
CC = N**3. / L**3.
CC2 = N**3. / L**3.
pkreal = np.abs(delta_k)**2. / N**3.
pkreal2 = np.abs(delta_k2)**2. / N2**3.
s8_real = R.discrete_realisation_sigma8(k, pkreal)
s8_real2 = R.discrete_realisation_sigma8(k2, pkreal2)


print "Nside\t", N, "\t", N2, "\t", N/N2
print "Scale\t", SCALE, "\t", SCALE2, "\t", SCALE/SCALE2
print "\t\t\t\t", (SCALE/SCALE2)**2., "\t", (SCALE/SCALE2)**(2./3.)
print "sigma8_real =", s8_real, s8_real2

print ""
print "delta_x^2\t", np.sum(delta_x**2.), "\t", np.sum(delta_x2**2.), "\t", np.sum(delta_x**2.)/np.sum(delta_x2**2.)
print "delta_k^2\t", np.sum(np.abs(delta_k)**2.) / (N**3.), "\t", np.sum(np.abs(delta_k2)**2.) / (N2**3.)
print "sum[P(k)]\t", np.sum(ps)*norm, "\t", np.sum(ps2)*norm2

print ""
print "delta_xi^2\t", np.sum(delta_xi**2.) / N**3., "\t", np.sum(delta_xi2**2.) / N2**3.
print "delta_ki^2\t", np.sum(np.abs(delta_ki)**2.) / N**6., "\t", np.sum(np.abs(delta_ki2)**2.) / N2**6.


"""
NSIDE = np.arange(8, 64, 4)
SCALE = [0.7e2, 1.2e3, 4.5e3, 1e4, 3.2e4]

# Get realisations
d2 = []
for SS in SCALE:
	_d2 = []
	for NN in NSIDE:
		print NN
		x, k, delta_x, delta_k, delta_xi, delta_ki, ps = realise(scale=SS, nsamp=NN)
		_d2.append( np.sum(delta_x**2.) )
	d2.append(_d2)

P.subplot(111)
for i in range(len(d2)):
	P.plot(NSIDE, d2[i])
P.show()
"""
