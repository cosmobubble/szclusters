#!/usr/bin/python
"""Test code to calculate variance of matter field."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import pylab as P
import cosmolopy as cp

cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}

def window(k, RR):
	"""Window function, which filters the FT. Used in sigma8 calculation."""
	# See Weinberg, Eq.8.1.46
	x = k*RR
	f = (3. / x**3.) * ( np.sin(x) - x*np.cos(x) )
	return f

"""
N = 1000
R = 10.
x = np.linspace(0, N, N)
fx = np.zeros(N)
fx[:int(R)] = 1.0

fk = fft.fftn(fx) # 3D discrete FFT
k = fft.fftfreq(N, 1.)
kk = fft.fftshift(k)

WW = window(kk, R)
#ww = fft.ifftn(WW) # 3D discrete FFT
"""


# sigma_0^2
_k = np.logspace(-10, 20, 5e4)
pk = cp.perturbation.power_spectrum(_k, z=0.0, **cosmo)

s0 = scipy.integrate.simps(_k**2.*pk, _k)
print s0

P.subplot(111)
P.plot(_k, _k**2. * pk, 'r-')
P.xscale('log')
P.yscale('log')


"""
P.subplot(211)
P.plot(x, fx, 'b-')
#P.plot(x, ww, 'r-')

P.subplot(212)
P.plot( kk, fft.fftshift(fk), 'b-' )
P.plot( kk, WW, 'r-')
"""
#P.show()
