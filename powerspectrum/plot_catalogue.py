#!/usr/bin/python
"""Plot data from catalogue generated from a realisation of the density field."""

import numpy as np
import pylab as P

fname = "catalogue-box-128-3000.npy"
theta, phi, zz, M, x, y, z, vx, vy, vz = np.load(fname).T
theta = np.nan_to_num(theta)
phi = np.nan_to_num(phi)

P.subplot(221)
P.hist(M, bins=50)
P.xlabel("M")

P.subplot(222)
P.hist(zz, bins=50)
P.xlabel("z")

P.subplot(223)
P.hist(z, bins=100)
P.xlabel("z_coord")

P.subplot(224)
P.hist(vx, bins=50)
P.xlabel("vx")

P.show()
