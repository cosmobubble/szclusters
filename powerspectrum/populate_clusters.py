#!/usr/bin/python
"""Populate random density field with clusters."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import scipy.interpolate
import scipy.spatial
import cosmolopy as cp
import pylab as P
from load_box import load_config
from cosmology import Cosmology

np.seterr(divide='ignore', invalid='ignore') # Kill some warnings for div/0
np.random.seed(10)
__TESTING__ = True


# FIXME: NEED TO CHECK ARRAY ORDERING!
# At the moment, any generator that goes like [[[... for i in range()] for j in range...
# produces arrays with indices arr[z][y][x]
# (Should have fixed this now, but still need to check properly.)


################################################################################
# Define a class which returns a sample of clusters over some mass/z range
################################################################################

class ClusterSample(object):
	"""Create a sample of clusters witht he correct clustering properties in 
	   redshift. Needs a density field to do this."""
	
	def __init__(self, boxname):
		"""Initialise the cluster sample by loading a density field box."""
		self.boxname = boxname
		
		self.CALCULATE_ANGLES = False # Whether to calculate angles or not
		
		# Set cluster-related properties
		self.M_MAX = 		1e15 	# Max. cluster mass, Msun
		self.M_MIN = 		2e14 	# Msun (~10^13 is roughly typ. cluster mass in surveys)
		self.N_MASS_BINS = 	10 		# Number of logarithmic mass bins
		self.Z_MAX = 		0.4 	# Max. cluster redshift
		self.Z_MIN = 		0.0
		self.N_Z_BINS = 	15 		# No. linear redshift bins
		self.set_cluster_bins()
		
		# Load the realisation data
		self.load_box() # NOTE: Heavy I/O
		
		# Initialise cosmological model and box parameters
		self.C = Cosmology(self.config)
		self.L = float(self.config['L'])
		self.N = int(self.config['N'])
		self.boxfactor = (self.N**6.) / self.L**3.
		self.print_box_properties()
		
		# Pre-compute FFT frequency and cartesian coords for all grid cells
		self.get_fft_sample_spacing()
		self.get_cartesian_coords()
		
		# Set observer position and calculate density field evolution
		print "\tCalculating redshift evolution..."
		self.origin = self.get_centre_origin()
		self.calculate_grid_evolution(self.origin)
		print "\tFinished."
		
		# Precompute sigma(M, z)
		print "\tPrecomputing sigma(M, z)..."
		self.sigma_interp()
		print "\tFinished."
		
	
	def print_box_properties(self):
		"""Output the basic properties of the box and cosmological model."""
		print "\tBox cells:", np.shape(self.delta_x)
		print "\tBox size:", self.L, "Mpc"
		#print "\tBox config. info:", self.config


	############################################################################
	# Loading and saving 
	############################################################################

	def load_config(self):
		"""Load the configuration file for a realised box, and return a 
		   dictionary."""
		f = open(self.config_filename, 'r')
		config = {}
		for line in f.readlines():
			# Check for comments
			if "#" not in line:
				line = line.split(":")
				if len(line) == 2:
					key = line[0]
					val = line[1][:-1]
					try:
						val = float(val)
					except:
						val = str(val)
					config[key] = val
		return config
	
	def load_box(self):
		"""Load box containing realised density field, plus its Fourier 
		   transform and k values."""
		# Define filenames
		self.deltax_filename = self.boxname + ".delta_x.npy"
		self.deltak_filename = self.boxname + ".delta_k.npy"
		self.kvx_filename = self.boxname + ".kvx.npy"
		self.kvy_filename = self.boxname + ".kvy.npy"
		self.kvz_filename = self.boxname + ".kvz.npy"
		self.config_filename = self.boxname + ".config"
		
		# Load data (could take up quite a lot of memory!)
		self.delta_x = np.load(self.deltax_filename) # Load density field
		self.delta_k = np.load(self.deltak_filename) # Load FT of density field
		self.kvx = np.load(self.kvx_filename) # Load unscaled velocity arrays
		self.kvy = np.load(self.kvy_filename)
		self.kvz = np.load(self.kvz_filename)
		self.config = self.load_config() # Load config file/settings

	def get_fft_sample_spacing(self):
		"""Calculate the sample spacing in Fourier space, given some symmetric 3D 
		   box in real space, with 1D grid point coordinates 'x'."""
		self.kx = np.zeros((self.N,self.N,self.N))
		self.ky = np.zeros((self.N,self.N,self.N))
		self.kz = np.zeros((self.N,self.N,self.N))
		NN = ( self.N*fft.fftfreq(self.N, 1.) ).astype("i")
		for i in NN:
			self.kx[i,:,:] = i
			self.ky[:,i,:] = i
			self.kz[:,:,i] = i
		fac = (2.*np.pi/self.L)
		self.k = np.sqrt(self.kx**2. + self.ky**2. + self.kz**2.) * fac
		
	def get_cartesian_coords(self):
		"""Get i,j,k coords for each cell in the whole grid."""
		self.ix = np.zeros((self.N, self.N, self.N))
		self.iy = np.zeros((self.N, self.N, self.N))
		self.iz = np.zeros((self.N, self.N, self.N))
		for i in range(self.N):
			self.ix[i,:,:] = i
			self.iy[:,i,:] = i
			self.iz[:,:,i] = i
	
	############################################################################
	# Sampling the cluster mass function
	############################################################################

	def set_cluster_bins(self):
		"""Define the mass and redshift bins to be used for creating the 
		   cluster sample."""
		
		# Mass bin edges
		self.M_bin_edges = np.logspace( np.log10(self.M_MIN), 
										np.log10(self.M_MAX),
										self.N_MASS_BINS+1 )
		# Mass bin centroids
		self.Mc = np.array(  [ 0.5 * (self.M_bin_edges[i] + self.M_bin_edges[i+1]) 
							   for i in range(self.N_MASS_BINS) ]  )
		# Mass bin linear and logarithmic intervals
		self.dM = np.array(  [ (self.M_bin_edges[i+1] - self.M_bin_edges[i])
							  for i in range(self.N_MASS_BINS) ]  )
		self.dlnM = np.array([ (np.log(self.M_bin_edges[i+1]) - np.log(self.M_bin_edges[i]))
							  for i in range(self.N_MASS_BINS) ]  )
		
		# Redshift bin edges, centroids, and linear intervals
		self.z_bin_edges = np.linspace(self.Z_MIN, self.Z_MAX, self.N_Z_BINS+1)
		self.zc = np.array( [ 0.5 * (self.z_bin_edges[i] + self.z_bin_edges[i+1]) 
							   for i in range(self.N_Z_BINS) ]  )
		self.dz = np.array(  [ (self.z_bin_edges[i+1] - self.z_bin_edges[i])
							  for i in range(self.N_Z_BINS) ]  )

	
	def sigma_interp(self):
		"""sigma(M, z) takes a long time to compute, but should vary quite 
		   slowly as a function of M and z. Precompute its values on a grid and 
		   provide an interpolator to speed-up calculation.
		"""
		# Loop through all bin edges in (M, z) and calculate sigma(M, z)
		s = []
		for m in self.M_bin_edges:
			_s = []
			R = cp.perturbation.mass_to_radius(m, **self.config)
			for z in self.z_bin_edges:
				sigma, err = cp.perturbation.sigma_r(R, z, **self.config)
				_s.append(sigma)
			s.append(_s)
		s = np.array(s)
		
		# Create interpolation function
		# FIXME: Check that the array is ordered correctly! sig should be transposed?
		self.isigma = scipy.interpolate.interp2d( self.M_bin_edges,
												  self.z_bin_edges,
												  s.T, kind='linear' )
	
	def get_bin_Nbar(self, Midx, zidx):
		"""Get expected no. clusters for a given mass and redshift bin. 
		   Integrate to find Nbar. Integrand is time-consuming to evaluate, so 
		   sample it on a grid and then add up."""
		
		# Calc. no. clusters expected in bin (see Lima and Hu, 2004, Sect.II)
		# NOTE: We are neglecting covariance between bins! (Should be small.)
		# Define sample points
		mm = np.linspace(self.M_bin_edges[Midx], self.M_bin_edges[Midx+1], 20)
		zz = np.linspace(self.z_bin_edges[zidx], self.z_bin_edges[zidx+1], 21)
		
		# Evaluate sample points
		s = np.array( [[self._Nbar_integrand(m, z) for m in mm] for z in zz] )
		
		# Integrate in mass, then redshift
		m_int = [scipy.integrate.simps(s[i], mm) for i in range(len(zz))]
		I = scipy.integrate.simps(m_int, zz)
		return I
		
	
	def _Nbar_integrand(self, M, z):
		"""Integrand of N(M,z); see Basilakos et al., arXiv:1006.3418, Eq. 6.10."""
		# Integrand is n(M,z) * (dV/dz)
		
		# Get sigma(R)
		dlogM = 1e-2 # In Msun. FIXME: ***TEST SENSITIVITY TO THIS***
		MdM = np.exp(np.log(M) + dlogM)
		sigma = self.isigma(M, z)[0]
		sigma1 = self.isigma(MdM, z)[0]
		
		# Get volume element
		r = cp.distance.comoving_distance(z, **self.config)
		drdz = cp.distance.comoving_integrand(z, **self.config)
		dVdz = 4. * np.pi * r**2. * drdz
		
		# Get number density from Press-Schechter theory
		rho_c, rho_m = cp.density.cosmo_densities(**self.config)
		d_c = 1.686 # Critical density. See also p285 of Dodelson. FIXME: z evolution?
		x = d_c / sigma
		fPS = np.sqrt(2./np.pi) * x * np.exp(-x**2. / 2.) # See Basilakos Eq. 6.2
		dlogsigma_dlogM = ((1./sigma1) - (1./sigma)) / dlogM # d(sigma^-1)/dM
		nmz = (rho_m / M**2.) * dlogsigma_dlogM * fPS
		
		# Integrand: n(M, z) * dV/dz
		return nmz * dVdz
	
	def get_bin_vol(self, zidx):
		"""Get the volume in redshift space of a given bin."""
		z1 = self.z_bin_edges[zidx]
		z2 = self.z_bin_edges[zidx+1]
		vol, err = scipy.integrate.quad(self._vol_integrand, z1, z2)
		return vol
	
	def _vol_integrand(self, z):
		"""Integrand of dV/dz."""
		# Get volume element
		r = cp.distance.comoving_distance(z, **self.config)
		drdz = cp.distance.comoving_integrand(z, **self.config)
		dVdz = 4. * np.pi * r**2. * drdz
		return dVdz
		

	############################################################################
	# Smoothed fields
	############################################################################
	
	def bias(self, z, M):
		"""The multiplicative bias to apply to the density contrast for clusters."""
		# FIXME: See Sheth and Tormen 1999 for better cluster bias prescription.
		return 1.0
	
	def smoothed_field(self, R, kfield):
		"""Return a density field, smoothed by some window function, given its 
		   Fourier Transform, 'field'."""
		# FT of tophat window function. See "Cosmology", S. Weinberg, Eq.8.1.46.
		window = lambda k, R: (3. / (k*R)**3.) * ( np.sin(k*R) - (k*R)*np.cos(k*R) )
		
		# Convolve field with tophat window fn., in Fourier space.
		skfield = window(self.k, R) * kfield
		skfield = np.nan_to_num(skfield)
		
		# Do inverse transform to get real-space field back
		# (should be real, with very small imaginary parts that we can remove)
		rfield = fft.ifftn(skfield)
		return rfield.real
	
	def get_centre_origin(self):
		"""Get origin (observer location) which is at the centre of the box 
		   (or close to it)."""
		ii = int(self.N/2)
		return (ii, ii, ii)
	
	def get_random_origin(self):
		"""Get origin which is in a random location in the box."""
		ii = np.random.randint(0, self.N)
		jj = np.random.randint(0, self.N)
		kk = np.random.randint(0, self.N)
		return (ii, jj, kk)
	
	def calculate_grid_evolution(self, origin):
		"""Calculate the comoving distance, redshift, and linear growth factor 
		   of every cell in the grid, given some origin grid cell 
		   (tuple index in x,y,z)."""
		
		# Calculate comoving distances between cells
		NN = np.array(range(self.N))
		i0, j0, k0 = origin
		dx = [[[ np.sqrt(  (i-i0)**2. + (j-j0)**2. + (k-k0)**2.  )
				for k in NN] for j in NN] for i in NN]
		self.dx = np.array(dx) * self.L / self.N
		
		# FIXME: Something is going wrong with these calculations!
		
		# Calculate angular coordinates of cells w.r.t. origin. (takes a while)
		# FIXME: Should be precomputed for grid
		# NOTE: Can speed this up in quite a simple way.
		if self.CALCULATE_ANGLES:
			NN = NN.astype("f")
			self.phi = [[[ np.arctan((j-j0)/(i-i0)) 
						   for k in NN] for j in NN] for i in NN]
			_z = np.array( [[[ (k-k0) for k in NN] for j in NN] for i in NN] )
			self.theta = np.arccos(_z/self.dx)
		
		# Calculate redshift as a function of comoving distance
		zz = np.linspace(0.0, 2.0, 200)
		x = cp.distance.comoving_distance(zz, **self.config)
		interp_z_x = scipy.interpolate.interp1d(x, zz, kind="linear")
		self.z = interp_z_x(self.dx) # Redshift of each grid cell
		
		print "\tMax. z from grid (complete sample):", interp_z_x(self.L*0.5)
		print "\tMax. z from grid (incomplete sample):", interp_z_x(self.L)
		
		# Calculate linear growth factor D(z) for each grid cell
		# FIXME: Is this normalised correctly? See notes in Cosmolopy docs.
		self.Dz = cp.perturbation.fgrowth(self.z, self.config['omega_M_0'])
		
		# Calculate bin masks
		self.zmasks = []
		for i in range(self.zc.size):
			mask = np.where(  (self.z_bin_edges[i] <= self.z) 
							& (self.z_bin_edges[i+1] > self.z) )
			self.zmasks.append(mask)
	
	def get_bin_density_field(self, Midx, zidx):
		"""Get the density field at a given redshift, smoothed for a given mass."""
		# FIXME: Need to worry about redshift evolution
		
		# Get smoothed density field
		M = self.Mc[Midx]
		z = self.zc[zidx]
		R = cp.perturbation.mass_to_radius(M, **self.config) * (1. + z)
		sfield = self.smoothed_field(R, self.delta_k)
		
		# Get redshift bin mask and evolve the field in redshift
		msk = self.zmasks[zidx]
		cell = sfield[msk] * self.Dz[msk]
		return cell


	############################################################################
	# Cluster properties (profile, radius, mass etc.)
	############################################################################

	def r_delta(self, M_delta, delta):
		"""Cluster radius corresponding to mass M_delta, which is supposed to be 
		   where the overdensity is delta times the critical density of the 
		   Universe; see Eq. 13 of Komatsu and Seljak."""
		return ( 3.*M_delta / (4. * np.pi * self.C.rho_crit * delta) )**(1./3.)

	def r_vir_komatsu(self, M_vir):
		"""Cluster virial radius, which depends on mass and background cosmology;
		   see Eq. 6 of Komatsu and Seljak."""
		# Essentially, from spherical collapse model
		DeltaC = self.C.DeltaC(0.0) # FIXME: See disc. below Eq.6; evolves with z for LCDM!
		return self.r_delta(M_vir, DeltaC)

	def r_vir_melin(self, M_vir, z=0.0):
		"""Different equation for the virial radius. See Melin et al., 
		   Eq. 9, arXiv:astro-ph/0409564 (2004)."""
		E = cp.distance.e_z(z, **self.config) # FIXME: Test. E(z), dim'less Hubble rate
		DeltaC = self.C.DeltaC(0.0) # FIXME: From LambdaCDM
		# Fitting formula for virial radius, given virial mass (in Mpc)
		return 1.69*(self.h*E)**(-2./3.) * (M/1e15)**(1./3.) * (DeltaC/178.)**(-1./3.)
	
	
	############################################################################
	# Calculate velocity field
	############################################################################
	
	def f_linear(self):
		"""Linear growth rate. See Eq. 9.17 in Dodelson for expression."""
		# Also see Di Porto and Amendola, http://arXiv.org/abs/0707.2686v1
		return self.config['omega_M_0']**0.55
		
	def get_velocities(self, Midx, zidx, idxs):
		"""Get the velocity vector at the grid points given by idxs, for a 
		   smoothed velocity field."""
		
		# Get smoothed velocity field
		M = self.Mc[Midx]
		zc = self.zc[zidx]
		R = cp.perturbation.mass_to_radius(M, **self.config) * (1. + zc)
		sv_x = self.smoothed_field(R, self.kvx)
		sv_y = self.smoothed_field(R, self.kvy)
		sv_z = self.smoothed_field(R, self.kvz)
		
		# Pick out the desired points and get evolution factors
		msk = self.zmasks[zidx]
		z = self.z[msk].flatten()[idxs]
		D = self.Dz[msk].flatten()[idxs]
		a = 1. / (1. + z)
		f = self.f_linear()
		H = 100. * self.config['h'] * cp.distance.e_z(z, **self.config) # km/s/Mpc
		fac = f * a * H * D
		
		# Calculate final velocities of all points
		v_x = fac * sv_x[msk].flatten()[idxs]
		v_y = fac * sv_y[msk].flatten()[idxs]
		v_z = fac * sv_z[msk].flatten()[idxs]
		
		#print "Velocity diagnostic:", v_x[0], v_y[0], v_z[0]
		return v_x, v_y, v_z
	
	
	############################################################################
	# Place clusters in the density field
	############################################################################
	
	def get_cell_coords(self, idxs, zidx):
		"""Get the coordinates for selected cells """
		# Get only those coords that are in the z-bin
		msk = self.zmasks[zidx]
		xc = self.ix[msk].flatten()
		yc = self.iy[msk].flatten()
		zc = self.iz[msk].flatten()
		
		# Get the coords of the requested indices
		_x = xc[idxs]
		_y = yc[idxs]
		_z = zc[idxs]
		return _x, _y, _z
	
	def get_angle(self, x, y, z, origin):
		"""Get the angle on the sky, for coordinates with respect to some origin."""
		x0, y0, z0 = origin
		dx = x-x0; dy = y-y0; dz = z-z0
		
		# Get phi
		phi = np.arctan(dy / dx)
		
		# Get theta, and deal with poles
		try:
			theta = np.arccos(dz / np.sqrt(dx**2. + dy**2. + dz**2.) )
		except:
			if dz >= 0.:
				theta = 0.0
			else:
				theta = np.pi
		return theta, phi
		
	
	def generate_catalogue(self, origin):
		"""Return coordinates for clusters."""
		print "\tGenerating catalogue..."
		# Define empty catalogue
		cl_th = []; cl_ph = []; cl_zz = []; cl_m = []
		cl_x = []; cl_y = []; cl_z = []
		cl_vx = []; cl_vy = []; cl_vz = []
		
		# Loop through all cluster bins
		for Midx in range(self.Mc.size):
			for zidx in range(self.zc.size):
				print "\t\t* Bin", Midx, zidx
				
				# Get expected number count, and weight by density contrast
				Nbar = self.get_bin_Nbar(Midx, zidx)
				cells = self.get_bin_density_field(Midx, zidx)
				# Get mean density of cells (with delta >= -1 restriction applied)
				# FIXME: Is this sensible?
				delta = np.mean( np.where(cells >= -1., cells, 0.0) )
				print "\t\tBin <delta> =", delta
				
				# Realise no. of clusters to place and test that the bin is big enough
				Ncl = np.random.poisson(lam=Nbar*(1.+delta))
				if cells.size < Ncl:
					print "\t\tWARNING: More clusters than grid cells in this bin."
				if cells.size == 0:
					print "\t\tERROR: No grid cells in this bin. Exiting..."
					exit()
				if __TESTING__: # FIXME
					print "\t\tRequested Ncl =", Ncl
					#if Ncl > 500:
					#	Ncl = 500 # Use low Ncl to speed things up
				if Ncl == 0:
					continue # Skip to next bin if no clusters were requested.
				
				# Place clusters in bin
				idxs = self.place_clusters_melin(Ncl, cells) # Bottleneck for large Ncl
				x, y, z = self.get_cell_coords(idxs, zidx)
				
				# FIXME: Should properly sample from the mass bin...
				mm = np.random.uniform( low=self.M_bin_edges[Midx],
						   				high=self.M_bin_edges[Midx+1], size=Ncl )
				
				# Get cluster velocities
				vx, vy, vz = self.get_velocities(Midx, zidx, idxs)
				
				# Loop through each cluster and assign its properties
				for k in range(Ncl):
					_z = self.z[x[k]][y[k]][z[k]]
					_th, _ph = self.get_angle(x[k], y[k], z[k], origin)
					_m = mm[k]
					
					# Add to catalogue
					cl_th.append(_th); cl_ph.append(_ph)
					cl_zz.append(_z); cl_m.append(_m)
					cl_x.append(x[k]); cl_y.append(y[k]); cl_z.append(z[k])
					cl_vx.append(vx[k]); cl_vy.append(vy[k]); cl_vz.append(vz[k])
		
		print "\tFinished."
		A = np.array # More readable to alias this command
		cat = np.column_stack( ( A(cl_th), A(cl_ph), A(cl_zz), A(cl_m), 
								 A(cl_x), A(cl_y), A(cl_z),
								 A(cl_vx), A(cl_vy), A(cl_vz) )         )
		return cat
				
	
	def place_clusters_melin(self, Ncl, delta_x):
		"""Return coordinates for Ncl clusters, which are biased towards regions 
		   of high density. Based on method (briefly!) mentioned in Sect. 3.1 in 
		   Melin et al., arXiv:astro-ph/0409564 (2004)."""
		# Randomly place clusters, with probability proportional to (1+delta).
		# Placement location does not depend on cluster mass. Clusters can be 
		# placed in the same location as one another!
		print "\t\t  Placing", Ncl, "clusters over", delta_x.size, "grid points."
		
		b = 1. # Bias
		N = (delta_x.size)**(1./3.) # Grid Ncell
		_delta = delta_x.flatten()
	
		# Get density factor, (1+b*delta). Set factor to 0 if delta < -1.
		# This factor will be used to weight the placement probability
		fac = np.where(_delta >= -1.0, 1.+b*_delta, 0.)

		# Do cumulative sum to approximate probability integral transform
		# Prob. Int. Trans.: Find y = cumfac(x); => x is new random variable
		cumfac = np.cumsum(fac) # Cum. sum
		cumfac = cumfac / np.max(cumfac) # Normalise ***
	
		# Get grid indices randomly assigned to clusters (cell idx per cluster)
		# Need to only get indices where "distance" (cumfac - x) is positive
		# This is a massive bottleneck if a simple argmin() is used. Instead, 
		# use KDTrees to do it much faster (but algorithm is more complicated).
		
		X = np.random.rand(Ncl) # Sample from Uniform distribution
		
		# Get top 100 nearest neighbours for each point (ordered by distance)
		# No. of nearest neighbours to return. Large KNUM means fewer redo's, 
		# but longer non-redo execution time.
		KNUM = 10; KNUM2 = 150
		
		# Build KD tree, get neighbours, and get mask for neighbours which have 
		# non-negative distance (to ensure correct bin is found for each x in X)
		tree = scipy.spatial.cKDTree( cumfac.reshape((cumfac.size, 1)) )
		dists, idx2 = tree.query( X.reshape(X.size, 1), k=KNUM )
		fidx = [np.where(cumfac[idx2[i]] - X[i] >= 0.0)[0] for i in range(X.size)]

		# Check for rows where a nearest neighbour wasn't found
		fsize = np.array([len(row) for row in fidx])
		redo = np.where(fsize == 0)[0]
		if __TESTING__:
			print "\t\t\tplace_clusters_melin(): Redo's:", redo.size
		
		# Redo rows without NN by drawing larger sets of neighbours
		re_dists, re_idx2 = tree.query( X[redo].reshape(X[redo].size, 1), k=KNUM2 )
		re_fidx = [ np.where(cumfac[re_idx2[i]] - X[redo][i] >= 0.0)[0][0]
					for i in range(X[redo].size) ]
		# Artificially set "correct" index to be that in array element 0; then, 
		# set that element to correct value found from larger set of neighbours 
		# (in fidx array)
		for i in range(len(re_fidx)):
			fidx[redo[i]] = np.array([0])
			idx2[redo[i]][0] = re_idx2[i][re_fidx[i]]

		# Produce final set of indices
		idx = np.array([idx2[i][fidx[i][0]] for i in range(len(idx2))])
		
		# Basic version of this (slow for large cumfac and large X.size
		#idx = [ np.where((cumfac - x) >= 0., (cumfac - x), BIGNUM).argmin()
		#		for x in X ]
		return idx


