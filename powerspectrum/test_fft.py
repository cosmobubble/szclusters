#!/usr/bin/python
"""Generate a Gaussian random field with some power spectrum, following an 
   algorithm that Pedro wrote down."""

import numpy as np
import scipy.fftpack as fft
import cosmolopy as cp
import pylab as P
import time
tstart = time.time()
np.random.seed(12)

# Define cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.72,
		 'n' : 0.96,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}
NSAMP = 64 # Grid size. Should be some power of two, ideally.

# Generate 3D Gaussian random field in real space
x = np.linspace(-2e3, 2e3, NSAMP) # Symmetric 3D grid, in Mpc
N = x.size # Dimension of grid
_delta_x = np.random.randn(N, N, N)


# FT the field, respecting reality conditions on the FFT
# See Ch.12, "Fourier Transform Spectral Methods" in Numerical Recipes 
# (Fortran, 1986) for reality conditions on Fourier transform.
# Require delta*(x) = delta(x) and delta*(-k) = delta(k)
# => delta(x) is real
# Presumably, need to sample in k space from [kmax, -kmax]?
_delta_k = fft.fftn(_delta_x) # 3D discrete FFT


# Get sample spacing. In 1D, k_n = 2pi*n / (N*dx), where N is length of 1D array
# In 3D, k_n1,n2,n3 = [2pi / (N*dx)] * sqrt(n1^2 + n2^2 + n3^2)
# FIXME: Is this right?
def kscale(x):
	"""Get the scale in frequency (k) space."""
	
	# Scale of real-space grid
	N = x.size
	dX = x[-1] - x[0]
	fac = 2.*np.pi/dX # Overall factor (Mpc^-1)
	
	# Get 3D grid of k-vector amplitudes
	# Need slightly special FFT freq. numbering order, given by fftfreq()
	NN = N*fft.fftfreq(N, 1.)
	kk = [[[np.sqrt(i**2. + j**2. + k**2.) for i in NN] for j in NN] for k in NN]
	kk = fac * np.array(kk)
	return kk
	
	# FIXME: This would be a more efficient method to implement
	# (1) Make ordered 1D array of unique k-vector amplitudes (for calculating P(k))
	# (2) Write function to populate 3D array with correct P(k) amplitudes
	# TODO


# Calculate linear power spectrum, P(k)
kk = kscale(x)
k = kk.flatten() # FIXME: Lots of duplicates. Can be more efficient.
ps = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
ps = np.reshape(ps, np.shape(kk))
ps = np.nan_to_num(ps)
print "\n\n\n"


# Multiply by sqrt of power spectrum and transform back to real space
delta_k = np.sqrt(ps) * _delta_k
delta_x = fft.ifftn(delta_k) # 3D discrete FFT
##delta_x = np.real_if_close(delta_x) # Remove imag. parts that are precision artefacts
delta_x = np.real(delta_x)



################################################################################
# Plot the results
################################################################################


# Plot results

P.subplot(111)
P.imshow(delta_x[0], interpolation='bilinear', extent=(min(x), max(x), min(x), max(x)))

"""
# Make histogram
# Get flattened power spectrum
xx_delta_k = fft.fftn(delta_x)
psexp = (np.abs(xx_delta_k).flatten()**2.)
#psexp = psexp / np.max(psexp)

# Get bin masks
edges = np.linspace(0.0, np.max(k), 40)
dk = edges[1] - edges[0]
centroids = [0.5 * (edges[i+1] + edges[i]) for i in range(edges.size - 1)]
m = [np.where((k >= edges[i]) & (k < edges[i+1]), True, False) for i in range(edges.size - 1)]

# Get averaged value in each bin
vals = np.array(  [np.sum(psexp[mm]) / np.sum(mm) for mm in m]  )
vals = vals / np.max(vals)


# TESTING
P.subplot(111)
P.plot(centroids, vals, 'b+')
#P.plot(centroids, vals2, 'gx')
P.plot(k, ps.flatten() / np.max(ps.flatten()), 'r.')
#P.plot(k, ps.flatten(), 'r.')
"""

# Output timing information
print "Run took", round(time.time() - tstart, 1), "sec."
P.show()
