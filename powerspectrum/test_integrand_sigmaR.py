#!/usr/bin/python
"""Plot integrand of sigma_R within some window."""

import numpy as np
import pylab as P
import scipy.integrate
import cosmolopy as cp


# Define default cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_k_0' : 0.0,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.8,
		 'baryonic_effects': True
		}

# Box size / smoothing scale
L = 4000. # In Mpc
NSIDE = 64

# Get kmax and kmin, max. and min. sample values for k
kmax = 2. * np.pi * NSIDE / L
kmin = 2. * np.pi / L

# Window function
R = 10.0 # in Mpc
window = lambda k, R: (3. / (k*R)**3.) * ( np.sin(k*R) - (k*R)*np.cos(k*R) )

# Get full theory power spectrum
k = np.logspace(-4, 1, 5e2)
pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
y = pk * k**2. * window(k, R)**2. # Integrand samples
y = np.nan_to_num(y) # Remove NaNs

# Get theory power spectrum only in defined k range
k1 = np.logspace(np.log10(kmin), np.log10(kmax), 1e3)
pk1 = cp.perturbation.power_spectrum(k1, z=0.0, **cosmo)
y1 = pk1 * k1**2. * window(k1, R)**2. # Integrand samples
y1 = np.nan_to_num(y1) # Remove NaNs

# Get sigma_r
sigma_r, err = cp.perturbation.sigma_r(R, z=0.0, **cosmo)

# Get sigma8
sigma8 = scipy.integrate.simps(y1, k1) / (2. * np.pi**2.)
print "\nsigmaR (calc.) =", round(np.sqrt(sigma8), 4)
print "sigmaR (actu.) =", round(sigma_r, 4)
print "factor =", round(sigma_r/ np.sqrt(sigma8), 4)

# Plot results
P.subplot(111)
P.plot(k, y, 'r-')
P.plot(k1, y1, 'b-')
P.axvline(kmax, ls='solid', color='k')
P.axvline(kmin, ls='solid', color='k')

P.xlabel("k [Mpc^-1]")
P.ylabel("P(k) k^2 W(k, R)")
P.xscale('log')
P.show()
