#!/usr/bin/python
"""Use Hu's fitting function to plot cluster number counts (astro-ph/0301416)."""

import numpy as np
import scipy.integrate
#import scipy.fftpack as fft
import cosmolopy as cp
import pylab as P
from units import *

# Define default cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.8,
		 'baryonic_effects': True
		}


def rho_m():
	"""Mean matter density today, in solar masses per Mpc^3."""
	return 3. * (cosmo['h']*fH0)**2. * cosmo['omega_M_0'] / (8. * np.pi * G)

def window(k, R):
	"""Fourier transform of tophat window function, used to calculate 
	   sigmaR etc. See "Cosmology", S. Weinberg, Eq.8.1.46."""
	x = k*R
	f = (3. / x**3.) * ( np.sin(x) - x*np.cos(x) )
	return f**2.

def sigmaR(R):
	"""Get variance of matter perturbations, smoothed with a tophat filter 
	   of radius R h^-1 Mpc."""
	# FIXME: Worry about h^-1 Mpc units
	
	# Discretely-sampled integrand, y
	y = k**2. * pk * window(k, R/cosmo['h'])
	I = scipy.integrate.simps(y, k)
	
	# Return sigma_R (note factor of 4pi / (2pi)^3 from integration)
	return np.sqrt( I / (2. * np.pi**2.) )

def sigmaM(M):
	"""Get variance of matter perturbations, smoothed with tophat that encloses 
	   mass M at mean matter density today, rho_m. (See Hu.)"""
	# Convert enclosed mass M to smoothing radius R
	R = ( (3. * M) / (4. * np.pi * rho_m()) )**(1./3.)
	return sigmaR(R) # Get corresponding sigma(R)

def dn_dlnM(M):
	"""Get halo number counts as a function of mass scale (see Hu, Eq. 1, for 
	   fitting fn.)."""
	
	# Get masses and variances
	dlnM = 1e-2
	M2 = np.exp(np.log(M) + dlnM)
	sm = sigmaM(M); sm2 = sigmaM(M2)
	
	# Calculate derivative dln(sigma^-1)/dlnM
	dd = -1. * ( np.log(sm2) - np.log(sm) ) / dlnM
	
	return (0.3*rho_m() / M) * dd * np.exp(-np.abs(0.64 - np.log(sm))**3.82)



# Get power spectrum, sampled in log(k) space
k = np.logspace(-5., 1., 1e4)
pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)

# Get sigma(M)
M = np.logspace(13., 16., 150)
sm = map(lambda x: sigmaM(x), M)

# Get dn/dlnM and n(M >= M*)
dn = map(lambda x: dn_dlnM(x), M)
lnM = np.log(M)

nn = []
for i in range(len(lnM)):
	nn.append( scipy.integrate.simps(dn[i:], lnM[i:]) ) # Normalisation?
nn = np.array(nn)
