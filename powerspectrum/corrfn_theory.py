#!/usr/bin/python
"""Test code to calculate the correlation function."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import realise_matterpower as R
import pylab as P
import time
tstart = time.time()
np.seterr(all='ignore') # Because of some annoying div/0 warnings.

# Define default cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}

# Get theoretical power spectrum and correlation function
#k = np.linspace(1e-6, 1e1, 1e4)
th_k = np.logspace(-3, 1.5, 1e4)
th_r = np.linspace(1.0, 200./0.7, 60) # Correlation lengths

z = [0.0, 0.1, 0.2, 0.5]

def corr(zz):
	th_pk = cp.perturbation.power_spectrum(th_k, z=zz, **cosmo)	
	th_corr = []
	fk_list = []
	for rr in th_r:
		fk = th_k * th_pk * np.sin(th_k*rr) / (rr) # Integrand
		_corr = scipy.integrate.simps(fk, th_k) / (2.*np.pi**2.)
		th_corr.append(_corr)
		fk_list.append(fk)
	th_corr = np.array(th_corr)
	return th_corr

cc = [corr(zz) for zz in z]

# Correlation function
P.subplot(111)
for c in cc:
	P.plot(th_r*0.7, c)
P.ylabel("Corr. Fn.")
P.xlabel("r [Mpc]")
#P.yscale('log')
P.ylim((-0.005, 0.02))
P.xlim((40., 180.))

"""
# Integrand of correlation function
P.subplot(212)
P.plot(th_k, fk_list[0], 'b-')
P.plot(th_k, fk_list[2], 'g-')
P.plot(th_k, fk_list[10], 'r-')
P.axhline(0.0, ls='dashed', color='k')
P.ylabel("Corr. Fn. Integrand")
P.xlabel("k")
P.xscale('log')
"""

P.show()
