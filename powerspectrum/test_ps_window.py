#!/usr/bin/python
"""Try doing things slightly differently."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import pylab as P

# Define default cosmology
DEFAULT_COSMOLOGY = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.8,
		 'baryonic_effects': True
		}

def fft_sample_spacing(x):
	"""Calculate the sample spacing in Fourier space, given some symmetric 3D 
	   box in real space, with 1D grid point coordinates 'x'."""
	N = x.size # Dimension of grid
	L = x[-1] - x[0]
	fac = 2.*np.pi/L # Overall factor (Mpc^-1)
	
	# Need slightly special FFT freq. numbering order, given by fftfreq()
	NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
	
	# Get sample spacing. In 1D, k_n = 2pi*n / (N*dx), where N is length of 1D array
	# In 3D, k_n1,n2,n3 = [2pi / (N*dx)] * sqrt(n1^2 + n2^2 + n3^2)
	kk = [[[np.sqrt(i**2. + j**2. + k**2.) for i in NN] for j in NN] for k in NN]
	kk = fac * np.array(kk)
	return kk


def test_fft_sample_spacing(x):
	"""Calculate the sample spacing in Fourier space, given some symmetric 3D 
	   box in real space, with 1D grid point coordinates 'x'."""
	N = x.size # Dimension of grid
	L = x[-1] - x[0]
	fac = 2.*np.pi/L # Overall factor (Mpc^-1)
	
	# Need slightly special FFT freq. numbering order, given by fftfreq()
	NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
	dx = L/N
	_k = 2.*np.pi*fft.fftfreq(N, dx)
	
	# Get sample spacing. In 1D, k_n = 2pi*n / (N*dx), where N is length of 1D array
	# In 3D, k_n1,n2,n3 = [2pi / (N*dx)] * sqrt(n1^2 + n2^2 + n3^2)
	print NN
	kk = [i for i in NN]
	kk = fac * np.array(kk)
	return kk, _k

def window(k, R):
	"""Window function, which filters the FT. Used in sigma8 calculation."""
	# See Weinberg, Eq.8.1.46
	x = k*R
	f = (3. / x**3.) * ( np.sin(x) - x*np.cos(x) )
	return f**2.


scale = 5e3
nsamp = 8
x = np.linspace(-scale, scale, nsamp) # 3D grid coords, in Mpc
L = x[-1] - x[0]
dx = L/nsamp

ksamp = fft_sample_spacing(x) # Get grid coordinates in k-space
ksamp = ksamp.flatten()

# Calculate matter power spectrum
k = np.logspace(-5, 2, 1e3)
ps = cp.perturbation.power_spectrum(k, z=0.0, **DEFAULT_COSMOLOGY)
ps = np.nan_to_num(ps) 
ps2 = ps * window(k, 8.0/0.7)
ps3 = ps * window(k, 50.0/0.7)

P.subplot(111)
P.plot(k, ps, 'k-')
P.plot(k, ps2, 'g-')
P.plot(k, ps3, 'm-')
for kk in ksamp:
	P.axvline(kk, color='r')
P.axvline(2.*np.pi/L, color='b')
P.axvline(2.*np.pi*(0.5*np.sqrt(3.)*nsamp)/L, color='b', ls="dashed")
P.xscale('log')
#P.yscale('log')
P.show()
