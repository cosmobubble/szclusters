#!/usr/bin/python
"""Output 3D density fluctuation box in a serialised binary format, to be 
efficiently read by vfield.c."""

import numpy as np

BOX_NAME = "testbox-64-4000" # FIXME: Remove after testing and just pass arg. to fn below.
OUT_NAME = BOX_NAME + "-delta_x.binary"

# Load delta_x data
deltax_filename = BOX_NAME + ".delta_x.npy"
delta_x = np.load(deltax_filename)

# Output in binary format that can be loaded by C program
# NOTE: tofile() produces files that aren't necessarily portable between 
# machines (endianness etc. is an issue)
print "Array size:", delta_x.size
delta_x.flatten().tofile(OUT_NAME)
# Double precision "d":
# http://docs.scipy.org/doc/numpy/reference/generated/numpy.typename.html#numpy.typename
