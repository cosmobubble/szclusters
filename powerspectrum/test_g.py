#!/usr/bin/python
"""Test the normalisation that I derived for the discrete FFT power spectrum."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import pylab as P

np.random.seed(18)

# Define default cosmology
DEFAULT_COSMOLOGY = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}

def fft_sample_spacing(x):
	"""Calculate the sample spacing in Fourier space, given some symmetric 3D 
	   box in real space, with 1D grid point coordinates 'x'."""
	N = x.size # Dimension of grid
	fac = 2.*np.pi/(x[1]-x[0]) # Overall factor (Mpc^-1)
	
	# Need slightly special FFT freq. numbering order, given by fftfreq()
	NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
	
	# Get sample spacing. In 1D, k_n = 2pi*n / (N*dx), where N is length of 1D array
	# In 3D, k_n1,n2,n3 = [2pi / (N*dx)] * sqrt(n1^2 + n2^2 + n3^2)
	kk = [[[np.sqrt(i**2. + j**2. + k**2.) for i in NN] for j in NN] for k in NN]
	kk = fac * np.array(kk)
	return kk


def realise(cosmo=DEFAULT_COSMOLOGY, scale=2e3, nsamp=32):
	"""Create realisation of the matter power spectrum for some regular grid of 
	   side NSAMP, with cosmology 'cosmo'."""
	
	# Generate 3D Gaussian random field in real space
	x = np.linspace(-scale, scale, nsamp) # 3D grid coords, in Mpc
	delta_xi = np.random.randn(x.size, x.size, x.size)

	# FT the field, respecting reality conditions on FFT (Ch12, Num. Rec.)
	delta_ki = fft.fftn(delta_xi) # 3D discrete FFT
	kk = fft_sample_spacing(x) # Get grid coordinates in k-space
	
	# Calculate matter power spectrum
	k = kk.flatten() # FIXME: Lots of duplicate values. Can be more efficient.
	ps = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
	norm = cp.perturbation.norm_power(**cosmo)
	ps = np.reshape(ps, np.shape(kk))
	ps = np.nan_to_num(ps) # Remove NaN at k=0 (and any others...)

	# Multiply by sqrt of power spectrum and transform back to real space
	# FIXME: 'norm' should be here, or not?
	delta_k = delta_ki
	delta_x = np.real(  fft.ifftn(delta_k)  ) # Discard small imag. values
	
	return x, kk, delta_x, delta_k, delta_xi

x, kk, delta_x, delta_k, delta_xi = realise()

print "Initial:", np.std(delta_xi)**2.
print "Back:", np.std(delta_x)**2.


