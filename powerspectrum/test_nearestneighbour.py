#!/usr/bin/python
"""Test different nearest-neighbour identification algorithms."""

import numpy as np
import scipy.interpolate
import scipy.spatial
import pylab as P
import cProfile

np.random.seed(10)
Y = np.random.randn(100000)
X = np.random.rand(5000)

cumfac = np.cumsum(Y) # Cum. sum
cumfac = cumfac / np.max(cumfac)


# Method 1 (slow)

#print "-"*50
#print "(1) argmin()"
#cProfile.run("""
#idx = np.array([np.where((cumfac - x) >= 0., (cumfac - x), 1e10).argmin() for x in X])
#print idx
#""")


# Method 2 (fastest, but wrong)
print "-"*50
print "(2) cKDTree (basic)"
cProfile.run("""
tree = scipy.spatial.cKDTree( cumfac.reshape((cumfac.size, 1)) )
dists, idx2 = tree.query( X.reshape(X.size, 1) ) # Query expects certain input shape
""")

################################################################################
# Method 3 (fast, more complicated, but correct)
print "-"*50
print "(3) cKDTree (refined)"

# Get top 100 nearest neighbours for each point (ordered by distance)
# No. of nearest neighbours to return. Large KNUM means fewer redo's, but 
# longer non-redo execution time.
KNUM = 10
KNUM2 = 150
tree = scipy.spatial.cKDTree( cumfac.reshape((cumfac.size, 1)) )
dists, idx2 = tree.query( X.reshape(X.size, 1), k=KNUM ) # Get a sample of 10
fidx = [np.where(cumfac[idx2[i]] - X[i] >= 0.0)[0] for i in range(X.size)]

# Check for rows where a nearest neighbour wasn't found
fsize = np.array([len(row) for row in fidx])
redo = np.where(fsize == 0)[0]
print "Redo's", redo.size

# Redo selected ones by drawing larger sets of neighbours
re_dists, re_idx2 = tree.query( X[redo].reshape(X[redo].size, 1), k=KNUM2 )
re_fidx = [np.where(cumfac[re_idx2[i]] - X[redo][i] >= 0.0)[0][0] for i in range(X[redo].size)]
for i in range(len(re_fidx)):
	# Artificially set "correct" index to be that in array element 0; then, set 
	# that element to the correct value found from the larger set of neighbours
	fidx[redo[i]] = np.array([0])
	idx2[redo[i]][0] = re_idx2[i][re_fidx[i]]

# Produce final set of indices
idx3 = np.array([idx2[i][fidx[i][0]] for i in range(len(idx2))])

###############################################################################
