#!/usr/bin/python
"""Test code to calculate the correlation function."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import realise_matterpower as R
import pylab as P
import time
tstart = time.time()
np.seterr(all='ignore') # Because of some annoying div/0 warnings.

# Define default cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.79,
		 'baryonic_effects': True
		}

# Get theoretical power spectrum and correlation function
#k = np.linspace(1e-6, 1e1, 1e4)
th_k = np.logspace(-6, 1, 1e4)
th_pk = cp.perturbation.power_spectrum(th_k, z=0.0, **cosmo)
th_r = np.linspace(1.0, 200., 60) # Correlation lengths
th_corr = []
for rr in th_r:
	fk = th_k**2. * th_pk * np.sin(th_k*rr) / (th_k*rr) # Integrand
	_corr = scipy.integrate.simps(fk, th_k) / (2.*np.pi**2.)
	th_corr.append(_corr)


# Get realisation of density field
x, k, delta_x, delta_k, pk, norm = R.realise(scale=5e1, nsamp=8)

def i31(i, j, k, N):
	"""Convert an (i,j,k) 3D index to a 1D index."""
	return N**2 * k + N*j + i

def i13(alpha, N):
	"""Convert a 1D index to a 3D (i,j,k) index."""
	ii = alpha % N
	jj = (alpha - ii)/N % N
	kk = (alpha - ii - N*jj)/N**2 % N
	return ii, jj, kk

def corr(x, delta):
	"""Calculate distances and correlations between each pixel."""
	
	# 2D array of distances between every pixel
	N = x.size
	NN = range(N**3)
	xx = [ x[i13(m,N)[0]] for m in NN ]
	yy = [ x[i13(m,N)[1]] for m in NN ]
	zz = [ x[i13(m,N)[2]] for m in NN ]
	
	# Calculate pairwise distances
	d2 = [[ (xx[m]-xx[n])**2. + (yy[m]-yy[n])**2. + (zz[m]-zz[n])**2. 
			for m in NN] for n in NN ]
	d = np.sqrt( np.array(d2) )
	
	# Calculate correlations
	dx = delta_x.flatten()
	cc = [[ dx[m]*dx[n] for m in NN] for n in NN ]
	cc = np.array(cc)
	
	return d, cc

def bin_corr(r, cc, bins=10):
	"""Bin the correlation matrix in some manner."""
	
	# Flatten before doing calculation
	r = r.flatten()
	cc = cc.flatten()
	
	# Get maximum distance scale to probe
	#rmax = np.max(r)
	rmax = 200. # 200 Mpc
	
	# Get bin masks
	edges = np.linspace(0.0, rmax, bins)
	dr = edges[1] - edges[0]
	centroids = [0.5 * (edges[i+1] + edges[i]) for i in range(edges.size - 1)]
	m = [np.where((r >= edges[i]) & (r < edges[i+1]), True, False) for i in range(bins-1)]

	# Get averaged value in each bin
	vals = np.array(  [np.sum(cc[mm]) / np.sum(mm) for mm in m]  )
	return np.array(centroids), vals

# Get 2-point correlations
dd, cc = corr(x, delta_x)

# Bin the correlation function
rval, cval = bin_corr(dd, cc, bins=20)

# Get binned power spectrum for realisation
r_k, r_pk = R.realisation_ps(x, delta_x, bins=40)

# Get normalisation correction factor
r_s8 = R.realisation_sigma8(r_k, r_pk)
factor = (0.79/r_s8)
print factor

# Plot the results
P.subplot(211)
P.plot(rval, abs(cval), 'r-')
P.plot(rval, abs(cval), 'r+')
P.plot(th_r, abs(th_corr/(factor**2.)), 'b-')
P.axhline(0.0, ls="dashed", color='k')
P.yscale('log')

P.subplot(212)
P.hist(dd.flatten(), bins=20)



print "Run took", round(time.time() - tstart,2), "sec."
P.show()
