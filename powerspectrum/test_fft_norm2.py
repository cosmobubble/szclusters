#!/usr/bin/python

import pylab as P
import numpy as np
import scipy.fftpack as fft
import cosmolopy as cp

cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.8,
		 'baryonic_effects': True
		}

# Box parameters
N = 48
L = 8e3

# Get FFT frequencies
kx = np.zeros((N,N,N))
ky = np.zeros((N,N,N))
kz = np.zeros((N,N,N))
NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
for i in NN:
	kx[i,:,:] = i
	ky[:,i,:] = i
	ky[:,:,i] = i
k = np.sqrt(kx**2. + ky**2. + kz**2.) * (2.*np.pi*N/L)

	
# Get power spectrum
kk = k.flatten()
pk = cp.perturbation.power_spectrum(kk, z=0.0, **cosmo)
pk = np.reshape(pk, np.shape(k))
pk = np.nan_to_num(pk)

# Normalise the power spectrum properly
pk = pk * (N**6.) / L**3.

# Get random Gaussian fields
re = np.random.normal(0.0, 1.0, np.shape(k))
im = np.random.normal(0.0, 1.0, np.shape(k))
kfield = ( re + 1j*im ) * np.sqrt(pk)
kfield = kfield.real # Save only the real part

# Get the real field back by FT
rfield = fft.ifftn(kfield)

print np.std(rfield)
