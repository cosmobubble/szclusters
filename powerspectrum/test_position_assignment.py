#!/usr/bin/python

import numpy as np
import pylab as P

np.random.seed(11)

N = 5
delta = 100. * np.random.randn(N,N,N)
idxs = np.argsort(delta.flatten())

_delta = delta.flatten()
fac = np.where(_delta.flatten() >= -1.0, 1.+_delta, 0.)
ii = np.arange(fac.size)

# Probability inegral transform
cumfac = np.cumsum(fac[idxs]) # Cum. sum
cumfac = cumfac / np.max(cumfac) # Normalise

Ncl = 1000
# Find y = cumfac(x); => x is new random variable
# FIXME: Highest density region has anomalously low count.
idx = [(np.abs(cumfac - y)).argmin() for y in np.random.rand(Ncl)]
count = [np.sum(np.where(idx==i, 1, 0)) for i in ii]

# Plot results
P.subplot(111)
#P.hist(idx, bins=N**3)
P.plot(_delta[idxs], count, 'r+')
P.xlabel("Density (delta)")
P.ylabel("No. clusters in cell")
P.show()
