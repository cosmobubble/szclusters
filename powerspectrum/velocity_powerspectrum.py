#!/usr/bin/python
"""Plot theoretical velocity power spectrum, Pvv, and Pvdelta."""

import numpy as np
import cosmolopy as cp
import pylab as P
import scipy.integrate

# Define default cosmology
cosmo = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_k_0' : 0.0,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.8,
		 'baryonic_effects': True
		}

# Matter (density) power spectrum
k = np.logspace(-5, 1, 5e2)
pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)

# Window function
window = lambda k, R: (3. / (k*R)**3.) * ( np.sin(k*R) - (k*R)*np.cos(k*R) )
R = np.logspace(-1, 5, 50) # In Mpc

# Pre-factor for velocity power spectrum
f = cosmo['omega_M_0']**0.55 #cp.perturbation.fgrowth(z=0.0, omega_M_0=cosmo['omega_M_0'])
H = 100. * cosmo['h'] * cp.distance.e_z(z=0.0, **cosmo)
a = 1.0
fac = (f * a * H)**2. / (2. * np.pi**2.)

# Get V_RMS
vrms = np.array([scipy.integrate.simps(pk * window(k, RR)**2., k) * fac for RR in R])
vrms = np.sqrt(vrms)

# Get sigma(R)^2
sigma = np.array([cp.perturbation.sigma_r(RR, 0.0, **cosmo)[0] for RR in R])

P.subplot(211)
P.plot(R, sigma, 'k-')
#P.xlabel("Smoothing scale R [Mpc]")
P.ylabel("sigma(R)")
P.xscale('log')
#P.yscale('log')

P.subplot(212)
P.plot(R, vrms, 'r-')
P.xlabel("Smoothing scale R [Mpc]")
P.ylabel("v_rms [km/s]")
P.xscale('log')

P.show()
