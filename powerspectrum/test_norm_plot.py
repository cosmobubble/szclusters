#!/usr/bin/python
"""Manipulate the matter power spectrum."""

import numpy as np
import pylab as P

# [i, scals[i], Nsamp[i], s8_exp, s30_exp, s8_th, s30_th]
ii, scals, nsamp, s8e, s30e, s8t, s30t = np.genfromtxt("NormTest.dat", delimiter="\t").T

L = np.unique(scals)
nside = np.unique(nsamp)
Nsigma8 = [ [] for i in range(len(nside)) ]
Nsigma30 = [ [] for i in range(len(nside)) ]
Lsigma8 = [ [] for i in range(len(L)) ]
Lsigma30 = [ [] for i in range(len(L)) ]

# Get realisation sigmas, grouped by nsize, and by L
for i in ii:
	Nsigma8[ list(nside).index(nsamp[i]) ].append( s8e[i] )
	Nsigma30[ list(nside).index(nsamp[i]) ].append( s30e[i] )
	
	Lsigma8[ list(L).index(scals[i]) ].append( s8e[i] )
	Lsigma30[ list(L).index(scals[i]) ].append( s30e[i] )


# Plot results
# sigma8 vs. L
P.subplot(221)
P.plot(L/1e3, s8t[0]*np.ones(len(L)), 'k-', lw=1.5, label="Theory: "+str(round(s8t[0],2)))
for i in range(len(Nsigma8)):
	P.plot(L/1e3, (2.*np.pi**2.)**2. *np.array(Nsigma8[i])/(nside[i])**3., marker="o", label="nside="+str(nside[i]))
P.xlabel("Box Size, L [Gpc]")
P.ylabel("sigma8")
P.xlim((0.0, 25.))
P.legend(loc="upper left", prop={'size':'xx-small'})

# sigma30 vs. L
P.subplot(222)
P.plot(L/1e3, s30t[0]*np.ones(len(L)), 'k-', lw=1.5, label="Theory: "+str(round(s30t[0],2)))
for i in range(len(Nsigma30)):
	P.plot(L/1e3, Nsigma30[i], marker="o", label="nside="+str(nside[i]))
P.xlabel("Box Size, L [Gpc]")
P.ylabel("sigma30")
P.xlim((0.0, 25.))
P.legend(loc="upper left", prop={'size':'xx-small'})

# sigma8 vs. nside
P.subplot(223)
P.plot(nside, s8t[0]*np.ones(len(nside)), 'k-', lw=1.5, label="Theory: "+str(round(s8t[0],2)))
for i in range(len(Lsigma8)):
	P.plot(nside, Lsigma8[i], marker="o", label="L="+str(round(L[i]/1e3, 1)))
P.xlabel("NSIDE")
P.ylabel("sigma8")
P.xlim((0, 150))
P.legend(loc="upper left", prop={'size':'xx-small'})

# sigma30 vs. nside
P.subplot(224)
P.plot(nside, s30t[0]*np.ones(len(nside)), 'k-', lw=1.5, label="Theory: "+str(round(s30t[0],2)))
for i in range(len(Lsigma30)):
	P.plot(nside, Lsigma30[i], marker="o", label="L="+str(round(L[i]/1e3, 1)))
P.xlabel("NSIDE")
P.ylabel("sigma30")
P.xlim((0, 150))
P.legend(loc="upper left", prop={'size':'xx-small'})


P.show()
