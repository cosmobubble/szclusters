#!/usr/bin/python
"""Try doing things slightly differently."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import realise_matterpower as RMP
import pylab as P

# Define default cosmology
DEFAULT_COSMOLOGY = {
		 'omega_M_0' : 0.27,
		 'omega_lambda_0' : 0.73,
		 'omega_b_0' : 0.045,
		 'omega_n_0' : 0.0,
		 'N_nu' : 0,
		 'h' : 0.7,
		 'n' : 0.96,
		 'sigma_8' : 0.8,
		 'baryonic_effects': True
		}


def fft_sample_spacing(x):
	"""Calculate the sample spacing in Fourier space, given some symmetric 3D 
	   box in real space, with 1D grid point coordinates 'x'."""
	N = x.size # Dimension of grid
	L = x[-1] - x[0]
	#fac = 2.*np.pi/(x[1]-x[0]) # Overall factor (Mpc^-1)
	fac = 2.*np.pi/L # Overall factor (Mpc^-1)
	
	# Need slightly special FFT freq. numbering order, given by fftfreq()
	NN = ( N*fft.fftfreq(N, 1.) ).astype("i")
	
	# Get sample spacing. In 1D, k_n = 2pi*n / (N*dx), where N is length of 1D array
	# In 3D, k_n1,n2,n3 = [2pi / (N*dx)] * sqrt(n1^2 + n2^2 + n3^2)
	kk = [[[np.sqrt(i**2. + j**2. + k**2.) for i in NN] for j in NN] for k in NN]
	kk = fac * np.array(kk)
	return kk


def realise(cosmo=DEFAULT_COSMOLOGY, scale=2e3, nsamp=32):
	"""Create realisation of the matter power spectrum for some regular grid of 
	   side NSAMP, with cosmology 'cosmo'."""
	
	# Generate 3D Gaussian random field in real space, and find k values
	x = np.linspace(-scale, scale, nsamp) # 3D grid coords, in Mpc
	kk = fft_sample_spacing(x) # Get grid coordinates in k-space
	
	# Calculate matter power spectrum
	k = kk.flatten() # FIXME: Lots of duplicate values. Can be more efficient.
	ps = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
	ps = np.reshape(ps, np.shape(kk))
	ps = np.nan_to_num(ps) # Remove NaN at k=0 (and any others...)
	
	# Generate Gaussian random field with variance given by power spectrum
	delta_k = np.random.normal(0.0, 1.0, kk.size)
	delta_k = np.reshape(delta_k, np.shape(kk)) * np.sqrt(ps)
	norm = cp.perturbation.norm_power(**cosmo)
	
	# Transform to real space
	delta_x = fft.ifftn(delta_k)
	# FIXME: The real and imag. parts are of the same order!
	delta_x = np.real(delta_x) # Discard small(?) imag. values
	return x, kk, delta_x, delta_k, ps, norm


def window(k, R):
	"""Window function, which filters the FT. Used in sigma8 calculation."""
	# See Weinberg, Eq.8.1.46
	x = k*R
	f = (3. / x**3.) * ( np.sin(x) - x*np.cos(x) )
	return f**2.



# Make realisation
x, kk, delta_x, delta_k, pk, norm = realise(scale=5e2, nsamp=64)
N = x.size
L = x[-1] - x[0]

delta_x *=  np.sqrt(2. * N**3.)
aa = np.sum(delta_x**2.) # FIXME: Factor of 2, because IFFT complex!
bb = np.sum(np.abs(delta_k)**2.)
cc = np.sum(pk)

# Calc. sigma8
_k = np.unique( kk.flatten() )
_pk = cp.perturbation.power_spectrum(_k, z=0.0, **DEFAULT_COSMOLOGY)
_y = _k**2. * _pk * window(_k, 8.0/0.7)
_y = np.nan_to_num(_y)

# Calc. theoretical sigma8 in window
kmax = 2.*np.pi*0.5*np.sqrt(3.)*N/L
kmin = 2.*np.pi/L
_k2 = np.linspace(kmin, kmax, 5e3)
_pk2 = cp.perturbation.power_spectrum(_k2, z=0.0, **DEFAULT_COSMOLOGY)
_y2 = _k2**2. * _pk2 * window(_k2, 8.0/0.7)
_y2 = np.nan_to_num(_y2)

# Calc. full sigma8 (in sufficient window)
_k3 = np.logspace(-5, 2, 5e4)
_pk3 = cp.perturbation.power_spectrum(_k3, z=0.0, **DEFAULT_COSMOLOGY)
_y3 = _k3**2. * _pk3 * window(_k3, 8.0/0.7)
_y3 = np.nan_to_num(_y3)

print "\n"
print "sigma8_re =", np.sqrt(  scipy.integrate.simps(_y,_k) / (2. * np.pi**2.)  )
print "sigma8_th =", np.sqrt(  scipy.integrate.simps(_y2, _k2) / (2. * np.pi**2.)  )
print "sigma8_fu =", np.sqrt(  scipy.integrate.simps(_y3, _k3) / (2. * np.pi**2.)  )
print ""

# Parseval's theorem?
print "Corr. Fn. =\t", aa
print "Sum_k P_re(k) =\t", bb
print "Sum_k P_th(k) =\t", cc
print np.mean(delta_x), np.std(delta_x), np.max(delta_x), np.min(delta_x)
print ""

# Smooth on some scale, and then calculate stats
delta_k2 = fft.fftn(delta_x) / N**(3./2.)
print np.sum(np.abs(delta_k2)**2.)
print np.sum(delta_x**2.)

# Bin the realised P(k)
pkpk = np.abs(delta_k2)**2.
bins = np.logspace(np.log10(kmin), np.log10(kmax), 20) # k-space bins
cent = [0.5*(bins[j+1] + bins[j]) for j in range(bins.size-1)]
vals = np.zeros(bins.size)
stddev = np.zeros(bins.size)
idxs = np.digitize(kk.flatten(), bins)
for i in range(bins.size):
	ii = np.where(idxs==i, True, False)
	vals[i] = np.mean(pkpk.flatten()[ii])
	stddev[i] = np.std(pkpk.flatten()[ii])

# Plot
P.subplot(111)
P.plot(_k3, _pk3, 'b-')
#P.errorbar(bins, vals, yerr=stddev, fmt=".", color='r')
P.plot(cent, vals[1:], 'r.')
P.xscale('log')
P.yscale('log')
P.show()

#print np.mean(delta_x2), np.std(delta_x2), np.max(delta_x2), np.min(delta_x2)
