#!/usr/bin/python
"""Test variance of matter field."""

import numpy as np
import scipy.fftpack as fft
import pylab as P

window = lambda k, R: (3. / (k*R)**3.) * ( np.sin(k*R) - (k*R)*np.cos(k*R) )


def smoothed_field(R, kfield, k, N):
	"""Return a density field, smoothed by some window function, given its 
	   Fourier Transform, 'field'."""
	# FT of tophat window function. See "Cosmology", S. Weinberg, Eq.8.1.46.
	window = lambda k, R: (3. / (k*R)**3.) * ( np.sin(k*R) - (k*R)*np.cos(k*R) )
	
	# Convolve field with tophat window fn., in Fourier space.
	skfield = window(k, R) * kfield
	skfield = np.nan_to_num(skfield)
	
	# Do inverse transform to get real-space field back
	# (should be real, with very small imaginary parts that we can remove)
	rfield = fft.ifftn(skfield) * np.sqrt(N**3.)
	return rfield.real


# Load data files
delta_x = np.load("testbox-64-4000.delta_x.npy")
delta_k = np.load("testbox-64-4000.delta_k.npy")
k = np.load("testbox-64-4000.ksamp.npy")

# Get smoothed field
N = 64
R = 80.
delta_x_R = smoothed_field(R, delta_k, k, N)

# Get statistics
print "sigma  :", np.std(delta_x.flatten())
print "sigmaR:", np.std(delta_x_R.flatten())

# Plot results
P.subplot(111)
P.hist(delta_x.flatten(), bins=100, alpha=0.5)
P.hist(delta_x_R.flatten(), bins=100, alpha=0.5)
P.show()
