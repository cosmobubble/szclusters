#!/usr/bin/python
"""Test code to calculate variance of matter field."""

import numpy as np
import scipy.integrate
import scipy.fftpack as fft
import cosmolopy as cp
import realise_matterpower as R
np.seterr(all='ignore') # Because of some annoying div/0 warnings.
np.random.seed(12)


def window(k, RR):
	"""Window function, which filters the FT. Used in sigma8 calculation."""
	# See Weinberg, Eq.8.1.46
	x = k*RR
	f = (3. / x**3.) * ( np.sin(x) - x*np.cos(x) )
	return f

def sigma8(k, pk):
	"""Calculate sigma8 for a realisation of the power spectrum."""
	RR = 8. / 0.7 # 8 h^-1 Mpc # FIXME: h is hard-coded here.
	y = pk * (k * window(k, RR))**2. # Integrand samples
	#y = np.nan_to_num(y) # Remove NaNs
	s8_sq = scipy.integrate.simps(y, k) / (2. * np.pi**2.)
	return np.sqrt(s8_sq)


# Define default cosmology
cosmo = R.DEFAULT_COSMOLOGY

# Get theoretical power spectrum and sigma8's
k = np.logspace(-5, 2, 1e4)
pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
s1 = sigma8(k, pk)
s2 = cp.perturbation.sigma_r(r=8.0/0.7, z=0.0, **cosmo)[0]
s2a = np.sqrt( cp.perturbation.norm_power(**cosmo) )
print s2a


"""
# Get lots of realisations
NSIDE = [16, 16, 16, 24, 24, 24, 32, 32, 32]
SCALE = [1e3, 5e3, 2e4, 1e3, 5e3, 2e4, 1e3, 5e3, 2e4]
for i in range(len(NSIDE)):
	# Realise
	x, kk, delta_x, delta_k, ps, norm = R.realise(scale=SCALE[i], nsamp=NSIDE[i])
	L = x[-1] - x[0] # Length scale
	pk_real = np.abs(delta_k)**2. / L**3.
	s3 = R.discrete_realisation_sigma8(kk, pk_real)
	
	print NSIDE[i], "\t", SCALE[i], "\t", s3/s1, "\t", s3/s2
"""

NSIDE = 16
SCALE = 1e3
sigma = []; sigma0_x = []; sigma0_k = []
for i in range(1):
	# Realise
	x, kk, delta_x, delta_k, ps, norm = R.realise(scale=SCALE, nsamp=NSIDE)
	L = x[-1] - x[0] # Length scale
	N = x.size
	pk_real = np.abs(delta_k)**2.
	s3 = R.discrete_realisation_sigma8(kk, pk_real)
	s3a = np.sqrt(np.sum(pk_real))
	s4 = np.sqrt(np.sum(delta_x**2.))
	sigma.append(s3)
	sigma0_k.append(s3a)
	sigma0_x.append(s4)
	
	print round(s1,3), round(s2,3), round(s2a,6), round(s3,3), round(s3a/N**(3./2.),3), round(s4,3)
	
	print "\n", s4/s2a, s1*s2a
	

#print round(s1,3), round(s2,3), round(np.mean(sigma), 3), round(np.std(sigma), 4), round(np.mean(sigma_x), 8), round(np.std(sigma_x), 4)

