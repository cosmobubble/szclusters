#!/usr/bin/python
"""Plot REXCESS profiles as a function of x = r/R500."""

import numpy as np
from arnaud_profile import *
from load_rexcess import load_rexcess_catalogue
import pylab as P
import time
tstart = time.time()

# Take parameters of some cluster from Planck data (see arXiv:1204.2743, Tables A.1, D.1)
# and also Arnaud et al. 2010 (Table C.1)
# Cluster A68 (Planck, 1); Cluster RXC J2048.1-1750 (Arnaud, 2); Cluster RXC J2129.8-5048 (Arnaud, 3)
# Had to get z and M500 for (2) from Pratt et al. 0909.3776
# params: [alpha, beta, gamma, c500, P0, M500, r500, z]

# Load best-fit REXCESS pressure profiles
p = load_rexcess_catalogue()
C = [ArnaudProfile(pp) for pp in p]

# Plot Ysph as a function of radius
x = np.linspace(1e-2, 20., 80)
P.subplot(111)
for i in range(len(C)):
	r = x*C[i].r500
	yy = map(lambda x: C[i].Ysph(x)/1e-5, r)
	P.plot(x, yy/yy[-1], '0.5')
P.axvline(5.0, color='k', ls="--")
P.axhline(1.0, color='k', ls=":")
P.axhline(0.9, color='k', ls=":")
P.xlabel("$r/R_{500}$")
P.ylabel("$Y_{sph}(x)/Y_\infty$")
P.ylim((0.4, 1.05))



# Output timing information
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
