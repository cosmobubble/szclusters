#!/usr/bin/python
"""Plot LOS momentum statistics vs. the full statistic.
"""

import numpy as np
import pylab as P
from MockClusterCatalogue import MockClusterCatalogue
import pairwise_momentum_statistics
import time
tstart = time.time()

Ncl = 4000
rmax = 1000. # Max. coordinate sep., in Mpc
dr = 6.0 # Bin width, in Mpc

# Load cluster catalogue
cat = MockClusterCatalogue("virgo", Ncl)
#cat = MockClusterCatalogue("sehgal", Ncl)
#cat = MockClusterCatalogue("bull", Ncl)

print cat.M500[:10]

exit()

# Get LOS statistic and full statistic
# Takes about 1.5min to run with 5000 clusters (1min with 4000)
rbins, ppr, NN = pairwise_momentum_statistics.ppr_los(cat, rmax, dr)
#rbins2, ppr2, NN2 = pairwise_momentum_statistics.ppr_full(cat, rmax, dr)
print "Finished..."

# Save results
np.save("test_los_numpy", np.column_stack((rbins, ppr, NN)))
exit()

# Plot
P.subplot(211)
P.figtext(0.7, 0.2, "Ncl = "+str(Ncl))
P.axhline(0.0, ls="dotted", color="k")
P.plot(rbins, ppr, 'r.', label="LOS p(r)")
#P.plot(rbins, ppr2, 'b.', label="Full p(r)")
P.ylabel("Pairwise momentum <p(r)>")
P.xlabel("Comoving separation r [Mpc]")
#P.ylim((np.min(ppr)*1.2, 1000.))
P.legend(loc="lower right", prop={'size':'small'})

P.subplot(212)
P.plot(rbins[1:], NN[1:])

# Report on timing
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
