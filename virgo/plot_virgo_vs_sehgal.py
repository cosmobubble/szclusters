#!/usr/bin/python
"""Plot cluster statistics for both VIRGO and Sehgal simulations"""

import sys
sys.path.append("/home/phil/postgrad/sz_clusters/VelocityStats/installed/lib/python")
import numpy as np
import pylab as P
#import VelocityStats
from MockClusterCatalogue import MockClusterCatalogue

# Load cluster catalogue
L = 3000. / 0.7 # Mpc
Ncl = 20000
dr = 10.
cat = MockClusterCatalogue("virgo", Ncl)
cat2 = MockClusterCatalogue("sehgal", Ncl) # Sehgal catalogue already has vpec?


P.subplot(221)
P.hist(cat.vx, bins=50, alpha=0.3, fc='r')
P.hist(cat2.vx, bins=50, alpha=0.3, fc='b')
P.xlabel("vx")

P.subplot(222)
P.hist(cat.vy, bins=50, alpha=0.3, fc='r')
P.hist(cat2.vy, bins=50, alpha=0.3, fc='b')
P.xlabel("vy")

P.subplot(223)
P.hist(cat.vz, bins=50, alpha=0.3, fc='r')
P.hist(cat2.vz, bins=50, alpha=0.3, fc='b')
P.xlabel("vz")

P.subplot(224)
P.hist(cat.M500, bins=50, alpha=0.3, fc='r')
P.hist(cat2.M500, bins=50, alpha=0.3, fc='b')
P.xlabel("M_500")

P.show()
