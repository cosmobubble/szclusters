#!/usr/bin/python
"""Test (hopefully, more efficient) distance calculation with Scipy."""

import numpy as np
import scipy.spatial.distance
from MockClusterCatalogue import MockClusterCatalogue
import cProfile

np.random.seed(10)

Ncl = 500
cat = MockClusterCatalogue("virgo", Ncl)

# Calculate momenta of clusters
px = cat.M500*cat.vx
py = cat.M500*cat.vy
pz = cat.M500*cat.vz
p = np.column_stack((px,py,pz))
x = np.column_stack((cat.x, cat.y, cat.z))
scipy.spatial.distance.cdist(x, x)

rr = np.array( [np.sqrt(   (cat.x[i]-cat.x)**2.
							 + (cat.y[i]-cat.y)**2.
							 + (cat.z[i]-cat.z)**2. )
					 for i in range(cat.N)] )

rr2 = scipy.spatial.distance.cdist(x, x)

print np.shape(rr), np.shape(rr2)

print np.sum(rr - rr2)

exit()

# (1) Standard calculation
# For each cluster, calculate pairwise momentum with all other clusters
"""
rr = np.array( [np.sqrt(   (cat.x[i]-cat.x)**2.
						 + (cat.y[i]-cat.y)**2.
						 + (cat.z[i]-cat.z)**2. )
				 for i in range(cat.N)] )

pp = np.array( [ (    (px[i]-px)*(cat.x[i]-cat.x)
					+ (py[i]-py)*(cat.y[i]-cat.y)
					+ (pz[i]-pz)*(cat.z[i]-cat.z) ) / rr[i]
				for i in range(cat.N)] )
"""

# Unit pairwise direction vectors, rhat_ij = (r_i - r_j) / |r_i - r_j|
xi = np.array([cat.x,]*Ncl)
yi = np.array([cat.y,]*Ncl)
zi = np.array([cat.z,]*Ncl)
rij = np.dstack( (xi-xi.T, yi-yi.T, zi-zi.T)) # (N, N, 3)
rhatij = rij / np.array([np.sqrt(np.sum(rij*rij, axis=2)),]*3).T
rhatij = np.nan_to_num(rhatij)


exit()
# (2) Faster calculation
#rr = scipy.spatial.distance.cdist(x, x)
pixi = np.array([np.sum(p*xhat, axis=1),]*Ncl)
pirj = scipy.spatial.distance.cdist(p, xhat)
stat = pixi + pixi.T - pirj - pirj.T


print pp
print "-"*30
print  stat

