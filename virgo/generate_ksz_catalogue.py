#!/usr/bin/python
"""Generate mock cluster catalogue that can be used for studying kSZ effect.
   http://www.mpa-garching.mpg.de/galform/virgo/hubble/catalog.shtml [1]
"""

import numpy as np
import pylab as P
import cosmolopy as cp
import scipy.interpolate
import time
tstart = time.time()

# Clusters are mass-ordered. Don't want too many massive clusters in mock 
# survey, so start some way down the list. [???]
startcl = 0
Ncl = 1700 # Max. number of clusters to process (size of survey)
CATALOGUE_NAME = "ksz-mock-virgo.tsv"

################################################################################
# Define cosmology and load cluster catalogue
################################################################################

# Define cosmology from VIRGO LCDM simulation
cosmo = {
 'omega_M_0': 		0.3,
 'omega_lambda_0': 	0.7,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.9, # Note, high
 'baryonic_effects': True
}
# Cluster finder used was SO with delta_c = 200 [1]
# "\delta_c is RELATIVE TO CRITICAL DENSITY \rho_c(z) for both LCDM and tCDM"

# Define cluster catalogue parameters (from [1])
Mp = 2.25e12 / cosmo['h'] # Particle mass, in Msun
Lbox = 3000. / cosmo['h'] # Mpc, box size, from "Lightcone geometry: LCDM" table in [1]
fname = "lcdm.MS.msort12.bin.npy"

# File format (columns) [~1.5m clusters!]
# m = number of particles (particle mass is 2.22/2.25e12/h for tcdm/lcdm)
# zred = redshift
# sigma = measured 1D velocity dispersion 
# ip = parent flag
# x,y,z = cluster location in SURVEY COORDINATES in 0-1 units 
# vx,vy,vz= physical peculiar velocity in km/s
# [Spherical lightcone ("MS") has origin at (0.5,0.5,0.5) in survey coords.]
## m, zred, sigma, ip, x, y, z, vx, vy, vz = np.load(fname)

m, zred, sigma, ip, x, y, z, vx, vy, vz = np.load(fname).T[startcl:startcl+Ncl].T
#m, zred, sigma, ip, x, y, z, vx, vy, vz = np.load(fname)
M = m * Mp / 1e14 # Cluster mass, in units of 10^14 Msun (M = M_200)

################################################################################
# Get comoving distance, restframe (cosmological) redshift, peculiar redshift
################################################################################

x0 = 0.5; y0 = 0.5; z0 = 0.5 # origin
rco = np.sqrt((x-x0)**2. + (y-y0)**2. + (z-z0)**2.) * Lbox
zsamp = np.linspace(0., 1., 200)
rco_samp = cp.distance.comoving_distance(zsamp, **cosmo)
interp_z_rco = scipy.interpolate.interp1d(rco_samp, zsamp, kind='linear')
z_cosmo = interp_z_rco(rco) # Cluster rest-frame redshift

# Get LOS velocity and peculiar redshift
v_los = ( (x-x0)*vx + (y-y0)*vy + (z-z0)*vz ) * Lbox / rco
z_pec = v_los / 3e5 # C in km/s

# Get observed redshift
z_obs = z_cosmo + z_pec


################################################################################
# Calculate M_500 and R_500, given that we have M_200
################################################################################

# Used script plot_locuss_M200_M500.py to find relation between M_200, M_500 
# for the AMI/Locuss data set => M_200 ~ 2 x M_500 = 1.972 M_500
M500 = 0.5 * M # Approximate?

# Find R_500, based on mass (use fitting fn.)
# The VIRGO cluster finder output gives a radius, R, but the files are only 
# available on request.

def r500_spherical_collapse(M500, z):
	"""*Basic* estimate of R500, using the spherical collapse model."""
	# Expects M500 in 10^14 Msun/Mpc^3
	delta = 500.
	rho_m = cp.density.cosmo_densities(**cosmo)[1] * (1.+z)**3.
	return ( 3.*M500*1e14 / (4.*np.pi*delta*rho_m) )**(1./3.) # In Mpc

R500 = r500_spherical_collapse(M500, z_cosmo) # In Mpc


################################################################################
# Calculate angles of clusters on the sky
################################################################################

# Calculate angles on the sky
def get_angle(x, y, z, origin):
	"""Get the angle on the sky, for coordinates with respect to some origin."""
	x0, y0, z0 = origin
	dx = x-x0; dy = y-y0; dz = z-z0
	
	# Get phi
	phi = np.arctan(dy / dx)
	
	# Get theta, and deal with poles
	try:
		theta = np.arccos(dz / np.sqrt(dx**2. + dy**2. + dz**2.) )
	except:
		if dz >= 0.:
			theta = 0.0
		else:
			theta = np.pi
	return np.nan_to_num(theta), np.nan_to_num(phi)

theta, phi = get_angle(x, y, z, (x0, y0, z0))
theta *= 180./np.pi
phi *= 180./np.pi

################################################################################
# Output catalogue in MCXC format
################################################################################

ID = np.arange(M500.size) # Row IDs
hdr = "# ID\ttheta[deg]\tphi[deg]\tz_obs\tv_los[km/s]\tM500[10^14 Msun]\tR500[Mpc]"
fmtstr = "%4.1i\t%7.4f\t%7.4f\t%7.6f\t%7.1f\t%7.4f\t%7.4f"
dat = np.column_stack(   ( ID, theta, phi, z_obs, v_los, M500, R500 )  )
np.savetxt(CATALOGUE_NAME, dat, fmt=fmtstr, delimiter="\t")
print "Saved catalogue to", CATALOGUE_NAME


################################################################################
# Plot statistics
################################################################################

P.subplot(321)
P.hist(R500, bins=20)
P.xlabel("R500 [Mpc]")

P.subplot(322)
P.hist(zred, bins=20)
P.xlabel("z_red")

P.subplot(323)
P.hist(M500, bins=20)
P.xlabel("Mass, M500 (10^14 Msun)")

P.subplot(324)
P.hist(v_los)
P.xlabel("v_los [km/s]")

P.subplot(325)
P.hist(theta)
P.xlabel("theta [rad]")

P.subplot(326)
P.hist(phi)
P.xlabel("phi [rad]")

# Report on timing
print "Run took", round(time.time() - tstart, 2), "sec."
#P.show()
