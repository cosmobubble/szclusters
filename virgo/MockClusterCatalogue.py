#!/usr/bin/python
"""Load mock cluster catalogues from various sources."""
import numpy as np
import cosmolopy as cp


# Define cosmology from VIRGO LCDM simulation
VIRGO_COSMOLOGY = {
 'omega_M_0': 		0.3,
 'omega_lambda_0': 	0.7,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.9, # Note: high
 'baryonic_effects': True
}


class MockClusterCatalogue(object):

	def __init__(self, catname, Ncap=-1, random=False):
		"""Load a given mock catalogue. The columns should be in a standard 
		   format, as follows:
		    * theta (or ra) [deg, 0 -- +180]
		    * phi (or dec) [deg, -180 -- +180]
		    * zz (redshift)
		    * M500 (mass at delta=500) [10^14 Msun]
		    * R500 (radius at delta=500) [Mpc]
		    * x, y, z (comoving position) [Mpc]
		    * vx, vy, vz (physical velocity) [km/s]
		"""
		
		self.N = Ncap # Max. number of clusters to put in arrays
		self.random = random
		
		# Choose which catalogue to load
		if catname == "virgo":
			self.load_virgo()
		if catname == "sehgal":
			self.load_sehgal()
		if catname == "bull":
			self.load_bull()
	
	def load_virgo(self):
		"""Load the VIRGO Hubble Volume simulation mock cluster catalogue.
		   http://www.mpa-garching.mpg.de/galform/virgo/hubble/catalog.shtml [1]
		   http://www.mpa-garching.mpg.de/galform/virgo/hubble/ [2]"""
		
		# Load data file
		fname = "lcdm.MS.msort12.bin.npy"
		dat = np.load(fname)
		
		# File format (columns) [~1.5m clusters!]
		# m = number of particles (particle mass is 2.22/2.25e12/h for tcdm/lcdm)
		# zred = redshift
		# sigma = measured 1D velocity dispersion 
		# ip = parent flag
		# x,y,z = cluster location in SURVEY COORDINATES in 0-1 units 
		# vx,vy,vz= physical peculiar velocity in km/s
		# [Spherical lightcone ("MS") has origin at (0.5,0.5,0.5) in survey coords.]
		
		if self.N == -1:
			# Get all of the clusters
			m, zz, sigma, ip, x, y, z, vx, vy, vz = dat
		elif self.random:
			# Get a random selection of clusters, of size 'N'
			idxs = np.random.random_integers(0, np.shape(dat)[1], self.N)
			m, zz, sigma, ip, x, y, z, vx, vy, vz = dat.T[idxs].T
		else:
			# Get only the first 'N' clusters
			m, zz, sigma, ip, x, y, z, vx, vy, vz = dat.T[:self.N].T
		
		# Scale/fix variables as appropriate
		h = 0.7
		Mp = 2.25e12 / h # Particle mass, in Msun
		Lbox = 3000. / h # Mpc, box size, from "Lightcone geometry: LCDM" table in [1]
		# N.B. Lbox quoted online doesn't seem to tally with Table C10 in Evrard.
		M = m * Mp / 1e14 # In units of 10^14 Msun
		
		# Derive any parameters that weren't initially specified
		# Used script plot_locuss_M200_M500.py to find relation between 
		# M_200, M_500 for AMI/Locuss data => M_200 ~ 2 x M_500 = 1.972 M_500
		M500 = 0.5 * M # Approximate
		R500 = self.r500_spherical_collapse(M500, zz)
		x *= Lbox; y *= Lbox; z *= Lbox
		
		# Save in standard catalogue format
		self.ra = np.zeros(M.size); self.dec = np.zeros(M.size) # FIXME
		self.zz = zz
		self.M500 = M500; self.R500 = R500
		self.x = x; self.y = y; self.z = z
		self.vx = vx; self.vy = vy; self.vz = vz
		
		# Set size of data
		self.N = M.size
	
	
	
	def load_sehgal(self):
		"""Load the Sehgal et al. SZ cluster simulation catalogue.
   		   See http://lambda.gsfc.nasa.gov/toolbox/tb_sim_ov.cfm [1]"""
		
		# Load data file
		fname = "sehgal_clusters.npy"
		dat = np.load(fname)
		
		if self.N == -1:
			# Get all of the clusters
			zz, ra, dec, x, y, z, vx, vy, vz, M500, R500 = dat.T
		else:
			# Get only the first 'N' clusters
			zz, ra, dec, x, y, z, vx, vy, vz, M500, R500 = dat[:self.N].T
		
		# Scale/fix variables as appropriate
		M500 /= 1e14
		# FIXME: Ra, Dec?
		
		# Save in standard catalogue format
		self.ra = ra; self.dec = dec; self.zz = zz
		self.M500 = M500; self.R500 = R500
		self.x = x; self.y = y; self.z = z
		self.vx = vx; self.vy = vy; self.vz = vz
		
		# Set size of data
		self.N = M500.size
	
	
	
	def load_bull(self, L=3000., N=128):
		"""Load a cluster catalogue produced by my own density field 
		   realisation code."""
		
		# Define length scale for box
		scale = L / N
		
		# Load data file
		fname = "../powerspectrum/catalogue-box-128-3000.npy"
		dat = np.load(fname)
		
		if self.N == -1:
			# Get all of the clusters
			theta, phi, zz, M, x, y, z, vx, vy, vz = dat[idxs].T
		else:
			# Randomly select clusters from the catalogue
			Ntot = np.shape(dat)[0]
			idxs = np.random.randint(0, Ntot, Ncl) # Get random indices
			theta, phi, zz, M, x, y, z, vx, vy, vz = dat[idxs].T
		
		# Scale/fix variables as appropriate
		x *= scale; y *= scale; z *= scale
		theta = np.nan_to_num(theta); phi = np.nan_to_num(phi)
		M /= 1e14 # FIXME: Is this mass M500?
		# FIXME: RA, Dec?
		
		# Calculate derivative values
		R500 = self.r500_spherical_collapse(M, zz)
		
		# Save in standard catalogue format
		self.ra = theta; self.dec = phi; self.zz = zz
		self.M500 = M; self.R500 = R500
		self.x = x; self.y = y; self.z = z
		self.vx = vx; self.vy = vy; self.vz = vz
		
		# Set size of data
		self.N = M.size
	
	

	def r500_spherical_collapse(self, M500, z):
		"""Basic estimate of R500, using the spherical collapse model."""
		# Expects M500 in 10^14 Msun/Mpc^3
		delta = 500.
		rho0 = cp.density.cosmo_densities(**VIRGO_COSMOLOGY)[1]
		rho_m = rho0 * (1.+z)**3.
		return ( 3.*M500*1e14 / (4.*np.pi*delta*rho_m) )**(1./3.) # In Mpc

