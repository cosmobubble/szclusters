#!/usr/bin/python
"""Pairwise momentum statistics."""

import numpy as np
import scipy.spatial.distance

def ppr_full(cat, rmax=200., dr=10.):
	"""Calculate comoving pairwise momentum statistic, with full (x,y,z) 
	   information. See Ferreira et al. (1999) arXiv:astro-ph/9812456v2, Eq. 7, 
	   and Hand et al. (2012) arXiv:1203.4219, Eq. 1."""
	
	# Calculate momenta of clusters
	px = cat.M500*cat.vx
	py = cat.M500*cat.vy
	pz = cat.M500*cat.vz

	# For each cluster, calculate pairwise momentum with all other clusters
	# p = (p_i - p_j) . (r_i - r_j)
	x = np.column_stack((cat.x, cat.y, cat.z))
	rr = scipy.spatial.distance.cdist(x, x)
	rr[np.where(rr==0.0)] = 1e10 # Set to special val. so it's not included in histo.

	pp = np.array( [ (    (px[i]-px)*(cat.x[i]-cat.x)
						+ (py[i]-py)*(cat.y[i]-cat.y)
						+ (pz[i]-pz)*(cat.z[i]-cat.z) ) / rr[i]
					for i in range(cat.N)] )
	
	# Bin the results
	rbins = np.arange(0.01, rmax, dr)
	bin_idxs = np.digitize(rr.flatten(), rbins)
	
	# Calculate the final quantity
	ppr = np.array( [ np.mean( pp.flatten()[np.where(bin_idxs==i)] )
					   for i in range(rbins.size) ] )
	ppr = np.nan_to_num(ppr)
	
	NN = np.array( [ np.sum( np.ones(pp.size)[np.where(bin_idxs==i)] )
					   for i in range(rbins.size) ] )
	return rbins, ppr, NN



def ppr_los(cat, rmax=200., dr=10.):
	"""Calculate comoving pairwise momentum statistic, with only line of sight 
	   information. See Ferreira et al. (1999) arXiv:astro-ph/9812456v2, Eq. 7, 
	   and Hand et al. (2012) arXiv:1203.4219, Eqs. 2--3."""

	# Define origin (VIRGO)
	h = 0.7
	x0 = 0.5*3000./h; y0 = 0.5*3000./h; z0 = 0.5*3000./h

	# Calculate LOS vectors
	xl = cat.x - x0; yl = cat.y - y0; zl = cat.z - z0
	rl = np.sqrt( xl**2. + yl**2. + zl**2. )

	xhat = xl/rl
	yhat = yl/rl
	zhat = zl/rl

	# Calculate LOS momenta of clusters
	plx = cat.M500 * cat.vx * xhat
	ply = cat.M500 * cat.vy * yhat
	plz = cat.M500 * cat.vz * zhat
	pl = plx + ply + plz

	# Cluster comoving separation
	rr = np.array( [np.sqrt( (xl[i] - xl)**2. + (yl[i] - yl)**2. + (zl[i] - zl)**2. )
					for i in range(cat.N)] )


	# Cluster angular separation
	costheta = np.array( [xhat[i]*xhat + yhat[i]*yhat + zhat[i]*zhat
						  for i in range(cat.N)] )
	costheta = np.nan_to_num(costheta)
	
	# LOS weight matrix
	cij = np.array( [     0.5*(rl[i] - rl)*(1. + costheta[i])
						/ np.sqrt( rl[i]**2. + rl**2. - 2.*rl[i]*rl*costheta[i] )
					for i in range(cat.N)] )
	cij = np.nan_to_num(cij)

	# Weighted LOS pairwise momentum
	ppair = np.array( [(pl[i] - pl) * cij[i] for i in range(cat.N)] )
	ppair = np.nan_to_num(ppair)

	# Bin the results
	rbins = np.arange(0.01, rmax, dr)
	bin_idxs = np.digitize(rr.flatten(), rbins)

	# TESTING
	"""
	print "Sum(ppair):", [np.sum(ppair.flatten()[np.where(bin_idxs==i)]) for i in range(rbins.size) ]
	print "Sum(cij):", [np.sum(cij.flatten()[np.where(bin_idxs==i)]**2.) for i in range(rbins.size) ]
	print "N(bin):", [np.sum(np.ones(cij.size)[np.where(bin_idxs==i)]**2.) for i in range(rbins.size) ]
	"""
	# No. samples per bin
	NN = np.array( [ np.sum( np.ones(ppair.size)[np.where(bin_idxs==i)] )
					   for i in range(rbins.size) ] )
	
	ppr = np.array( [ np.sum(ppair.flatten()[np.where(bin_idxs==i)]) / 
					  np.sum(cij.flatten()[np.where(bin_idxs==i)]**2.)
					   for i in range(rbins.size) ] )
	ppr = np.nan_to_num(ppr)
	return rbins, ppr, NN


def ppr_los_zdist(cat, rmax=200., dr=10.):
	"""Calculate comoving pairwise momentum statistic, with only line of sight 
	   information. See Ferreira et al. (1999) arXiv:astro-ph/9812456v2, Eq. 7, 
	   and Hand et al. (2012) arXiv:1203.4219, Eqs. 2--3."""

	# Define origin (VIRGO)
	h = 0.7
	x0 = 0.5*3000./h; y0 = 0.5*3000./h; z0 = 0.5*3000./h

	# Calculate LOS vectors
	xl = cat.x - x0; yl = cat.y - y0; zl = cat.z - z0
	rl = np.sqrt( xl**2. + yl**2. + zl**2. )

	xhat = xl/rl
	yhat = yl/rl
	zhat = zl/rl
	
	# Rescale xl, yl, zl by the Hubble distance, d_H = cz/H0
	dH = cat.zz * 3e5 / 70. # in Mpc
	xl = xhat * dH
	yl = yhat * dH
	zl = zhat * dH
	rl = dH
	

	# Calculate LOS momenta of clusters
	plx = cat.M500 * cat.vx * xhat
	ply = cat.M500 * cat.vy * yhat
	plz = cat.M500 * cat.vz * zhat
	pl = plx + ply + plz

	# Cluster comoving separation
	rr = np.array( [np.sqrt( (xl[i] - xl)**2. + (yl[i] - yl)**2. + (zl[i] - zl)**2. )
					for i in range(cat.N)] )


	# Cluster angular separation
	costheta = np.array( [xhat[i]*xhat + yhat[i]*yhat + zhat[i]*zhat
						  for i in range(cat.N)] )
	costheta = np.nan_to_num(costheta)
	
	# LOS weight matrix
	cij = np.array( [     0.5*(rl[i] - rl)*(1. + costheta[i])
						/ np.sqrt( rl[i]**2. + rl**2. - 2.*rl[i]*rl*costheta[i] )
					for i in range(cat.N)] ) 
	cij = np.nan_to_num(cij)

	# Weighted LOS pairwise momentum
	ppair = np.array( [(pl[i] - pl) * cij[i] for i in range(cat.N)] )
	ppair = np.nan_to_num(ppair)

	# Bin the results
	rbins = np.arange(0.01, rmax, dr)
	bin_idxs = np.digitize(rr.flatten(), rbins)

	# No. samples per bin
	NN = np.array( [ np.sum( np.ones(ppair.size)[np.where(bin_idxs==i)] )
					   for i in range(rbins.size) ] )
	
	ppr = np.array( [ np.sum(ppair.flatten()[np.where(bin_idxs==i)]) / 
					  np.sum(cij.flatten()[np.where(bin_idxs==i)]**2.)
					   for i in range(rbins.size) ] )
	ppr = np.nan_to_num(ppr)
	return rbins, ppr, NN


