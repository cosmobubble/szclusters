#!/usr/bin/python
"""Plot the LOS pairwise momentum statistic with different distance measures.
"""

import numpy as np
import pylab as P
from MockClusterCatalogue import MockClusterCatalogue
import pairwise_momentum_statistics
import time
tstart = time.time()

Ncl = 4000
rmax = 2500. # Max. coordinate sep., in Mpc
dr = 10.0 # Bin width, in Mpc

# Load cluster catalogue
cat = MockClusterCatalogue("virgo", Ncl)
#cat = MockClusterCatalogue("sehgal", Ncl)
#cat = MockClusterCatalogue("bull", Ncl)

# Get LOS statistic
rbins, ppr, NN = pairwise_momentum_statistics.ppr_los(cat, rmax, dr)
rbins2, ppr2, NN2 = pairwise_momentum_statistics.ppr_los_zdist(cat, rmax, dr)

# Plot
P.subplot(211)
P.figtext(0.7, 0.2, "Ncl = "+str(Ncl))
P.axhline(0.0, ls="dotted", color="k")
P.plot(rbins, ppr, 'r.', label="LOS p(r)")
P.plot(rbins2, ppr2, 'b.', label="Hubble dist. p(r)")
P.ylabel("Pairwise momentum <p(r)>")
P.xlabel("Comoving separation r [Mpc]")
P.ylim((np.min(ppr)*1.2, 1000.))
P.legend(loc="lower right", prop={'size':'small'})

P.subplot(212)
P.plot(rbins[1:], NN[1:])

# Report on timing
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
