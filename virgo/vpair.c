/*
Calculate pairwise momenta for entire Virgo cluster catalogue
*/

#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

// Histogram properties
/*
const int Nbins = 10001; // Number of bins
const int Npairs = 10000; // No. of clusters to use in the analysis
const double dr = 0.0001; // Bin width
*/
const int Nbins = 501; // Number of bins = int(1/dr) + 1
const int Npairs = 4000; // No. of clusters to use in the analysis
const double dr = 0.002; // Bin width

struct datacols {
	double *M;
	double *x;
	double *y;
	double *z;
	double *vx;
	double *vy;
	double *vz;
};

double* load_column(char* fname, int* N){
	/// Load column of numbers from binary file
	// Open file and get file size
	FILE *f; int fsize;
	f = fopen(fname, "rb");
	fseek(f, 0L, SEEK_END);
	fsize = ftell(f);
	rewind(f);
	
	// Calculate no. of elements in array and allocate memory
	*N = fsize / sizeof(double);
	double* data = (double*)malloc(fsize);
	
	// Load all data into array and return pointer
	int status = fread(data, sizeof(double), *N, f);
	fclose(f);
	return data;
}


void calculate_pairwise_momenta(int N, struct datacols* d){
	// Calculate the pairwise momenta for every pair of clusters
	int i; int j; int k;
	double pij, rij;
	
	// Allocate memory for p(r) bins and N bins
	double* pbin = (double*)malloc(Nbins * sizeof(double));
	int* Nbin = (int*)malloc(Nbins * sizeof(int));
	double* pavgbin = (double*)malloc(Nbins * sizeof(double));
	for(i=0; i<Nbins; i++){
		*(pbin+i) = 0.0;
		*(Nbin+i) = 0;
		*(pavgbin+i) = 0.0;
	}
	
	// Loop through all pairs
	for(i=0; i<Npairs; i++){ // FIXME
	 for(j=0; j<Npairs; j++){
	   
	   // Calculate separation distance
	   rij = 
		  (*(d->x+i) - *(d->x+j)) * (*(d->x+i) - *(d->x+j))  // x^2
		+ (*(d->y+i) - *(d->y+j)) * (*(d->y+i) - *(d->y+j))  // y^2
		+ (*(d->z+i) - *(d->z+j)) * (*(d->z+i) - *(d->z+j)); // z^2
	   rij = sqrt(rij);
	   
	   // Calculate pairwise momenta
	   pij = 
		  ( (*(d->M+i) * *(d->vx+i)) - (*(d->M+j) * *(d->vx+j)) ) * (*(d->x+i) - *(d->x+j))
		+ ( (*(d->M+i) * *(d->vy+i)) - (*(d->M+j) * *(d->vy+j)) ) * (*(d->y+i) - *(d->y+j))
		+ ( (*(d->M+i) * *(d->vz+i)) - (*(d->M+j) * *(d->vz+j)) ) * (*(d->z+i) - *(d->z+j));
	   pij /= rij; // Dotted with unit vector
	   
	   // Add data to bin
	   k = (int)(rij / dr);
	   *(pbin+k) += pij;
	   *(Nbin+k) += 1;
	   
	 } // end j loop
	} // end i loop
	
	// Output basic info
	for(i=0; i<Nbins; i++){
		*(pavgbin+i) = *(pbin+i) / ((double) *(Nbin+i) );
		printf("%f\t%f\t%d\n", (double)i*dr*3000., *(pavgbin+i), *(Nbin+i));
	} // end output loop 
}




void calculate_los_pairwise_momenta(int N, struct datacols* d){
	// Calculate the LOS pairwise momenta for every pair of clusters
	// See Eqs. 2--3 in arXiv:1203.4219
	int i; int j; int k;
	double ri, rj, pri, prj, costheta, cij, rij;
	
	// Define origin (observer location)
	double x0 = 0.5 * 3000.;
	double y0 = 0.5 * 3000.;
	double z0 = 0.5 * 3000.;
	
	// Allocate memory for ppair(r) bins and initialise
	double* ppair_num = (double*)malloc(Nbins * sizeof(double));
	double* ppair_denom = (double*)malloc(Nbins * sizeof(double));
	double* ppair = (double*)malloc(Nbins * sizeof(double));
	int* Nbin = (int*)malloc(Nbins * sizeof(int));
	
	for(i=0; i<Nbins; i++){
		*(ppair_num+i) = 0.0;
		*(ppair_denom+i) = 0.0;
		*(ppair+i) = 0.0;
		*(Nbin+i) = 0;
	}
	
	// Loop through all pairs
	for(j=0; j<Npairs; j++){
	
	 // LOS momentum dot product (j)
	 rj = sqrt(   (*(d->x+j) - x0)*(*(d->x+j) - x0)
				  + (*(d->y+j) - y0)*(*(d->y+j) - y0)
				  + (*(d->z+j) - z0)*(*(d->z+j) - z0)  );
	 
	 prj = (  *(d->vx+j) * (*(d->x+j) - x0)
			  + *(d->vy+j) * (*(d->y+j) - x0)
			  + *(d->vz+j) * (*(d->z+j) - x0)  ) * *(d->M+j) / rj;
	 
	 // Begin i loop
	 for(i=0; i<j; i++){
	   
	   // LOS momentum dot product (i)
	   ri = sqrt(   (*(d->x+i) - x0)*(*(d->x+i) - x0)
				  + (*(d->y+i) - y0)*(*(d->y+i) - y0)
				  + (*(d->z+i) - z0)*(*(d->z+i) - z0)  );
	   pri = (  *(d->vx+i) * (*(d->x+i) - x0)
			  + *(d->vy+i) * (*(d->y+i) - y0)
			  + *(d->vz+i) * (*(d->z+i) - z0)  ) * *(d->M+i) / ri;
	   
	   // Angle between LOS vectors
	   costheta = (   (*(d->x+i) - x0) * (*(d->x+j) - x0)
					+ (*(d->y+i) - y0) * (*(d->y+j) - y0)
					+ (*(d->z+i) - z0) * (*(d->z+j) - z0)  )
				  / (ri*rj);
	   
	   // Projection weighting factor
	   cij =  0.5 * (ri - rj) * (1. + costheta)
			/ sqrt(ri*ri + rj*rj - 2.*ri*rj*costheta);
	   
	   // Calculate separation distance
	   rij = 
		  (*(d->x+i) - *(d->x+j)) * (*(d->x+i) - *(d->x+j))  // x^2
		+ (*(d->y+i) - *(d->y+j)) * (*(d->y+i) - *(d->y+j))  // y^2
		+ (*(d->z+i) - *(d->z+j)) * (*(d->z+i) - *(d->z+j)); // z^2
	   rij = sqrt(rij);
	   
	   // Add data to appropriate bin
	   k = (int)(rij / dr);
	   *(ppair_num+k) += (pri - prj) * cij;
	   *(ppair_denom+k) += cij*cij;
	   *(Nbin+k) += 1;
	   
	 } // end j loop
	} // end i loop
	
	// Output basic info
	for(i=0; i<Nbins; i++){
		*(ppair+i) = *(ppair_num+i) / *(ppair_denom+i);
		printf("%f\t%f\t%d\n", (double)i *dr*3000., *(ppair+i), *(Nbin+i));
	} // end output loop 
}




int main(int argc, char* argv[]){
	
	// Load masses, positions, and velocities
	int N;
	struct datacols d; // Keep data pointers in struct
	d.M = load_column("virgoMS.M.bin", &N);
	d.x = load_column("virgoMS.x.bin", &N);
	d.y = load_column("virgoMS.y.bin", &N);
	d.z = load_column("virgoMS.z.bin", &N);
	d.vx = load_column("virgoMS.vx.bin", &N);
	d.vy = load_column("virgoMS.vy.bin", &N);
	d.vz = load_column("virgoMS.vz.bin", &N);
	fprintf(stderr, "\tNo. clusters: %d\n", N);
	
	// Check that the requested no. of cluster pairs is valid
	if(Npairs > N){printf("ERROR: N < Npairs.Exiting.\n"); exit(2);};
	
	// TESTING
	int i;
	for(i=0; i<10; i++){
		printf("%f\n", d.M[i]);
	}
	return 0;
	
	// Calculate the pairwise momenta
	fprintf(stderr, "\tCalculating pairwise momenta...\n");
	clock_t start = clock(); // Timing
	
	//calculate_pairwise_momenta(N, &d);
	calculate_los_pairwise_momenta(N, &d);
	
	int msec = (clock() - start) * 1000 / CLOCKS_PER_SEC;
	fprintf(stderr, "Run took %d sec.\n", msec/1000);
	
	// Free arrays and return
	free(d.M); free(d.x); free(d.y); free(d.z);
	free(d.vx); free(d.vy); free(d.vz);
	return 0;
}
