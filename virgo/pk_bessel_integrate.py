#!/usr/bin/python
"""Calculate the integrand of P(k) subject to Bessel window functions, for a 
   range of x. Used in velocity catalogue calculations."""

import numpy as np
import cosmolopy as cp
import pylab as P
import scipy.integrate
from scipy.special import jn

# Define cosmology from VIRGO LCDM simulation
cosmo = {
 'omega_M_0': 		0.3,
 'omega_lambda_0': 	0.7,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.9, # Note, high
 'baryonic_effects': True
}

# Precompute power spectrum
k = np.logspace(-5, 2., 2e4)
pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)

def integrate_pk_j0(pk, k, x):
	"""Integrate P(k) with j0(kx) window function, for a given x."""
	return scipy.integrate.simps(pk*jn(0, k*x), k)

def integrate_pk_j2(pk, k, x):
	"""Integrate P(k) with j2(kx) window function, for a given x."""
	return scipy.integrate.simps(pk*jn(2, k*x), k)

x = np.linspace(0., 200., 200)
y1 = map(lambda xx: integrate_pk_j0(pk, k, xx), x)
y2 = map(lambda xx: integrate_pk_j2(pk, k, xx), x)

P.subplot(111)
P.plot(x, y1, 'r-')
P.plot(x, y1, 'rx')

P.plot(x, y2, 'b-')
P.plot(x, y2, 'bx')
P.show()
