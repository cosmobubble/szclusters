#!/usr/bin/python
"""Test matrix inversion techniques."""

import numpy as np
import scipy.linalg
import cProfile

# Generate random symmetric square matrix of size N; make it symmetric
N = 1000
rawmat = np.random.normal(loc=0.0, scale=1.0, size=(N,N))
mat = 0.5 * (rawmat + rawmat.T) # Get only symmetric part




# (1) Standard NumPy algorithm
#inv1 = np.linalg.inv(mat)

# (2) LU decomposition
#I = np.eye(N)
#LUfac = scipy.linalg.lu_factor(mat)
#inv2 = scipy.linalg.lu_solve(LUfac, I)
