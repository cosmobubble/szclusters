#!/usr/bin/python

import sys
sys.path.append("/home/phil/postgrad/sz_clusters/VelocityStats/installed/lib/python")

import numpy as np
import pylab as P
from MockClusterCatalogue import MockClusterCatalogue
import VelocityStats
import time
import pairwise_momentum_statistics
tstart = time.time()

# Load cluster catalogue
L = 3000. / 0.7 # Mpc
Ncl = 20000
dr = 2.
cat = MockClusterCatalogue("virgo", Ncl)
cat2 = MockClusterCatalogue("sehgal", Ncl)

print np.mean(cat.M500), np.mean(cat2.M500)
print np.mean(cat.x), np.mean(cat2.x)
print np.mean(cat.vx), np.mean(cat2.vx)


# Calculate binning
NBINS = int(2 * L/dr)
rr = np.linspace(0.0, dr*NBINS, NBINS)

# DO IN C CODE
pp, NN = VelocityStats.pairwise_momenta(NBINS, dr, cat.M500, cat.x, cat.y, cat.z, cat.vx, cat.vy, cat.vz)

pp2, NN2 = VelocityStats.pairwise_momenta(NBINS, dr, cat2.M500, cat2.x, cat2.y, cat2.z, cat2.vx, cat2.vy, cat2.vz)

# DO IN PYTHON
#r1, ppr1, NN1 = pairwise_momentum_statistics.ppr_full(cat, rmax=500., dr=10.)

#pprx, NNx = VelocityStats.pairwise_momenta_los(NBINS, dr, cat.M500, (cat.x-0.5*L), (cat.y-0.5*L), (cat.z-0.5*L), cat.vx, cat.vy, cat.vz)

ii = 50
P.subplot(211)
P.axhline(0.0, ls="dashed", color='k')
P.plot(rr[:ii], pp[:ii]/NN[:ii], 'k-', alpha=0.8)
#P.plot(rr[:ii], pprx[:ii], 'r.')
#P.plot(r1, ppr1, 'b--')

P.subplot(212)
P.axhline(0.0, ls="dashed", color='k')
P.plot(rr[:ii], pp2[:ii]/NN2[:ii], 'r-', alpha=0.8)

# Timing stats
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
