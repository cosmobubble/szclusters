#!/usr/bin/python
"""
"""

import numpy as np
import pylab as P
import pairwise_momentum_statistics
from MockClusterCatalogue import MockClusterCatalogue
import time
tstart = time.time()

np.random.seed(10)

Ncl = 2000
rmax = 200. # Max. coordinate sep., in Mpc
dr = 10. # Bin width, in Mpc

# Load cluster catalogue
#cat1 = MockClusterCatalogue("virgo")
#cat2 = MockClusterCatalogue("sehgal", Ncl)
#cat = MockClusterCatalogue("bull", Ncl)

# Estimate errors using bootstrap technique (not quite correct...)
ppr_rows = []
NN_rows = []
for jj in range(20):
	cat1 = MockClusterCatalogue("virgo", Ncl, random=True)
	rbins1, ppr1, NN1 = pairwise_momentum_statistics.ppr(cat1)
	ppr_rows.append(ppr1)
	NN_rows.append(NN1)
	rb = rbins1

cat2 = MockClusterCatalogue("virgo", 6000, random=False)
rbins2, ppr2, NN2 = pairwise_momentum_statistics.ppr(cat2)

ppr_rows = np.array(ppr_rows).T
NN_rows = np.array(NN_rows).T

ppr_avg = [np.mean(ppr_rows[i]) for i in range(rb.size)]
ppr_err = [np.std(ppr_rows[i]) for i in range(rb.size)]


# Plot, with error bars
P.subplot(211)
P.axhline(0.0, ls="dotted", color="k")
for i in range(len(ppr_rows.T)):
	P.plot(rb, ppr_rows.T[i], 'y.', alpha=0.2)
P.errorbar(rb, ppr_avg, yerr=ppr_err, fmt='r.')
P.plot(rbins2, ppr2, 'kx')
P.ylabel("Pairwise momentum <p(r)>")
P.xlabel("Comoving separation r [Mpc]")

P.subplot(212)
P.axhline(0.0, ls="dotted", color="k")
for i in range(len(ppr_rows.T)):
	P.plot(rb[1:], NN_rows.T[i][1:], 'b.', alpha=0.6)
P.ylabel("No. clusters in bin")

#rbins2, ppr2 = ppr(cat2)
"""
# Plot
P.subplot(111)
P.figtext(0.7, 0.2, "Ncl = "+str(Ncl))
P.axhline(0.0, ls="dotted", color="k")
P.plot(rbins1, ppr1, 'r.')
#P.plot(rbins2, ppr2, 'b.')
P.ylabel("Pairwise momentum <p(r)>")
P.xlabel("Comoving separation r [Mpc]")
#P.errorbar(rbins*Lbox, ppr*A_ACT, yerr=err_ppr*A_ACT, fmt='rx')
"""

# Report on timing
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
