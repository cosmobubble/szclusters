#!/usr/bin/python
"""Plot the LOS pairwise momentum statistic with RSDs added."""

import numpy as np
import cosmolopy as cp
import pylab as P
from MockClusterCatalogue import MockClusterCatalogue


# Define cosmology from VIRGO LCDM simulation
cosmo = {
 'omega_M_0': 		0.3,
 'omega_lambda_0': 	0.7,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.9, # Note, high
 'baryonic_effects': True
}

# Load cluster catalogue
L = 3000. / cosmo['h'] # Mpc
Ncl = 10000
cat = MockClusterCatalogue("virgo", Ncl)

rx = (cat.x-0.5*L)
ry = (cat.y-0.5*L)
rz = (cat.z-0.5*L)
rr = np.sqrt(rx**2. + ry**2. + rz**2.) # Comoving distances

# Get cosmological distances
r_co = cp.distance.comoving_distance(cat.zz, **cosmo)

# RSDs
vlos = (rx*cat.vx + ry*cat.vy + rz*cat.vz) / rr
zpec = vlos / 3e5
dr_rsd = zpec * 3e5 / (cosmo['h']*100.) # Mpc, for h=0.71
cx = rx * (1. + dr_rsd/rr)
cy = ry * (1. + dr_rsd/rr)
cz = rz * (1. + dr_rsd/rr)

# Plot results
P.subplot(111)
P.axvline(0.0, color="k")
P.axhline(0.0, color="k")
#P.plot(r_co - rr, dr_rsd, 'r,')
P.plot(cat.zz, r_co - rr, 'r,')

P.xlabel("z")
P.ylabel("delta r(z)")
#P.xlabel("r(z+dz) - r(z)")
#P.ylabel("c dz/H_0")

P.show()
