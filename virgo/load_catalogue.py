#!/usr/bin/python
"""Process VIRGO Hubble Volume simulation cluster catalogues.
   http://www.mpa-garching.mpg.de/galform/virgo/hubble/catalog.shtml
   http://www.mpa-garching.mpg.de/galform/virgo/hubble/
"""

import numpy as np
import pylab as P

fname = "lcdm.MS.msort12"
##fname = "test"
OUT_ROOT = "virgoMS"

# File format (columns) [~1.5m clusters!]
# m = number of particles (particle mass is 2.22/2.25e12/h for tcdm/lcdm)
# zred = redshift
# sigma = measured 1D velocity dispersion 
# ip = parent flag (see below)
# x,y,z = cluster location in SURVEY COORDINATES in 0-1 units 
# vx,vy,vz= physical peculiar velocity in km/s
# [Spherical lightcone ("MS") has origin at (0.5,0.5,0.5) in survey coords.]

fmtstr = "%5.i %7.5f %4.i %2.i %8.6f %8.6f %8.6f %5.i %5.i %5.i "
colwidths = [6, 8, 5, 3, 9, 9, 9, 6, 6, 6]
data = np.genfromtxt(fname, delimiter=colwidths).T

# Apply mass cut (same as in ACT paper)
h = 0.7
Mp = 2.25e12 / h # Particle mass, in Msun
Mcut = 2.5e13
m = data[0] * Mp

print np.max(m), np.min(m)

idxs = np.where(m > Mcut)[0]
dat = data.T[idxs].T # Get cut array

print np.shape(dat), np.shape(data)
exit()

# Output in binary format for Python programs to use
np.save(fname+".bin", dat)

# Output in binary format that can be loaded by C program
# NOTE: tofile() produces files that aren't necessarily portable between 
# machines (endianness etc. is an issue)
print "Array size:", dat[0].size
dat[0].tofile(OUT_ROOT + ".M.bin")

dat[4].tofile(OUT_ROOT + ".x.bin")
dat[5].tofile(OUT_ROOT + ".y.bin")
dat[6].tofile(OUT_ROOT + ".z.bin")

dat[7].tofile(OUT_ROOT + ".vx.bin")
dat[8].tofile(OUT_ROOT + ".vy.bin")
dat[9].tofile(OUT_ROOT + ".vz.bin")
# Double precision "d":
# http://docs.scipy.org/doc/numpy/reference/generated/numpy.typename.html#numpy.typename
