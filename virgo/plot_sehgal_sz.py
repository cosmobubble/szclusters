#!/usr/bin/python
"""Load the Sehgal simulated SZ cluster catalogue."""

import numpy as np
import pylab as P

"""
# See http://lambda.gsfc.nasa.gov/toolbox/tb_sim_readme_sz.cfm
# For SZ signal, see discussion at top of column 2 of page 14 of Sehgal et al.
# Presumably, the SZ values in the catalogue are the "integrated Compton Y"
# over a cylindrical aperature (written as solid angle in the paper). The units 
# are in arcmin^2, so should probably be multiplied by dA(z)^2 to get them in 
# Mpc^2, like other estimates (since Ycyl ~ dim'less * (length)^2).
Halo array
halo(    1,i) = redshift
halo(    2,i) = ra  in degrees
halo(    3,i) = dec in degrees
halo(  4:6,i) = comoving position of halo potential minimum in Mpc
halo(  7:9,i) = proper peculiar velocity in km/s
halo(   10,i) = Mfof in Msolar

halo(   11,i) = Mvir     in Msolar
halo(   12,i) = Mgas_vir in Msolar
halo(   13,i) = Rvir     in proper Mpc
halo(   14,i) = Integrated TSZ within Rvir in arcminute^2
halo(   15,i) = Integrated KSZ within Rvir in arcminute^2
halo(16:21,i) = Integrated SZ at 148,219,277,30,90,350 GHz

halo(22:32,i) = Same as 11-21 but for within R200
halo(33:43,i) = Same as 11-21 but for within R500

halo(   44,i) = Mstar               in Msolar
halo(   45,i) = central gas density in Msolar/Mpc^3
halo(   46,i) = central temperature in keV
halo(   47,i) = central pressure    in Msolar/Mpc/Gyr^2
halo(   48,i) = central potential   in (Mpc/Gyr)^2
"""

fname = "act_sz_2000.dat"
colnum = (0, 3,4,5, 6,7,8, 18, 32,34,35,36)
zz, x, y, z, vx, vy, vz, sz30ghz, M500, R500, Y500, K500 = np.genfromtxt(fname, usecols=colnum).T


P.subplot(111)
rrr = (K500/sz30ghz)
print "Mean, var =", np.mean(rrr), np.std(rrr)
P.hist(rrr, bins=50)
P.show()


exit()

r = np.sqrt(x**2. + y**2. + z**2.)
vr = (x*vx + y*vy + z*vz)/(r*3e5)

"""
# Get LOS velocities
r = np.sqrt(x**2. + y**2. + z**2.)
vr = (x*vx + y*vy + z*vz)/(r*3e5)

P.subplot(111)
P.plot(zz, Y500/(K500/vr), 'r.', ms=1.)
P.ylabel("tSZ Y500 / (kSZ Y500 / v_r)")
P.xlabel("z")

"""
# tSZ

P.subplot(231)
P.plot(zz, Y500, 'r.', ms=1.)
P.xlabel("z")
P.ylabel("tSZ Y500")
P.ylim((-0.0002, 0.0021))

P.subplot(232)
P.plot(M500, Y500, 'b.', ms=1.)
P.xlabel("M500")
P.ylabel("tSZ Y500")
P.ylim((-0.0002, 0.0021))

P.subplot(233)
P.plot(R500, Y500, 'g.', ms=1.)
P.xlabel("R500")
P.ylabel("tSZ Y500")
P.ylim((-0.0002, 0.0021))

# kSZ

P.subplot(234)
P.plot(zz, K500/vr, 'r.', ms=1.)
P.xlabel("z")
P.ylabel("kSZ Y500")
#P.ylim((-0.0002, 0.0002))

P.subplot(235)
P.plot(M500, K500/vr, 'b.', ms=1.)
P.xlabel("M500")
P.ylabel("kSZ Y500")
#P.ylim((-0.0002, 0.0002))

P.subplot(236)
P.plot(R500, K500/vr, 'g.', ms=1.)
P.xlabel("R500")
P.ylabel("kSZ Y500")
#P.ylim((-0.0002, 0.0002))



P.show()
