#!/usr/bin/python
"""Plot the LOS pairwise momentum statistic with RSDs added."""

import sys
sys.path.append("/home/phil/postgrad/sz_clusters/VelocityStats/installed/lib/python")
import numpy as np
import cosmolopy as cp
import pylab as P
import VelocityStats
from MockClusterCatalogue import MockClusterCatalogue
import time
tstart = time.time()


# Define cosmology from VIRGO LCDM simulation
cosmo = {
 'omega_M_0': 		0.3,
 'omega_lambda_0': 	0.7,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.9, # Note, high
 'baryonic_effects': True
}


# Load cluster catalogue
L = 3000. / 0.7 # Mpc
Ncl = 10000
dr = 2.0
ii = 50 # Max. bin to plot
cat = MockClusterCatalogue("virgo", Ncl)

# Calculate binning
NBINS = int(2 * L/dr)
rbins = np.linspace(0.0, dr*NBINS, NBINS)

# Calculate distances
rx = (cat.x-0.5*L)
ry = (cat.y-0.5*L)
rz = (cat.z-0.5*L)
rr = np.sqrt(rx**2. + ry**2. + rz**2.) # Comoving distances

# Get LOS velocity and corresponding redshift distortion and Hubble distance
vlos = (rx*cat.vx + ry*cat.vy + rz*cat.vz) / rr
zpec = vlos / 3e5
rhubble = (cat.zz + zpec) * 3e5 / 70. # Mpc, for h=0.7

# Get coordinates, rescaled into Hubble distances (including RSDs)
hx = rhubble * (rx / rr)
hy = rhubble * (ry / rr)
hz = rhubble * (rz / rr)

# Get coordinates, rescaled into Hubble distances (including RSDs)
dr_rsd = zpec * 3e5 / 70. # Mpc, for h=0.7
cx = rx * (1. + dr_rsd/rr)
cy = ry * (1. + dr_rsd/rr)
cz = rz * (1. + dr_rsd/rr)

# Get coordinates, rescaled into Hubble distances (including *scaled* RSDs)
fac = 0.5
cx2 = rx * (1. + fac*dr_rsd/rr)
cy2 = ry * (1. + fac*dr_rsd/rr)
cz2 = rz * (1. + fac*dr_rsd/rr)

# Calculate LOS pairwise momentum statistics
# (1) Without RSDs
MM = np.ones(cat.M500.size)
"""
pp1, NN1 = VelocityStats.pairwise_momenta_los(NBINS, dr, cat.M500, rx, ry, rz,
											  cat.vx, cat.vy, cat.vz)
"""

"""
# Standard (not mass-weighted)
pp2, NN2 = VelocityStats.pairwise_momenta_los(NBINS, dr, MM, rx, ry, rz,
											  cat.vx, cat.vy, cat.vz)
"""


"""
# (2) With RSDs and Hubble distances
pp2, NN2 = VelocityStats.pairwise_momenta_los(NBINS, dr, cat.M500, hx, hy, hz,
											  cat.vx, cat.vy, cat.vz)
"""

# (3) With RSDs, but standard comoving distances
pp2, NN2 = VelocityStats.pairwise_momenta_los(NBINS, dr, cat.M500, cx, cy, cz,
											  cat.vx, cat.vy, cat.vz)

# Randomised velocities
vx1 = cat.vx; vy1 = cat.vy; vz1 = cat.vz
np.random.shuffle(vx1)
np.random.shuffle(vy1)
np.random.shuffle(vz1)
pp3, NN3 = VelocityStats.pairwise_momenta_los(NBINS, dr, MM, cx, cy, cz,
											  vx1, vy1, vz1)

"""
# (4) With RSDs, reduced by a factor of 10, but standard comoving distances
pp4, NN4 = VelocityStats.pairwise_momenta_los(NBINS, dr, cat.M500, cx2, cy2, cz2,
											  cat.vx, cat.vy, cat.vz)
"""

# Plot results
P.subplot(111)
P.axhline(0.0, ls="dashed", color='k')
##P.plot(rbins[:ii], pp1[:ii], 'k-', alpha=0.8, label="Comov. dist.")
##P.plot(rbins[:ii], pp1[:ii], 'k+', alpha=0.8, label="Comov. dist.")
P.plot(rbins[:ii], pp2[:ii], 'b-', alpha=0.8, label="Comov. + RSD M=1")
P.plot(rbins[:ii], pp3[:ii], 'r-', alpha=0.8, label="Comov. + RSD M=1 (randomised vel.)")
#P.plot(rbins[:ii], pp4[:ii], 'g-', alpha=0.8, label="Comov. dist.+RSDs/2")
#P.plot(rbins[:ii], pp2[:ii], 'r-', alpha=0.8, label="Hubble dist.+RSDs")
P.xlabel("Comoving separation, r [Mpc]")
P.ylabel("Pairwise momentum, p_pair(r)")
P.legend(loc="lower right", prop={'size':'small'})
P.title("VIRGO")

# Timing stats
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
