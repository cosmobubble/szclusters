#!/usr/bin/python
"""Plot the comoving distance vs. the Hubble distance
"""

import numpy as np
import pylab as P
import cosmolopy as cp


# Define cosmology from VIRGO LCDM simulation
cosmo = {
 'omega_M_0': 		0.3,
 'omega_lambda_0': 	0.7,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.9, # Note, high
 'baryonic_effects': True
}

# Calculate distances
z = np.linspace(0., 0.5, 200)
r_co = cp.distance.comoving_distance(z, **cosmo)
r_H = z * (3e5 / 70.)

# Plot results
P.subplot(111)
P.plot(z, r_co, 'r-', label="R_comoving")
P.plot(z, r_H, 'b-', label="R_Hubble")
P.ylabel("Dist. [Mpc]")
P.xlabel("z")
P.grid(True)
P.legend(loc="lower right", prop={'size':'small'})
P.show()
