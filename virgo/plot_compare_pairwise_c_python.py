#!/usr/bin/python
"""Compare results of C and Python pairwise momentum statistics, with same 
   binning and cluster counts.
"""
import numpy as np
import pylab as P

# Load data
r1, ppr1, N1 = np.genfromtxt("test_los_c.dat")[:300].T
r2, ppr2, N2 = np.load("test_los_numpy.npy").T


# Plot results
P.subplot(211)
P.plot(r1, ppr1, 'r-')
P.plot(r2, ppr2, 'b-')

P.subplot(212)
P.plot(r1, N1, 'r-')
P.plot(r2, N2, 'b-')

P.show()
