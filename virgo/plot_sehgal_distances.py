#!/usr/bin/python
"""Plot the LOS pairwise momentum statistic with RSDs added."""

import numpy as np
import cosmolopy as cp
import pylab as P
from MockClusterCatalogue import MockClusterCatalogue

# Define cosmology from Sehgal (ACT) LCDM simulation
cosmo = {
 'omega_M_0': 		0.264,
 'omega_lambda_0': 	0.736,
 'omega_b_0': 		0.044,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.71,
 'n':				0.96,
 'sigma_8':			0.80, # Note, high
 'baryonic_effects': True
}

# Load cluster catalogue
L = 1000. / cosmo['h'] # Mpc
Ncl = 10000
cat = MockClusterCatalogue("sehgal", Ncl)

# Calculate distances
rx = cat.x
ry = cat.y
rz = cat.z
rr = np.sqrt(rx**2. + ry**2. + rz**2.) # Comoving distances

# Get cosmological distances
r_co = cp.distance.comoving_distance(cat.zz, **cosmo)

# RSDs
vlos = (rx*cat.vx + ry*cat.vy + rz*cat.vz) / rr
zpec = vlos / 3e5
dr_rsd = zpec * 3e5 / (cosmo['h']*100.) # Mpc, for h=0.71
cx = rx * (1. + dr_rsd/rr)
cy = ry * (1. + dr_rsd/rr)
cz = rz * (1. + dr_rsd/rr)

zcorr = cat.zz + zpec
r_coz = cp.distance.comoving_distance(zcorr, **cosmo)

# Plot results
P.subplot(111)
P.title("ACT (Sehgal) distances")
#P.axvline(0.0, color="k")
#P.plot(r_co - rr, dr_rsd, 'r,')
P.plot(cat.zz, (r_co - rr)/r_co, 'r.', alpha=1.0, label="$\Delta r$ (theory - cat.)")
P.plot(cat.zz, (dr_rsd)/r_co, 'b.', alpha=0.05, label="$\Delta r$ (pec. vel. - cat.)")
P.plot(cat.zz, (r_coz - rr)/r_coz, 'gx', alpha=0.1, label="$\Delta r$ (theory(z) - cat.)")

P.axhline(0.0, color="k")
P.legend(loc="lower right", prop={'size':'small'})
P.xlabel("z")
P.ylabel("$\Delta r / r$")
#P.xlabel("r(z+dz) - r(z)")
#P.ylabel("c dz/H_0")

P.show()
