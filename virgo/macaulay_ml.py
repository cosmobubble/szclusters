#!/usr/bin/python
"""Calculate the maximum likelihood velocity estimator for a velocity 
   catalogue, from Macaulay et al. (arXiv:1111.3338). Also uses results from 
   Ma et al. (arXiv:1010.4276)."""

import numpy as np
import cosmolopy as cp
import pylab as P
import scipy.spatial.distance
import scipy.interpolate
from scipy.special import jn
from MockClusterCatalogue import MockClusterCatalogue
import time
tstart = time.time()


##########
# FIXME: Should sub H0 -> H(a)???
# Also, try replacing the velocities with random Gaussians - should see white 
# noise power spectrum (could bias vel. correlation result?)
##########


np.seterr(divide="ignore", invalid="ignore") # Suppress div/0 errors

# Range and accuracy of pre-computed Bessel function integrals
BESSEL_XMIN = 0.
BESSEL_XMAX = 700.
BESSEL_SAMPLES = 120


################################################################################
# Define cosmology and load velocity catalogue
################################################################################

# Define cosmology from Sehgal (ACT) LCDM simulation
cosmo = {
 'omega_M_0': 		0.264,
 'omega_lambda_0': 	0.736,
 'omega_b_0': 		0.044,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.71,
 'n':				0.96,
 'sigma_8':			0.80, # Note, high
 'baryonic_effects': True
}

# Load catalogue
L = 1000. / 0.7 # Mpc
Ncl = 550
cat = MockClusterCatalogue("sehgal", Ncl)
N = cat.x.size


################################################################################
# Precompute power spectrum and Bessel interpolation functions
################################################################################

k = np.logspace(-5, 2., 2e4)
pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)

def integrate_pk_j0(pk, k, x):
	"""Integrate P(k) with j0(kx) window function, for a given x."""
	return scipy.integrate.simps(pk*jn(0, k*x), k)

def integrate_pk_j2(pk, k, x):
	"""Integrate P(k) with j2(kx) window function, for a given x."""
	return scipy.integrate.simps(pk*jn(2, k*x), k)

# Calculate Bessel-filtered P(k) integrals
_x = np.linspace(BESSEL_XMIN, BESSEL_XMAX, BESSEL_SAMPLES)
_y0 = map(lambda xx: integrate_pk_j0(pk, k, xx), _x)
_y2 = map(lambda xx: integrate_pk_j2(pk, k, xx), _x)
_ynn = scipy.integrate.simps(pk, k) / 3. # Value when n=m

# Construct interpolation functions
bessel0 = scipy.interpolate.interp1d(_x, _y0, kind='linear')
bessel2 = scipy.interpolate.interp1d(_x, _y2, kind='linear')

################################################################################
# Precompute distances, velocities and angles
################################################################################

# FIXME: Assumes observer at (0,0,0)

# Get distances between all objects in catalogue
x_n = np.column_stack((cat.x, cat.y, cat.z)) # Position vectors
r_n = np.sqrt(cat.x**2. + cat.y**2. + cat.z**2.) # Lengths
r_nm = scipy.spatial.distance.cdist(x_n, x_n) # Relative separation

# 2D arrays for calculation
x_nm = np.array([cat.x,]*N)
y_nm = np.array([cat.y,]*N)
z_nm = np.array([cat.z,]*N)
rr_nm = np.array([r_n,]*N)

# Get angles
cosa_nm = ( (x_nm*x_nm.T) + (y_nm*y_nm.T) + (z_nm*z_nm.T) ) / (rr_nm * rr_nm.T)
sin2a_nm = 1. - cosa_nm**2.

# Calculate LOS velocities, Sn = vlos_n
Sn = (cat.vx*cat.x + cat.vy*cat.y + cat.vz*cat.z) / r_n

# Check Bessel function validity
if np.max(r_nm) >= BESSEL_XMAX:
	print "Max. object separation > max. separation in Bessel precomputation."
	print "max(r_nm) =", np.max(r_nm)
	print "Bessel x_max =", BESSEL_XMAX
	exit()

################################################################################
# Compute F_nm ~ \int(f_nm(k) P(k) dk) and R^(v)_nm (Eq. 7 of Macaulay)
################################################################################

bess2 = bessel2(r_nm)
F_nm = (cosa_nm/3.) * ( bessel0(r_nm) - 2.*bess2 )
F_nm += bess2 * rr_nm*(rr_nm.T) * sin2a_nm / r_nm**2.
F_nm[np.diag_indices_from(F_nm)] = _ynn # Diagonal terms. FIXME: Is this correct?

# Array of scale factors, a_n * a_m
a = 1. / (1. + cat.zz)
a_nm = np.outer(a, a)

# Array of growth rates, f(a_n) * f(a_m)
# See Linder arXiv:astro-ph/0507263v2 for parametrisation
gamma = 0.55
Oma = cosmo['omega_M_0'] * a**-3.
Oma *= cp.distance.e_z(cat.zz, **cosmo)**-2.
fgrowth = Oma**gamma
fgrowth_nm = np.outer(fgrowth, fgrowth)

# Multiply by cosmology factors
bias = 1.0
H0 = cosmo['h'] * 100.
Rv_nm = F_nm * fgrowth_nm * a_nm * (H0 / bias)**2. / (2. * np.pi**2.)


################################################################################
# Compute diagonal noise and measurement error term
################################################################################

# Diagonal error term in covariance matrix
Re_nm = np.zeros((N,N))

"""
# Error on individual velocity measurements, and nonlinear velocity dispersion
sig_m = 1.*np.ones(N) # FIXME, ~200
sig_disp = 1. * np.ones(N) # FIXME (Value used in Macaulay: 350, see page 4)
Re_nm[np.diag_indices_from(Re_nm)] = sig_m**2. #+ sig_disp**2. # FIXME
"""

################################################################################
# Final covariance matrix; calculate inverse
################################################################################

def loglikelihood(A, Rv_nm, Re_nm, Sn):
	"""Return log likelihood, given some pre-calculated covariances."""
	print "A =", A
	
	# Construct full covariance matrix and invert it
	R_nm = Re_nm + A*Rv_nm
	invR = np.linalg.inv(R_nm) # NOTE: Intensive
	
	# Matrix too big; get log determinant (normalising factor)
	# NOTE: Matrix is symmetric, if that helps?
	detsign, logdetR = np.linalg.slogdet(R_nm)
	
	# log likelihood, from data
	logL = -0.5 * np.dot( Sn, np.dot(invR, Sn.T) )
	#Lnorm = 1. / (  (2.*np.pi)**(N/2.) * np.sqrt(detR)  )
	return logL - logdetR

# Calculate likelihoods
A = np.logspace(np.log10(5e-1), np.log10(2e0), 30)
logL = np.array(  [loglikelihood(AA, Rv_nm, Re_nm, Sn) for AA in A]  )

Amax = A[np.where(logL==np.max(logL))]

P.subplot(111)
P.plot(A, logL, 'b-')
P.plot(A, logL, 'bx')
P.xscale('log')
P.axvline(Amax, ls="dotted", color="k")
P.xlabel("P(k) amplitude, A")
P.ylabel("log likelihood")
P.figtext(0.6, 0.2, "Max.="+str(round(Amax[0], 3)))

print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
