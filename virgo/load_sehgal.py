#!/usr/bin/python
"""Load the Sehgal simulated SZ cluster catalogue."""

import numpy as np
import pylab as P

"""
# See http://lambda.gsfc.nasa.gov/toolbox/tb_sim_readme_sz.cfm
Halo array
halo(    1,i) = redshift
halo(    2,i) = ra  in degrees
halo(    3,i) = dec in degrees
halo(  4:6,i) = comoving position of halo potential minimum in Mpc
halo(  7:9,i) = proper peculiar velocity in km/s
halo(   10,i) = Mfof in Msolar

halo(   11,i) = Mvir     in Msolar
halo(   12,i) = Mgas_vir in Msolar
halo(   13,i) = Rvir     in proper Mpc
halo(   14,i) = Integrated TSZ within Rvir in arcminute^2
halo(   15,i) = Integrated KSZ within Rvir in arcminute^2
halo(16:21,i) = Integrated SZ at 148,219,277,30,90,350 GHz

halo(22:32,i) = Same as 11-21 but for within R200
halo(33:43,i) = Same as 11-21 but for within R500

halo(   44,i) = Mstar               in Msolar
halo(   45,i) = central gas density in Msolar/Mpc^3
halo(   46,i) = central temperature in keV
halo(   47,i) = central pressure    in Msolar/Mpc/Gyr^2
halo(   48,i) = central potential   in (Mpc/Gyr)^2
"""

#fname = "test"
fname = "halo_sz.ascii" # Need to unzip first!
OUT_NAME = "sehgal_clusters"
colnum = (0,1,2,3,4,5,6,7,8,32,34)
dat = np.genfromtxt(fname, usecols=colnum).T

# Get pertinent columns
zz = dat[0]
ra = dat[1]
dec = dat[2]
x = dat[3]
y = dat[4]
z = dat[5]
vx = dat[6]
vy = dat[7]
vz = dat[8]
M500 = dat[9] #dat[32]
R500 = dat[10] #dat[34]

# Save data in binary array
cols = np.column_stack((zz, ra, dec, x, y, z, vx, vy, vz, M500, R500))
##np.save(OUT_NAME, cols) # FIXME: Uncomment when used in anger!

