#!/usr/bin/python
"""Read-in the reformatted MCXC catalogue, and output a cleaned version."""

import numpy as np
import pylab as P
import astropysics.coords as coords
from pyltb import FLRW
import time
tstart = time.time()

# Semicolon separated (commas in text fields)
#0: Column	MCXC	(A12)	MCXC name (JHHMM.m+DDMM)	[ucd=meta.id,meta.main]
#1: Column	OName	(a18)	Other name	[ucd=meta.id]
#2: Column	AName	(a54)	Alternative name	[ucd=meta.id]
#3: Column	RAJ2000	(A10)	Right ascension (J2000)	[ucd=pos.eq.ra,meta.main]
#4: Column	DEJ2000	(A9)	Declination (J2000)	[ucd=pos.eq.dec,meta.main]
#5: Column	z	(F7.4)	Redshift	[ucd=src.redshift]
#6: Column	Scale	(F6.3)	Scale	[ucd=instr.scale]
#7: Column	L500	(F9.6)	X-ray luminosity in 10^44^erg/s (1)	[ucd=phys.luminosity,em.X-ray]
#8: Column	M500	(F7.4)	Total mass (1)	[ucd=phys.mass]
#9: Column	R500	(F7.4)	Characteristic radius (1)	[ucd=phys.size.radius]
#10: Column	Notes	(a42)	Notes (losStr = line of sight structure)	[ucd=meta.note]

fname = "data/MCXC_full_reformatted.dat"
fcols = (5, 6, 7, 8, 9) # Data columns (float)
scols = (0, 1, 2, 3, 4, 10) # Data columns (strings)

# Load data and convert coordinates
z, scale, L500, M500, R500 = np.genfromtxt(fname, delimiter=";", usecols=fcols).T
mname, mname2, mname3, ra, dec, notes = np.genfromtxt(fname, dtype="a", delimiter=";", usecols=scols).T

def convert_coords(ra_, dec_):
	"""Convert arrays from FK5/J2000 equatorial coords to galactic."""
	c = [coords.coordsys.FK5Coordinates((ra_[i], dec_[i])) for i in range(ra_.size)]
	c = [cc.convert(coords.GalacticCoordinates) for cc in c]
	l = [cc.l.degrees for cc in c]
	b = [cc.b.degrees for cc in c]
	return np.array(l), np.array(b)
l, b = convert_coords(ra, dec)

# Assign IDs
ids = np.arange(0, z.size)

# Get LOS substructure flags
flag_substr = map(lambda x: "losstr" in x.lower() and True or False, notes)

# Calculate angular size at R500
model = FLRW(h=0.7, Om=0.3, Ol=0.7) # Define reference cosmological model
dA = np.array(  map(lambda x: model.dA(x), z)  )
th500 = R500 / dA

# Output new-format table
# ID, z, gal(l), gal(b), da, theta500, r500, m500, l500
f1out = "data/MCXC_processed.dat"; f2out = "data/MCXC_names.txt"
f1 = open(f1out, 'w'); f2 = open(f2out, 'w')
for i in range(z.size):
	l1 = "\t".join( (str(ids[i]), str(z[i]), str(l[i]), str(b[i]), str(dA[i]), 
	                 str(th500[i]), str(R500[i]), str(M500[i]), str(L500[i]), 
	                 str(scale[i])) )
	# FIXME: Add other names in
	l2 = "\t".join( (str(ids[i]), str(mname[i]), str(mname2[i]), str(mname3[i])) )
	f1.write(l1 + "\n"); f2.write(l2 + "\n")
f1.close(); f2.close()

# Output timing information
print "Run took", round(time.time() - tstart, 2), "sec."
