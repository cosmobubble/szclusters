#!/usr/bin/python
"""Clean-up the MCXC catalogue by applying various cuts."""

import sys, time
sys.path.append("/home/phil/postgrad/lib") # HealPy path
import numpy as np
import pylab as P
import healpy as hp
import catalogue_cuts as cuts
import map_manipulation
import coordinates, catalogues
tstart = time.time()


################################################################################
# Settings and file names
################################################################################

opts = {
	'USE_PRECOMPUTED_DISTS':True, # Whether to compute ang. dists. or use saved versions
	
	'ANG_SIZE_CUT':0.63, # in degrees
	'EXCLUSION_RADIUS':5., # Cut out anything within E_R * r500 of a flagged cluster
	'DUP_ANG_CUT':0.20, # Angular distance cut, in degrees, for duplicate candidates
	'DUP_Z_CUT':0.0051, # Redshift difference for duplicates
	'RADIUS_IDENTIFY':1., # Radius within which ptsrcs identified as cls. (in theta500)
	'RADIUS_CONTAM':5., # Radius within which ptsrcs identified as contam. (in theta500)
	
	'NSIDE':512, # Nside of output Healpix mask
	'PS_MASK_RAD':0.6 * np.pi/180., # Point source mask radius, in rad. (0.6deg)
	'PS_MASK_RAD_PADDING':1.2, # Factor by which to pad mask radius if *unmasking*
	
	'FNAME_CL_TRIMMED':"data/MCXC_trimmed.dat", # Fully cleaned MCXC catalogue
	'FNAME_CL_DUPONLY':"data/MCXC_nodups.dat", # MCXC cleaned only of duplicates
	'FNAME_MASK':"data/WMAP_cleaned_ptsrc_mask.fits", # Cleaned ptsrc mask
	'FNAME_WMAP_GALMASK':"data/wmap_point_source_catalog_mask_r9_7yr_v4.fits", # Galaxy mask
	'FNAME_PRECOMPUTED_PTCL':"data/precomputed_ptsrc_angular_distances",
	'FNAME_PRECOMPUTED_CLCL':"data/precomputed_angular_distances",
	'FNAME_WMAP_PTSRC_MASK':"data/wmap_temperature_source_mask_r9_7yr_v4.fits"
	}


################################################################################
# Load data and calculate angular distances if necessary
################################################################################

# Load catalogues
catPS = catalogues.CatalogueWmapPointSource(galcoords=True)
catCL = catalogues.CatalogueMCXC()

# Get cluster-cluster and cluster-pointsource angular distances
if opts['USE_PRECOMPUTED_DISTS']:
	print "\n\t*** Using precomputed angular distances..."
	angdists_clcl = np.load(opts['FNAME_PRECOMPUTED_CLCL']+".npy")
	angdists_ptcl = np.load(opts['FNAME_PRECOMPUTED_PTCL']+".npy")
	# Check that data was reloaded correctly
	assert (catCL.l.size, catCL.l.size) == np.shape(angdists_clcl)
	assert (catCL.l.size, catPS.l.size) == np.shape(angdists_ptcl)
else:
	# Calculate distances between all clusters, cluster/ptsrcs (will take a while!)
	print "\n\t*** Computing angular distances (cluster-cluster)..."
	angdists_clcl = coordinates.angdists_catalogue(catCL.l, catCL.b)
	print "\t*** Computing angular distances (cluster-pointsrc)..."
	angdists_ptcl = coordinates.angdists_2catalogues(catPS.l, catPS.b, catCL.l, catCL.b)
	print "\t*** Finished computing distances."
	np.save(opts['FNAME_PRECOMPUTED_CLCL'], angdists_clcl)
	np.save(opts['FNAME_PRECOMPUTED_PTCL'], angdists_ptcl)



################################################################################
# Apply cuts to catalogues and obtain masks
################################################################################

print "\t*** Calculating masks..."

# Clusters that are very large on the sky
m_big = cuts.mask_large_clusters(catCL, opts)

# Clusters which overlap with very large clusters
m_neighb = cuts.mask_large_cluster_neighbours(angdists_clcl, catCL, opts)

# Duplicate/very close clusters in the catalogue
m_dup = cuts.mask_duplicate_clusters(angdists_clcl, catCL, opts)

# Pointsources that are actually clusters
m_ptsrc_cls = cuts.mask_ptsrcs_are_clusters(angdists_ptcl, catCL, catPS, opts)

# Clusters that are contaminated by pointsources
m_contam = cuts.mask_ptsrc_contam_clusters(angdists_ptcl, catCL, catPS, opts)


################################################################################
# Report on cuts
################################################################################

print ""
print "MASKING REPORT:"
print "\tBig Clusters:\t", np.sum(-m_big)
print "\t(Neighbours):\t", np.sum(-m_neighb)
print "\tDuplicates:\t", np.sum(-m_dup)
print "\tPtsrc is Cl.:\t", np.sum(-m_ptsrc_cls)
print "\tContam. Cl.:\t", np.sum(-m_contam)
print ""


################################################################################
# Write-out updated cluster catalogues
################################################################################

# Need to rejig the range of gal. coord. "l", so it's between [-180, 180], 
# rather than [0, 360]. This is done by taking any l > 180 and subtracting 360
l2_cl = np.where(catCL.l > 180., catCL.l - 360., catCL.l)

# The template cluster catalogue file I have (from H.K. Eriksen, 2012-05-07) 
# for use with Commander appears to be in the following format:
# l, b, z, scale, L500, M500, R500
fmtstr = "%+10.5f\t%9.5f\t%6.4f\t%6.3f\t%9.6f\t%7.4f\t%7.4f   "

# (1) Output fully-trimmed cluster catalogue
#m = (-m_big -m_neighb -m_dup -m_contam) # Need AND operation on masks
AND = np.logical_and
m = AND(AND(AND(m_big, m_neighb), m_dup), m_contam)
dat = np.column_stack(   ( l2_cl[m], catCL.b[m], catCL.z[m], catCL.scale[m], 
						   catCL.L500[m], catCL.M500[m], catCL.R500[m] )  )
np.savetxt(opts['FNAME_CL_TRIMMED'], dat, fmt=fmtstr, delimiter="\t")
print "Total masked clusters:", np.sum(-m), "/", m.size, " (delta="+str(m.size-np.sum(-m))+")"

# (2) Output cluster catalogue with only duplicates removed
m = m_dup
dat = np.column_stack(   ( l2_cl[m], catCL.b[m], catCL.z[m], catCL.scale[m], 
						   catCL.L500[m], catCL.M500[m], catCL.R500[m] )  )
np.savetxt(opts['FNAME_CL_DUPONLY'], dat, fmt=fmtstr, delimiter="\t")

# (3) Output trimmed point source catalogue
# TODO


################################################################################
# Construct Healpix mask for point sources
################################################################################

# Choose (a) or (b)
# (a) Mask all WMAP ptsrcs, except ones identified as clusters. Also saves mask.
##ptsrc_map = map_manipulation.mask_ptsrcs(mask=m_ptsrc_cls, cat=catPS, opts=opts)
# (b) Unmask WMAP ptsrcs identified as clusters. Also saves mask.
ptsrc_map = map_manipulation.unmask_wmap_clusters(mask=-m_ptsrc_cls, cat=catPS, opts=opts)

# Display updated pointsource mask
hp.mollview(ptsrc_map, title="Point source mask", norm='hist')
hp.graticule()


################################################################################
# Report on outputs
################################################################################

# Print report of output files
print ""
print "OUTPUT FILES:"
print "\tCatalogue (fully trimmed):\t", opts['FNAME_CL_TRIMMED']
print "\tCatalogue (duplicates trimmed):\t", opts['FNAME_CL_DUPONLY']
print "\tTrimmed WMAP pointsource mask:\t", opts['FNAME_MASK']
print ""


# Timing information
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
