#!/usr/bin/python
"""Construct a sky map (in the flat sky approximation)."""

import numpy as np
import scipy.signal

class SkyMap(object):
	
	def __init__(self, angsize, res):
		"""Construct a 2D map of a given size and resolution.
		     - angsize is the side length of the map in degrees
		     - res is the no. of arcmin per pixels
		"""
		
		# Calculate map size
		self.angsize_deg = angsize
		self.res = res
		self.npix = int( np.floor(angsize*60. / res) )
		self.angsize = self.npix / self.res # Revised ang. size, in arcmin
		
		# Construct empty map
		self._init_map()
		
		# Get pixel distances
		self.d = self.distances()
	
	def _init_map(self):
		"""Initialise an empty map."""
		self.map = np.zeros((self.npix, self.npix))
	
	def distances(self, x0=0.5, y0=0.5):
		"""Calculate array of pixel distances from map origin, in arcmin."""
		xc = np.zeros((self.npix, self.npix))
		yc = np.zeros((self.npix, self.npix))
		NNx = np.linspace(-x0*self.angsize, (1.-x0)*self.angsize, self.npix)
		NNy = np.linspace(-y0*self.angsize, (1.-y0)*self.angsize, self.npix)
		for i in range(self.npix):
			xc[i,:] = NNx[i]
			yc[:,i] = NNy[i]
		return np.sqrt(xc**2. + yc**2.)
	
	
	def smoothed_map(self, scale=60.):
		"""Return the map, smoothed by a Gaussian on some scale."""
		beam = np.exp( -(self.d**2. / (2.*scale**2.)) ) / (np.sqrt(2.*np.pi)*scale)**2.
		return scipy.signal.fftconvolve(self.map, beam, mode="same")

