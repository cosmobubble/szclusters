#!/usr/bin/python
"""Generate a basic, mock CMB map with a cluster kSZ and tSZ profile, plus 
   noise and CMB. See how difficult it is to recover actual kSZ signal."""

import numpy as np
import cosmolopy as cp
import pylab as P
import scipy.integrate
import scipy.interpolate
import scipy.signal
import sky
from arnaud_profile import ArnaudProfile
from szunits import *

np.random.seed(10)

################################################################################
# Define cluster profile and cosmology
################################################################################

# Define cosmology
cosmo = {
 'omega_M_0': 		0.27,
 'omega_lambda_0': 	0.73,
 'omega_b_0': 		0.045,
 'omega_n_0':		0.0,
 'omega_k_0':		0.0,
 'N_nu':			0,
 'h':				0.7,
 'n':				0.96,
 'sigma_8':			0.8,
 'baryonic_effects': True
}

R500 = 0.6296
M500 = 0.7373e14
z = 0.0396
dA = cp.distance.angular_diameter_distance(z, **cosmo)
fac = dA * np.pi/(180.*60.)

# Universal pressure profile
P0 = 8.403 * (0.7)**(-1.5)
c500 = 1.177
gamma = 0.3081
alpha = 1.0510
beta = 5.4905
v = 300. # km/s
params = [alpha, beta, gamma, c500, P0, M500, R500, z]
CL = ArnaudProfile(params)



################################################################################
# Calculate tSZ and kSZ profiles
################################################################################

# Get tSZ and kSZ profiles
interp_tsz = CL.tsz_profile(nu=17.)
interp_ksz = CL.ksz_profile(v)

# Add them to maps
tsz = sky.SkyMap(angsize=1.0, res=1.) # 1 degree, 10 arcmin/px
tsz2 = sky.SkyMap(angsize=1.0, res=1.)

tsz.map = interp_tsz(tsz.d*fac)
tsz2.map = interp_ksz(tsz.distances(x0=0.4, y0=0.4)*fac)

# Test plot
P.subplot(121)
P.imshow( tsz.map, interpolation="none")

P.subplot(122)
P.imshow( tsz2.map, interpolation="none")
P.show()

exit()










# Populate with noise
Anoise = 2.0e-7 # Noise amplitude
noise = np.random.normal(loc=0.0, scale=Anoise, size=(NPIX,NPIX))



# Do beam convolution
signal_tsz = interp_tsz(rc)
signal_ksz = interp_ksz(rc)



# Try to fit a template
tsz_template = scipy.signal.fftconvolve(interp_tsz(rc), beam, mode="same")
A_actual = np.max(np.abs(tsz_template))
tsz_template /= A_actual # Normalise


def fit_profile(A, template, data):
	"""Return goodness-of-fit of some template to the data."""
	return np.sum(np.sqrt((data - A*template)**2.))

# Choose a signal
signal = obs_tsz #+ obs_ksz + noise

"""
# Perform fitting for range of amplitudes
AA = np.logspace(-5.1, -2.8, 50)
gf = [fit_profile(A, tsz_template, signal) for A in AA]
gf = np.array(gf)
Amin = AA[np.where(gf==np.min(gf))]

# Do this for shifted template
tsz_template2 = np.roll(tsz_template, 5, axis=0) # Different centre for template
gf2 = [fit_profile(A, tsz_template2, signal) for A in AA]
gf2 = np.array(gf2)
Amin2 = AA[np.where(gf2==np.min(gf2))]

print Amin/A_actual, Amin2/A_actual

# Plot results
P.subplot(111)
P.plot(AA, gf, 'r-')
P.plot(AA, gf, 'r+')
P.plot(AA, gf2, 'b-')
P.plot(AA, gf2, 'b+')
P.axvline(A_actual, ls="dashed", color="k")
P.axvline(Amin, ls="solid", color="r", alpha=0.5)
P.axvline(Amin2, ls="solid", color="b", alpha=0.5)
P.xscale('log')
P.show()

exit()

"""

# Test plot
P.subplot(121)
#P.imshow( obs_tsz + obs_ksz + noise , interpolation="none")
P.imshow( tsz_template, interpolation="none")
#P.colorbar()

P.subplot(122)
P.imshow(  obs_ksz + noise , interpolation="none")

P.show()

