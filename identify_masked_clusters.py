#!/usr/bin/python
"""Identify clusters that were masked as point sources by WMAP, but which 
   appear as clusters in MCXC."""

import numpy as np
from catalogues import *
import coordinates
import pylab as P
import time
tstart = time.time()

# Load data from catalogue
Cwmap = CatalogueWmapPointSource()
Cmcxc = CatalogueMCXC()

ra1 = Cwmap.ra; dec1 = Cwmap.dec
ra2 = Cmcxc.ra; dec2 = Cmcxc.dec

# Find neighbours in the catalogue
dd = coordinates.eq_catalogue_distances(ra1, dec1, ra2, dec2)

i1, j1 = np.nonzero( np.where(dd < 0.1, 1., 0.) )
print i1, "\n", j1

# Timing information
print "\nRun took", round(time.time() - tstart, 2), "sec."
