\title{kSZ profile as a function of cluster radius}
\author{Phil Bull}
\date{\today}
\documentclass[12pt]{article}
\usepackage{graphicx, url, listings, wrapfig, float, rotating, caption, subcaption}
\usepackage[bookmarks]{hyperref}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\begin{document}

\maketitle

The fractional temperature change along the line of sight due to the thermal and kinetic Sunyaev-Zel'dovich effects are
\bea
\left . \frac{\Delta T}{T} \right |_{\mathrm TSZ} &=& g(\nu) \frac{\sigma_T}{m_e c^2} \int k_B T(l) n_e(l) dl \\
 &=& g(\nu) \frac{\sigma_T}{m_e c^2} \int P_e(l) dl \\
\left . \frac{\Delta T}{T} \right |_{\mathrm KSZ} &=& -\frac{v_p}{c} \sigma_T \int n_e(l) dl,
\eea
where $g(\nu)$ is the spectral dependence of the thermal effect, and both equations are valid for $\tau \ll 1$.


\section{Thermal SZ profile}

We are interested in finding the radial profiles of the SZ effects. The tSZ profile is given simply by integrating a pressure profile along the line of sight, i.e.
\be
y_{\mathrm SZ}(b) \propto 2 \int_b^{5 R_{500}} P(r) \frac{r dr}{\sqrt{r^2 - b^2}},
\ee
where $b = \Delta \theta d_A$ is the perpendicular distance of the line of sight from the cluster centre. The cluster extent is taken to be $5 R_{500}$ by convention (as described in previous studies, apparently based on simulations). We use the Arnaud {\it et al.} pressure profile \cite{Arnaud}, which is a slightly modified version of the Generalised NFW profile given by Nagai {\it et al.} (\cite{Nagai1}, Eq. A.1),
\be
P(r) \propto \frac{1}{(c_{500} x)^\gamma (1 + (c_{500}x)^\alpha)^{(\beta-\gamma)/\alpha} },
\ee
where $x = r/r_{500}$.


\section{kSZ profile}

For the kinetic effect, a density profile must be chosen. Unfortunately, the GNFW profile appears only to be an empirical fit to observed pressure profiles, so there doesn't seem to be a consistent way of obtaining a unique density profile that corresponds to a given pressure profile.

\subsection{Standard density profiles}

\begin{figure}[ht!]
  \centering
  \includegraphics[scale=0.7]{integrated_density.png}
  \caption{Integrated density profiles, Eq. \ref{eqn-los-density}, for three different concentration parameters. Solid: NFW profile. Dashed: Beta model, with $\beta=0.6$. The concentration parameters, $c_{500}$, are (green, blue, red) (0.5, 1, 2). $x_b$ is the dimensionless impact parameter, $x_b = b/r_{500}$}
  \label{fig-los-density}
\end{figure}

The simplest thing to try is a standard NFW profile,
\be
n_e \propto \frac{1}{x (1 + x)^2}.
\ee
Unfortunately, this is cusped at $r=0$, and so can be awkward to deal with. An alternative is the beta model,
\be
n_e \propto \frac{1}{(1 + x^2)^{\frac{3}{2}\beta}},
\ee
where $\beta \sim 0.6$ empirically (e.g. \cite{KS}). The integrated LOS density,
\be \label{eqn-los-density}
y_{\mathrm KSZ}(b) \propto 2 \int_b^{5 R_{500}} n_e(c_{500}r/r_{500}) \frac{r dr}{\sqrt{r^2 - b^2}},
\ee
is plotted in Figure \ref{fig-los-density} for these two models, and for different concentration parameters.


\begin{figure}[hb!]
  \centering
  \includegraphics[scale=0.6]{locuss-r200-fit.png}
  \caption{Best-fit to LoCuSS $R_{200}$ vs. $R_{500}$ data.}
  \label{fig-locuss}
\end{figure}


\subsection{Universal pressure profile and empirical temperature profile}

An alternative is to take the ``universal'' Arnaud pressure profile, and divide it through by a temperature profile,
\be
n_e(r) = \frac{P_e(r)}{k_B T_e(r)}.
\ee
The mean temperature profile for the REXCESS sample, given by Pratt {\it et al.} (\cite{Pratt}, Eq. 1 and Fig. 5), is empirically fitted by
\be
\frac{T}{T_X} = 1.19 - 0.74\frac{r}{R_{200}}.
\ee
The Arnaud profile is defined in terms of $R_{500}$ rather than $R_{200}$, so we must convert between the two. Since we don't know the density profile, we instead fit a straight line to some LoCuSS/AMI data \cite{Locuss} (which measures both $R_{200}$ and $R_{500}$) to find an approximate relation (see Fig. \ref{fig-locuss}),
\be
R_{200} = 1.729 R_{500}.
\ee
Since $R_{500}$ is generally estimated from the X-ray temperature of the cluster, we can use an empirical relation to work backwards and obtain $T_X$ (\cite{KS} Eq. 35),
\be\label{eqn-tx}
R_{500} = 2.37 \left ( \frac{T_X}{10 ~\mathrm{keV}} \right )^\frac{1}{2} \left ( \frac{h}{0.5} \right )^{-1} \mathrm{Mpc}.
\ee
(This relation is quite old, and has probably been improved.) The approximate temperature profile is then
\be
T(r) = 10 (R_{500} h)^2 \left ( 1 - 0.4\frac{r}{R_{500}}\right ) \mathrm{keV}.
\ee
Note that, because this is derived only from X-ray data, it is only likely to be a good fit within $R_{500}$. In fact, it will go negative outside $2.5 R_{500}$!


\subsection{Universal pressure profile and simulated temperature profile}

Another alternative is to use a ``universal'' temperature profile derived from simulations, as given by Loken {\it et al.} \cite{Loken}:
\bea
T(r) &=& 1.33~ T_X \left ( 1 + 1.5\frac{r}{r_{vir}}\right )^{-1.6} \\
 &=& 11.2 \left (\frac{R_{500}h}{\mathrm{Mpc}} \right )^2 \left ( 1 + 0.75\frac{r}{R_{500}}\right )^{-1.6} ~\mathrm{keV},
\eea
where in the second line we have substituted $r_{vir} \approx 2 R_{500}$ (neglecting a weak dependence on mass; see \cite{KS} Fig. 1), and Eq. (\ref{eqn-tx}) from above. An example temperature profile is plotted in Fig. \ref{fig-temp-loken}. The integrated LOS density, taken by dividing the Arnaud universal pressure profile by the Loken universal temperature profile, is shown in Fig. \ref{fig-loken}.


\begin{figure}[ht!]
  \centering
  \includegraphics[scale=0.6]{temp_loken.png}
  \caption{Universal temperature profile given by Loken {\it et al.}.}
  \label{fig-temp-loken}
\end{figure}

\begin{figure}[ht!]
  \centering
  \includegraphics[scale=0.6]{integrated_density_loken.png}
  \caption{Electron density integrated along the line of sight for three different density profiles: NFW, Beta, and the density profile derived by dividing the Arnaud universal pressure profile by the Loken universal temperature profile.}
  \label{fig-loken}
\end{figure}


\subsection{Recommended profile}

Probably the most sensible kSZ profile to use for now is the Arnaud/Loken profile, since we are using the Arnaud profile for the tSZ effect, and the Loken profile fits simulations well and has no obvious pathologies (unlike NFW and the empirically-fitted temperature profile). In summary, our kSZ radial profile is given by:
\bea
\left [\frac{\Delta T}{T} \right ](b) &=& -\frac{2 v_p}{c} \sigma_T \int_b^{5 R_{500}} \frac{P_e(r)}{T_e(r)} \frac{r dr}{\sqrt{r^2 - b^2}} \\
P_e(r) &=& P_e^{Arnaud}(r) \\
T_e(r) &=& 11.2 \left (\frac{R_{500}h}{\mathrm{Mpc}} \right )^2 \left ( 1 + 0.75\frac{r}{R_{500}}\right )^{-1.6} ~\mathrm{keV}.
\eea


\begin{thebibliography}{999}
 
 \bibitem{Arnaud} M. Arnaud, et al., arXiv:0910.1234.
 
 \bibitem{Birk} M. Birkinshaw, {\it Physics Reports} {\bf 310}, 97 (1999).

 \bibitem{Nagai1} D. Nagai et al., arXiv:astro-ph/0703661v2 (2007).
 
 \bibitem{SZ80} R. Sunyaev \& Ya. Zel'dovich, {\it MNRAS} (1980).
 
 \bibitem{KS} E. Komatsu \& U. Seljak, arXiv:astro-ph/0106151 (2001).
 
 \bibitem{Pratt} G. W. Pratt, {\it A\&A} {\bf 461}, 71 (2007).
 
 \bibitem{Locuss} AMI Consortium, {\it SZ study of 19 LoCuSS galaxy clusters with AMI out to the virial radius} (unpublished).
 
 \bibitem{Loken} C. Loken {\it et al.}, arXiv:astro-ph/0207095 (2002).
 
\end{thebibliography}

\end{document}
