\title{Cleaned MCXC catalogue and WMAP point source mask}
\author{Phil Bull}
\date{\today}
\documentclass[12pt]{article}
\usepackage{graphicx, url, listings, wrapfig, float, rotating, caption, subcaption}
\usepackage[bookmarks]{hyperref}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\begin{document}

\maketitle

The MCXC cluster catalogue and WMAP point source mask need to be cleaned up 
for a number of effects. The following procedures have been applied so far:

\begin{enumerate}
 \item Remove large clusters
 \item Remove neighbours of large clusters
 \item Remove duplicates in the MCXC catalogue
 \item Remove MCXC clusters from WMAP point source catalogue
 \item Remove MCXC clusters that are contaminated by WMAP point sources
\end{enumerate}

These cuts can be applied using the \texttt{clean\_mcxc.py} script in my git 
repo at \url{https://gitorious.org/bubble/szclusters}. The script must be 
modified on line 5 to give the path to an installation of HealPy.

\section{Clusters with large angular size}

Some clusters cover a large angle on the sky. This is problematic for 
Commander, since the cluster profile/map convolution step scales poorly with 
number of pixels per cluster. This leads to a few large clusters taking up most 
of the time spent on this step, slowing down the code. By removing the largest 
clusters, the execution time can be reduced significantly.

Another reason to remove large angular size clusters is that they will be 
better resolved, more obviously displaying substructure and non-sphericity, 
and so their shape is less likely to be well-fitted by the simple model we are 
using.

We trim large angular size clusters from the MCXC catalogue by applying a cut 
on characteristic angular size, $\theta_{500}$. The latter is calculated from 
the raw MCXC catalogue by taking
\be
\theta_{500,i} = R_{500,i} / d_A(z_i),
\ee
where a fiducial cosmology $\{h=0.7,\Omega_m=0.3,\Omega_\Lambda=0.7\}$ 
is assumed in order to calculate $d_A(z)$. We then remove all clusters that 
satisfy $\theta_{500} > \theta_{cut}$.

At the moment, the cut radius is chosen to remove only the extreme tail of the 
distribution in $\theta_{500}$ (shown in Fig. \ref{fig-cluster-size}), which 
is relatively sparse. We currently set $\theta_{cut} = 0.63$ degrees, which 
happens to be comparable to the WMAP beam size. A zoomed-in histogram showing 
only the high-$\theta_{500}$ tail of the distribution is shown in 
Fig. \ref{fig-cluster-size-large}. With this cut, {\bf 16} clusters are 
currently identified with $\theta_{500} > \theta_{cut}$.

\begin{figure}[b!]
  \centering
  \includegraphics[width=0.82\textwidth]{mcxc-cluster-size.png}
  \caption{Characteristic angular radii of clusters in the MCXC catalogue.}
  \label{fig-cluster-size}
\end{figure}

\begin{figure}[t!]
  \centering
  \includegraphics[width=0.82\textwidth]{mcxc-cluster-size-large.png}
  \caption{The largest clusters on the sky, from the MCXC sample. The most 
  extreme outlier is the Virgo cluster.}
  \label{fig-cluster-size-large}
\end{figure}


\section{Neighbours of large angular-size clusters}

Smaller clusters that overlap with the large angular size clusters must also be 
removed from the catalogue, since their measured SZ signal will be contaminated 
by the larger cluster. We remove any cluster whose centre falls within some 
angular radius $\Delta \theta = N \times \theta^{\, large}_{500}$ of the 
centre of a large cluster that failed the cut described above. Currently, we 
choose $N=5$ (for comparison, the virial radius of a cluster is about 
$2 \times \theta_{500}$). A similar maximum radius has been used by others in 
the literature; for example Draper {\it et al.} (\cite{Draper}, p.2) use it to 
define the region within which they fit their SZ template to a cluster, and the 
Planck Collaboration (\cite{Planck1}, Sect. 3.3) use it to define a truncation 
radius for their SZ profile. Arnaud {\it et al.} (\cite{Arnaud}, Sect. 6.1) 
suggest using $N=5$ as the cluster extent on the basis of numerical 
simulations. Fig. \ref{fig-ysph-rexcess} shows that $5 \times \theta_{500}$ 
roughly corresponds to the aperture that contains over 90\% of the integrated 
SZ signal, $Y_{sph}$.

When this cut is applied, {\bf 96} clusters are removed from the sample. The 
cluster with the greatest number of overlapping neighbours is Virgo, which has 
by far the highest $\theta_{500}$ of any cluster in the sample.


\begin{figure}[t!]
  \centering
  \includegraphics[width=0.92\textwidth]{Ysph-rexcess.png}
  \caption{SZ $Y_{sph}$ as a function of integration radius, normalised to its 
  value at $r = 20 R_{500}$. The clusters shown are from the REXCESS sample 
  used by Arnaud {\it et al.} (2010). For most of the clusters, $>90\%$ of the 
  integrated SZ signal is within $5 \times R_{500}$.}
  \label{fig-ysph-rexcess}
\end{figure}


\section{Duplicate clusters}

MCXC is a meta-catalogue, compiled from a number of other catalogues and 
surveys. Some entries in the catalogue have very similar pointings to one 
another, or otherwise seem to be duplicate entries. There are a few reasons 
why catalogued clusters might have similar pointings:

\begin{itemize}
 \item The cluster has been identified in two different surveys, and the 
  entries from each survey have been added to the MCXC catalogue separately 
  (true duplicate).
 \item The clusters form part of a multiple/merging system, so they are 
  actually separate objects, but lie very close to one another (apparent 
  duplicate).
\end{itemize}

Duplicate clusters are problematic for Commander, as they introduce degenerate 
parameters into the analysis. While close pairs of clusters are not technically 
duplicates, they are probably close enough together that they contaminate one 
another, and are likely to be poorly modelled by an Arnaud/GNFW profile. For 
these reasons, we remove clusters from the catalogue that are identified as 
being close to one another in redshift space.

We currently remove pairs of clusters that satisfy 
$\Delta \theta < 0.2^\circ$ (angular separation between cluster centres), 
and $\Delta z < 0.0051$. These numbers are chosen relatively arbitrarily, and 
are conservative in that they only exclude clusters that are very close to one 
another. This cut removes {\bf 20} clusters from the catalogue. It should also 
be noted that this cut may bias clustering statistics.

A more sophisticated way of identifying close pairs might be to find clusters 
that fall within each others' virial radius. This can be done by taking the 
virial radius to be $R_{vir} \approx 2 \times R_{500}$ ($R_{vir}$ is actually 
cosmology- and mass-dependent; see \cite{KS}), and then using a 
fiducial cosmology to calculate the real-space separation between the clusters. 
This might be overkill, and has not been implemented in our code.


\section{Clusters in the WMAP Point Source catalogue}

Point sources must be masked from the CMB maps before they can be analysed. 
The mask used for the 7-year WMAP analysis is available from \cite{WMAPpsmask}. 
This mask is compiled from a combination of point source catalogues from other 
experiments, as detailed on p4 of \cite{WMAPfg}, and WMAP's own point source 
catalogue, the latest version of which is \cite{WMAPpscat}. WMAP's own point 
source catalogue is compiled subject to a mask over the galactic plane 
\cite{WMAPpsgalmask}, so any sources within the galactic plane are from 
auxiliary catalogues.

Some of the entries in WMAP's published point source catalogue are actually 
mis-identified clusters. These clusters must be unmasked for our analysis. To 
do this, we first cross-correlate the MCXC cluster catalogue with the WMAP 
point source catalogue only (\textit{not} the full list of masked WMAP 
sources). We then take the original WMAP point source mask and unmask any 
pointings that correspond to clusters identified in the point source catalogue. 

Clusters are identified in the point source catalogue by finding any MCXC 
cluster that is within $\Delta \theta = N_i \times \theta_{500}$ of a WMAP 
point source. We currently use $N_i=1$, a value that was chosen arbitrarily. 
With the current cuts, {\bf 13} point sources are identified as being clusters.

The output mask FITS file that we produce has \texttt{NSIDE=512}, and uses 
$0.6^\circ$ circular unmasking discs. Masked pixels are set to {\tt 0}, and 
unmasked pixels to {\tt 1}. This matches the format of the published WMAP point 
source mask. The pixels that should be unmasked are identified using the Healpix 
\texttt{query\_disc()} function in its \texttt{inclusive} mode. A small 
padding factor (currently 1.2) is also applied to enlarge the disc radius, to 
ensure that stray single pixels don't remain masked (e.g. due to small pointing 
differences between the catalogues I'm using, and the ones that WMAP used to 
generate the original mask, the discs may not exactly line-up). This has the 
effect of `liberally' unmasking clusters, so that any other masked point 
sources very nearby may also be unmasked slightly.

The procedure described above only tries to identify clusters in WMAP's own 
point source catalogue, and not all of the catalogues that went into producing 
the original WMAP point source mask.
It may be necessary to cross-correlate between the full set of masked WMAP 
sources, rather than just the WMAP-identified sources, especially since one of 
the auxiliary point source catalogues is an X-ray catalogue (that might be more 
prone to selecting clusters). We do not currently do this. In order to 
do this, a full listing of masked point sources would be required. This can be 
compiled from the following sources (list quoted from \cite{WMAPfg}):
\begin{quote}
 \begin{enumerate}
  \item \it All of the sources from Stickel et al. (1994)
  \item Sources with 22 GHz fluxes $\ge$ 0.5 Jy from the Hirabayashi et al. 
   (2000) comprehensive compilation of flat spectrum AGN
  \item Additional flat spectrum objects from Ter\"{a}sranta et al. (2001)
  \item Sources from the X-ray/radio blazar survey of Perlman et al. (1998) 
   and Landt et al. (2001)
 \end{enumerate}
\end{quote}



\section{Clusters contaminated by point sources}

We apply a cut to remove MCXC clusters that are contaminated by WMAP point 
sources. This is done because the point source contamination may cause the SZ 
signal to be estimated incorrectly. (Ideally, the point source would be 
subtracted so that the cluster SZ signal can still be measured, but we do not 
do that here.)

Any cluster that has a WMAP point source within 
$\Delta \theta = N_c \times \theta_{500}$ of its centre (but further away 
than $N_i \times \theta_{500}$; see previous section) is cut from the 
catalogue. With our current cuts ($N_c=5$, $N_i=1$), {\bf 61} clusters are 
masked from the catalogue due to point source contamination.

This procedure could be improved by studying the effects of point source 
contamination on the recovered SZ signal in basic simulations. An optimal 
exclusion radius could then be found (which will probably be dependent on the 
point source flux). The present cutting criteria were chosen arbitrarily. 

Another improvement would involve using the full list of WMAP masked point 
sources, including those from auxiliary catalogues, rather than only those in 
the WMAP Point Source Catalog. This would result in more clusters being cut due 
to contamination. At the moment, we are leaving clusters in the catalogue that 
may be contaminated by non-WMAP catalogue point sources.


\section{Summary of current cuts}

The current set of cuts, and their results, are summarised in Table 
\ref{tbl-summary}.

\begin{table}[h]
 \begin{tabular}{|l|l|l|}
 \hline
 {\bf Cut} & {\bf Threshold} & {\bf No. Clusters} \\
 \hline
 Large angular size & $\theta_{500} > 0.63^{\circ}$ & 16 \\
 Overlap with large ang. size & $\Delta \theta < 5 \theta^{\,large}_{500}$ & 96 \\
 Duplicates & $\Delta \theta < 0.20^{\circ}$ \& $\Delta z < 0.0051$ & 20 \\
 Point source contamination & $\theta^{cl}_{500} < \Delta \theta^{cl}_{ps} < 5 \theta^{cl}_{500}$ & 61 \\
 \hline
 {\bf Total masked:} & & 172 / 1743 \\
 \hline
 \end{tabular}
 \caption{Cuts applied to the MCXC cluster catalogue. Note that some clusters 
 are removed in more than one cut, hence the total number of cut clusters 
 is 172.}
 \label{tbl-summary}
\end{table}

\begin{thebibliography}{999}

 \bibitem{Draper} P. Draper {\it et al.}, arXiv:1106.2185 (2011).
 
 \bibitem{Planck1} Planck Collaboration, arXiv:1204.2743 (2012).
 
 \bibitem{Arnaud} M. Arnaud, et al., arXiv:0910.1234.
 
 \bibitem{WMAPpsmask} \url{http://lambda.gsfc.nasa.gov/product/map/dr4/masks_info.cfm}, (file: \texttt{wmap\_temperature\_source\_mask\_r9\_7yr\_v4.fits})

 \bibitem{WMAPpsgalmask} \url{http://lambda.gsfc.nasa.gov/product/map/dr4/masks_info.cfm}, (file: \texttt{wmap\_point\_source\_catalog\_mask\_r9\_7yr\_v4.fits})
 
 \bibitem{WMAPfg} C.L. Bennett, et al., {\it ApJS} {\bf 148}, 97 (2003) [arXiv:astro-ph/0302208v2].
 
 \bibitem{WMAPpscat} WMAP Point Source Catalog, 5-band, v4, \url{http://heasarc.gsfc.nasa.gov/W3Browse/all/wmapptsrc.html}
 
 \bibitem{KS} E. Komatsu \& U. Seljak, arXiv:astro-ph/0106151 (2001).
 
\end{thebibliography}

\end{document}
