\title{Measuring the kSZ effect in the Coma cluster}
\author{Phil Bull}
\date{\today}
\documentclass[12pt]{article}
\usepackage{graphicx, url, listings, wrapfig, float, rotating}
\usepackage[bookmarks]{hyperref}
\usepackage[section]{placeins}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\begin{document}

\maketitle

The Coma cluster (also designated A1656; see NED for details \cite{NED:Coma}) 
has an apparently anomalous mass-to-light ratio. This could possibly have been 
caused by an incorrect distance estimate for Coma due to a large peculiar 
velocity. If Coma had a peculiar velocity such that its actual distance from us 
was 85 Mpc (rather than $\sim 100$ Mpc), its M/L ratio would be more normal. 
This would require a large peculiar velocity for Coma, of order 1000 km/s away 
from us. It would be interesting to see what sort of kSZ measurement we could 
get for Coma.

\section{Previous measurements}

A number of previous distance and peculiar velocity measurements for the Coma 
cluster exist in the literature, made using a range of methods. A non-exhaustive 
list is given in Tables \ref{tbl-distance} and \ref{tbl-vpec}, and the data 
are plotted in Fig. \ref{fig-dist-vpec}.

\begin{table}[ht]
 \begin{tabular}{|l|c|l|}
 \hline
 {\bf Method} & {\bf Distance (Mpc)} & {\bf Reference} \\
 \hline
 Thermal SZ $d_A(z)$ & $103 \pm 42$ & De Filippis et al. \cite{ComaDist1}, Table 3 \\
 Thermal SZ $d_A(z)$ & $96^{+4}_{-5}$ & Reese et al., in \cite{ComaDist1}, Table 3 \\
 Thermal SZ $d_A(z)$ & $235^{+218}_{-98}$ & Mason et al., in \cite{ComaDist1}, Table 3 \\
 Surface Brightness Fluc. & $102\pm14$ & Thomsen et al. \cite{ComaDist2} \\
 Surface Brightness Fluc. & $85\pm10$ & Jensen et al. \cite{ComaDist4} \\
 Surface Brightness Fluc. & $100\pm10$ & Liu \& Graham \cite{ComaDist5} \\
 Early type galaxies & $105\pm11$ & Tanvir et al. \cite{ComaDist3} \\
 \hline
 \end{tabular}
 \caption{Previous distance measurements to Coma.}
 \label{tbl-distance}
\end{table}

\begin{table}[ht]
 \begin{tabular}{|l|c|l|}
 \hline
 {\bf Method} & {\bf Pec. Vel. (km/s)} & {\bf Reference} \\
 \hline
 Tully-Fisher & $+170\pm210$ & Giovanelli et al. \cite{Comavp1}, Table 1 \\
 Tully-Fisher & $+212\pm186$ & Giovanelli et al. \cite{Comavp1}, Table 1 \\
 Fundamental Plane & $-29\pm299$ & Colless et al. \cite{Comavp2}, Table 7 \\
 kSZ & $-300 \pm 500$ & Hansen \cite{Comavp3}, p7\\
 \hline
 \end{tabular}
 \caption{Previous peculiar velocity measurements of Coma.}
 \label{tbl-vpec}
\end{table}

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.7]{coma-dist-vpec.png}
  \caption{Distances and peculiar velocities to Coma measured using a variety 
  of methods, as summarised in Tables \ref{tbl-distance} and \ref{tbl-vpec}.}
  \label{fig-dist-vpec}
\end{figure}


\FloatBarrier
\section{SZ maps and pressure profile}

The SZ effect in the Coma cluster has been detected from CMB maps before. 
Bielby \& Shanks \cite{Bielby} find a clear decrement along the LOS to Coma in 
the WMAP 3-year data (see their Fig. 7). Komatsu et al. (\cite{Komatsu}, Sect. 
7.2) present a detection of Coma in the WMAP 7-year data, and compare their 
result with previous SZ observations of the cluster. The Planck Collaboration 
have detected Coma with high significance \cite{PlanckClusters}.

I was unable to find a pressure profile for the Coma cluster in the literature. 
It does not seem to have been included in the REXCESS sample, so no Arnaud 
profile parameters are available for it. It has, however, been subject to 
detailed X-ray studies, some of which have produced high-resolution pressure 
maps, so the data do exist. For example, Fig. 3 of Schuecker et al. 
\cite{Schuecker} shows both temperature and pressure maps of Coma, taken using 
XMM-Newton. The raw data from XMM-Newton are publicly available, but they would 
need to be reduced in order to derive a pressure profile.


\section{Coma overlap with MCXC clusters}

Both the Coma and Virgo clusters cover large angles on the sky. If the two 
overlap within $15\times R_{500}$ of one another, Commander may need to 
process them jointly, and so a large area of the sky containing many clusters 
will need to be analysed in order to measure only the kSZ effect from Coma.

Virgo covers a large angle on the sky, and is relatively close to Coma 
($\Delta = 17^\circ$ separates the cluster centres). Coma has an angular 
size of $\theta_{500} = 0.68^\circ$, and Virgo has 
$\theta_{500} = 2.8^\circ$. In terms of their angular sizes, the two clusters 
are separated by
\bea
\Delta &=& 25.1\times\theta^\mathrm{Coma}_{500} \\
        &=& 6.1\times\theta^\mathrm{Virgo}_{500}.
\eea
The maximum radius without overlap for the two cluster is $4.89 \times \theta_{500}$, 
which is much less than the $15\times\theta_{500}$ that may be required by 
Commander. It is, however, quite close to the $5\times\theta_{500}$ figure 
used in other studies.

Other clusters overlap with Coma. There are 6 with centres within 
$5 \times \theta_{500}$ of Coma, and 41 within $15 \times \theta_{500}$.


\section{Email from Ryan, 12 June 2012}

(Provided for ease of reference.)

\begin{quote}
Dear Pedro, Dear Edward,

cc: Elena, our co-I in Padova
cc: Jo \& Phil, who I just had a quick chat with

A week ago, Roger and I quizzed you about the peculiar velocity of the
Coma cluster... here's some more details.

I think that Coma may be moving at ~1000km/s wrt the CMB (away from us
and along our line of sight). This is assuming the redshift of the
cluster is 0.024 (Han \& Mould 1992) while the (radial comoving)
distance is 85 Mpc (not ~100Mpc as a redshift of 0.024 would suggest).
There are good reasons for this:
  1) Although Thomsen et al. 1997 use SBF to infer a distance of
102+-14 Mpc (which gives a satisfying H0=71 km/s using the Han \& Mould
Vrec), the single galaxy they study the SBF of is not in the core of
the cluster, but is some considerable distance to the north
  2) Jensen et al. 1999 performed NIR SBF on one of the two central cD
galaxies in the cluster, and found a distance of 85 +- 10 Mpc.
  3) D'Onofrio et al. 1997 used the fundamental plane to infer that
the distance modulus between Virgo and Coma is 3.55 +- 0.15. This
suggests Coma is 5.12 times further than Virgo; SBF studies of
multiple galaxies in Virgo (Mei et al. 2007) tell us Virgo is 16.5 +-
0.1 Mpc away, putting Coma at 84.6 Mpc.
  4) In van Dokkum \& van der Marel 2007, they plot how the
mass-to-light of massive galaxies in clusters varies up to z~1. Coma
is a big outlier (Fig. 6) and could be strongly biasing this result.
Interestingly, they assume that Coma is at rest with the CMB and at
z=0.0246 (Smith, Lucey \& Hudson 2006)!
  5) In our own work, we also agree that Coma is odd in it's M/L
ratio, if you assume it's at rest wrt the CMB at z=0.024. But this
problem *is* resolved if we put Coma at 85 Mpc; the cosmological
corrections need to become non-standard, using z=0.024 for all
redshift effects (bandpass, energy, arrival rate) and z=0.02021 (for
D=85 Mpc) for the distance modulus and arcsec-kpc conversions. Tolman
dimming becomes awkward: I'm using 5log(1+0.024) + 5log(1+0.02021),
rather than 10log(1+z).

However, I'm not sure if something as massive as Coma is expected to
be travelling so rapidly wrt the CMB... is a peculiar velocity of 1000
km/s for a cluster of mass $\sim 10^{15}$ M\_sun acceptable? From talking to
Phil, it seems not (see p.s. at end). There are caveats:
  1) the Han \& Mould 1992 recession velocity was calculated from
spiral galaxies in Coma, which may not be highly representative of the
cluster (they'll be in-falling, and at large radii; see the spirals in
Virgo, Chung, A. et al. 2009). However, Phil wisely suggested I check
the peculiar (absorption line) velocities of the (mostly early-type)
galaxies (a lot are in SDSS); this has a peak at around z=0.0233.
  2) I don't know if putting Coma at rest between 85 Mpc and 102 Mpc
would also solve the (M/L) problem. Certainly, there isn't any need to
force z=0.024 and D=85 Mpc; z=0.02021 and D=85 Mpc works just as well
for me.

If you have any comments on this, they'd be very welcome. In
particular, is there any other evidence to suggest Coma has a high
peculiar velocity wrt the CMB? KSZ? I'm looking into some references
that Phil gave me.

Regards
Ryan

p.s. From talking to Phil, it seems if Coma were moving at 1000km/s
wrt the CMB, it would cause some problems for LCDM. We could try and
measure the Vpec for a handful of local clusters using FP distances
and galaxy redshifts.... I'm not sure if any of you are already doing
something similar to this (Ed?), but if not, it would make a nice
MPhys project as all the (reduced) data should be available.
\end{quote}

\FloatBarrier
\begin{thebibliography}{999}

 \bibitem{Arnaud} Arnaud, M. {\it et al.}, arXiv:0910.1234
 
 \bibitem{NED:Coma} NED (NASA/IPAC Extragalactic Database): Coma Cluster, \url{http://is.gd/tM9JWD}.
 
 % Coma distance measurements
 \bibitem{ComaDist1} E. De Filippis, M. Sereno, M. W. Bautz \& G. Longo, arXiv:astro-ph/0502153 (2005).
 
 \bibitem{ComaDist2} B. Thomsen et al., arXiv:astro-ph/9704121v2 (1997).
 
 \bibitem{ComaDist3} N. Tanvir et al., arXiv:astro-ph/9509160 (1995).
 
 \bibitem{ComaDist4} J. B. Jensen et al., arXiv:astro-ph/9807326 (1999).
 
 \bibitem{ComaDist5} M. C. Liu \& J. R. Graham, ApJ, 557, L31 (2001).
 
 % Coma peculiar velocity measurements
 \bibitem{Comavp1} R. Giovanelli et al. Ap.J., 116, 2632 (1998)
 
 \bibitem{Comavp2} M. Colless et al., arXiv:astro-ph/0008418 (2000).

 \bibitem{Comavp3} S. H. Hansen, arXiv:astro-ph/0310149 (2003).
 
 % CMB maps containing Coma
 \bibitem{Bielby} R. M. Bielby \& T. Shanks, Mon. Not. R. Astron. Soc., 382, 1196 (2007).
 
 \bibitem{Komatsu} E. Komatsu et al., arXiv:1001.4538v3 (2010).
 
 \bibitem{PlanckClusters} Planck Collaboration, arXiv:1101.2024v2 (2011).
 
 % Coma pressure profile data
 \bibitem{Schuecker} P. Schuecker et al., arXiv:astro-ph/0404132v2 (2004).
 
\end{thebibliography}

\end{document}
