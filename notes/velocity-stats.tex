\title{Measuring peculiar velocities and velocity statistics}
\author{Phil Bull}
\date{\today}
\documentclass[12pt]{article}
\usepackage{graphicx, url, listings, wrapfig, float, rotating, caption, subcaption}
\usepackage[bookmarks]{hyperref}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\begin{document}

\maketitle
\tableofcontents

\section{Methods of measuring peculiar velocities}

\subsection{Galaxy surveys}

These typically use galaxy redshifts and an independent distance 
indicator to infer peculiar velocities.

\begin{description}
  \item[Tully-Fisher] \hfill \\
  Luminosity distance indicator from spiral galaxy luminosity--rotational
  velocity relation. Has ~20\% distance errors; limited to small 
  volumes (\cite{Strauss} Sect. 6.1.1).
  
  \item[Faber-Jackson] \hfill \\
  Luminosity distance indicator from elliptical galaxy 
  luminosity--velocity dispersion relation. Also has ~20\% distance 
  errors (\cite{Strauss} Sect. 6.1.2).
  
  \item[Surface Brightness Fluctuations] \hfill \\
  Luminosity distance indicator using Poisson statistics of pixel 
  brightness, using the fact that the brightness in each pixel is the 
  result of an aggregation of stars. Errors depend on image resolution, 
  but can be better than TF or FJ (\cite{Strauss} Sect. 6.3.1).
  
  \item[BCG Luminosity/Shape] \hfill \\
  Luminosity distance indicator using a standard luminosity profile 
  shape for Brightest Cluster Galaxies. Distance errors around 16\%, 
  but expected to be useful for higher redshift than TF/FJ since BCGs 
  are more luminous (\cite{Strauss} Sect. 6.3.2).
  
  \item[Type Ia SN] \hfill \\
  Luminosity distance indicator using standardised supernova 
  lightcurves (\cite{Strauss} Sect. 6.3.3).
  
  \item[Deviations from mean galaxy magnitude-redshift relations] \hfill \\
  Galaxy magnitudes are plotted as a function of redshift for a large 
  redshift survey, and a mean relation is fitted to the data. The 
  deviation from the mean of individual galaxies is taken to be due to 
  the peculiar velocity of that galaxy \cite{Nusser}.
  
  \item[Reconstruction from the density field] \hfill \\
  The density and velocity fields are related to one another in linear 
  perturbation theory by the continuity equation. Given a 
  reconstruction of the density field (e.g. from a galaxy redshift 
  survey), the velocity field can therefore also be reconstructed \cite{Shao}.
\end{description}

\subsection{Kinetic SZ}

The kinetic SZ effect measures velocities without requiring a distance 
indicator. It can be extracted by fitting a frequency-dependent angular 
template in Fourier space, or by cross-correlating with a tracer of the 
density field or similar.

\begin{description}
  \item[Fourier space template fitting] \hfill \\
  CMB experiments can jointly fit a kSZ template along with other 
  templates in Fourier ($C_\ell$) space (e.g. \cite{Dunkley} Sect. 
  2.1.2). The kSZ angular power spectrum $\ell(\ell+1) C_\ell$ 
  appears to be relatively flat at high $\ell$ (\cite{Dunkley} Fig. 1), 
  where the thermal SZ spectrum is also relatively flat. If an 
  experiment has multiple frequency channels, the two can be more 
  readily distinguished by using the different frequency dependences of 
  the effects.
  
  \item[Correlation with galaxy clusters] \hfill \\
  The gas distribution in galaxy clusters can be modelled, to an 
  approximation, by a smooth, spherical electron density profile. This 
  can be used to derive a projected kSZ template. The CMB sky signal 
  can then be correlated with cluster pointings using this template. 
  Some studies do not use templates, instead assuming that the clusters 
  are unresolved and the whole cluster SZ contribution is integrated 
  into a single pixel (e.g. Hand et al. \cite{Hand}, Kashlinsky et al. 
  \cite{Kashlinsky}).
  A number of different types of cluster catalogue can be used to 
  provide the cluster pointings (and, potentially, the parameters for 
  their individual kSZ profiles):
  \begin{itemize}
   \item {\it X-ray catalogues}: These contain relatively large 
   samples of clusters (the biggest have 300--500) and have good 
   estimates of cluster mass/radius, but tend to be X-ray flux/luminosity 
   limited. A range of cluster masses are probed, and not just the most 
   massive. Larger compilation catalogues exist (e.g. MCXC), but these 
   are inhomogeneous samples.
   \item {\it SZ catalogues}: Small-scale CMB experiments are now able 
   to make blind detections of SZ clusters, resulting in mass-limited 
   samples. However, only around 100 clusters have been detected in 
   this way to date, most of them at the very high mass end of the halo 
   mass function. Their mass/radius can be determined from follow-up 
   observations using various scaling relations, depending on the 
   follow-up method. Some of these scaling relations are discrepant 
   with one another, or poorly calibrated.
   \item {\it Luminous galaxy cluster catalogues}: Luminous galaxies 
   (e.g. BCGs, LRGs) can be used to trace clusters and galaxy groups. 
   Large samples of these (tens of thousands) have been found in galaxy 
   redshift surveys, over a wide range of redshift and cluster/group 
   mass. The number and separation of neighbouring galaxies may also be 
   used to provide cluster mass and radius information through cluster 
   richness scaling relations (e.g. Draper et al. \cite{Draper}).
  \end{itemize}
  
  \item[Correlation with large galaxies] \hfill \\
  Galaxies are thought to be surrounded by coronae of ionised gas, the 
  WHIM. Though the gas is not in galaxies themselves, the WHIM should 
  nevertheless be correlated with large galaxies. By correlating galaxy 
  positions (plus an appropriate template for the kSZ profile of each 
  galaxy) with a CMB map, the kSZ (actually, Ostriker-Vishniac?) signal 
  can be extracted \cite{Lavaux}. \\
  Lavaux et al. \cite{Lavaux} argue that although cluster gas profiles 
  (and thus kSZ profiles) are much better understood than those for the 
  WHIM, galaxy surveys contain far more sources than cluster surveys, 
  and a greater amount of electrons can be found near to galaxies than 
  are in the gas component of galaxy clusters. This should make it 
  possible to obtain smaller errors on kSZ measurements using this 
  technique, rather than by correlating with clusters. They also point 
  out that beam-dependent effects may be important, as the galaxy 
  coronae are smaller than clusters, and thus less well resolved.
  
  \item[Pairwise correlations] \hfill \\
  Clusters/galaxies may be correlated with a CMB sky map in a pairwise 
  fashion. This is a simple extension of the correlation methods 
  listed above, except rather than attempting to measure the peculiar 
  velocities of single objects, the relative velocities of pairs of 
  objects is estimated, providing a more or less direct measurement of 
  pairwise velocity statistics such as $v_{12}(r)$ (Hand et al. 
  \cite{Hand}).
  
  \item[kSZ tomography] \hfill \\
  Shao et al. \cite{Shao} propose a novel kSZ/galaxy redshift 
  cross-correlation method to extract the kSZ signal tomographically. 
  Spectroscopic galaxy redshift surveys can be used to reconstruct the 
  3D density field, which can then be used to reconstruct the 3D 
  velocity field by using the mass conservation (continuity) equation 
  of linear perturbation theory,
  \be
  \dot{\delta}_m + \nabla \cdot \vec{v} = 0.
  \ee
  The kSZ signal can suffer from cancellations along the line of sight, 
  since the velocity pdf is symmetric about zero. Shao et al. 
  (\cite{Shao} Sect. 2.1) construct an estimator that traces the 
  reconstructed velocity field along the line of sight, and weight it 
  by density (Eq. 4),
  \be
  \hat{\Theta} = \int_\chi^{\chi+\Delta\chi} d\chi^\prime (1+ \delta) ~\vec{v}\cdot \hat{r}~ W(z),
  \ee
  where $W(z)$ is the kSZ window function (which depends on optical 
  depth), and the integral is over a bin in redshift (although here 
  written as proper distance $\chi$). \\
  They argue that this will be tightly correlated with the 
  kSZ signal, since this is also proportional to the density-weighted 
  velocity along the LOS. Therefore, the cross-correlation of the 
  estimator $\hat{\Theta}$ with the CMB sky signal $\Theta$ should 
  pick out the kSZ effect while avoiding null correlations (i.e. the 
  estimator has LOS cancellations where the kSZ signal has 
  cancellations). This should be a clean method of extraction 
  (contaminants like the primary CMB and the thermal SZ effect should 
  be uncorrelated with $\hat{\Theta}$), and allows for tomography; 
  by correlating the sky signal with the estimator binned in redshift, 
  the redshift dependence of the kSZ signal can in principle be picked 
  out.
  
\end{description}




\section{Velocity statistics}

This section summarises some of the statistics of the velocity field 
that may be computed. Note that \cite{BK} has a useful forecast 
comparison for three of the statistics.

\begin{description}
  \item[Bulk flows] \hfill \\
  Most studies of coherent large-scale (bulk) flows consider only the 
  dipole moment of the velocity field (e.g. \cite{Strauss} Sect. 7.1, 
  Kashlinsky et al. \cite{Kashlinsky}). This can be found simply by 
  fitting a dipole to the velocity distribution over the sky while 
  accounting for the peculiar velocity of the observer. Fitting only a 
  dipole, when there are in fact higher moments of the velocity field, 
  can lead to incorrect estimation of the dipole.
  
  \item[Moments of the velocity field] \hfill \\
  The velocity field can be expanded in an infinite series of moments 
  about the observer,
  $v_i(\vec{r}) = U_i + U_{ij}r_j + U_{ijk}r_i r_j + ...$ 
  (dipole, shear, octupole, etc.). Since only line-of-sight velocities 
  are observable, and we are only interested in large-scale coherent 
  motions (i.e. not small-scale velocities of individual galaxies), 
  individual galaxy peculiar velocity measurements must be weighted 
  in some way to ensure accurate reconstruction of the low-order 
  moments (Macaulay et al. \cite{Macaulay} Sect. 2.2). \\
  Each moment consists of a number of independent components (e.g. 3 
  for the dipole $U_i$, 6 for the shear $U_{ij}$ \cite{Macaulay}). One 
  can write down an expression for the covariance between different 
  components of the moments (labeled $p,q$) which depends on 
  the matter power spectrum (\cite{Macaulay} Eq. 11),
  \be
  \sigma^{(v)}_{pq} = \frac{f^2}{2 \pi^2} \int_0^\infty P(k) W^2_{pq}(k) dk
  \ee
  Given weighted measurements of the 
  line-of-sight velocities in a catalogue, the moments of the measured 
  velocity field can be computed. One can then write down a likelihood 
  and maximise it to find constraints on cosmological parameters (which 
  enter the calculation through the power spectrum).
  
  \item[Maximum likelihood catalogue method] \hfill \\
  Given the matter power spectrum, it is possible to calculate the 
  expected correlations between velocities at any two points (Macaulay 
  et al. \cite{Macaulay} Eq. 7). One can write down a likelihood for a 
  catalogue of positions/velocities by calculating the correlations 
  between every pair of objects in the catalogue and using them in a 
  `velocity' covariance matrix,
  \be
  \sigma^{(v)}_{mn} = \frac{H_0^2 a^2 f^2}{2 \pi^2} \int_0^\infty P(k) f_{nm}(k) dk,
  \ee
  where the objects in the pair are labeled by $n,m$ and $f_{nm}$ is a 
  window function that depends on the separation and orientation of 
  each pair. Only the line-of-sight velocities, 
  $S_m = \hat{r}_m \cdot \vec{v}_m$, are measurable, and this must be 
  accounted for in the calculation of $f_{nm}$ \cite{Macaulay}. 
  Another `error' covariance matrix, $\sigma^{(e)}_{mn}$, 
  should then be added to this to take into account measurement errors 
  and the like, giving
  \be
  \log \mathcal{L} \sim - S_m \left [ (\sigma^{(v)}_{mn})^2 + (\sigma^{(e)}_{mn})^2\right ]^{-1} S_n
  \ee
  Since $\sigma^{(v)}_{mn}$ is a function of the power spectrum, 
  maximising the likelihood will give constraints on the cosmological 
  parameters that $P(k)$ depends on.
  
  \item[RMS peculiar velocity] \hfill \\
  The RMS peculiar velocity, $\sigma_v$, is a simple integral of the 
  power spectrum convolved with some window function that smooths out 
  power below cluster scales \cite{Watkins},
  \be
  \sigma_v^2 = \frac{1}{3}\frac{H_0^2 \Omega_0^{1.2}}{2 \pi^2} \int_0^\infty P(k) W^2(k, R) dk.
  \ee
  (There is some ambiguity in the choice of 
  smoothing scale, but $R \sim 1.5 h^{-1}$ Mpc is reasonable.) A 
  simple likelihood function may be written down for a catalogue of 
  peculiar velocity measurements (\cite{Watkins} Eq. 6),
  \be
  \log \mathcal{L} \sim \frac{-v_i^2}{\sigma_i^2 + \sigma_v^2},
  \ee
  where the dependence on cosmological parameters enters through 
  $\sigma_v$.
  
  \item[Pairwise streaming velocity/momentum] \hfill \\
  The pairwise velocity $\vec{v}_{12}(r)$ quantifies the fact that 
  overdensities in the matter field will tend to fall towards one 
  another. It is a function of object separation $r$, and is related 
  to the correlation function $\xi(r)$ by
  \bea
  \vec{v}_{12} &=& \frac{\langle (\vec{v}_1 - \vec{v}_2)(1+\delta_1)(1+\delta_2) \rangle}{1 + \xi(r)} \\
   &\propto& \langle v_1 \delta_2 - v_2 \delta_1 \rangle
  \eea
  There are non-linear effects that become important at small $r$, 
  which have been modeled using an analytic approximation 
  (Ferreira et al. \cite{Ferreira}), 
  \bea
  v_{12}(r) &=& -\frac{2}{3} H r f \bar{\xi}(r) [1 + \alpha \bar{\xi}(r)] \\
  \bar{\xi}(r) &=& (3/r^3) [1 + \xi(r)]^{-1} \int_0^r \xi(x) x^2 dx
  \eea
  Ferreira et al. \cite{Ferreira} also write down an estimator using 
  only line-of-sight velocities. This statistic can be measured from 
  catalogues with position, velocity and mass/density information 
  (e.g. Hand et al. \cite{Hand}, except they do not use the mass 
  information fully), and must be binned in $r$. Alternatively, it can 
  be derived from measurements of the correlation function. The 
  shape and normalisation of $v_{12}$ can be used to constrain 
  cosmological parameters.
  
  \item[Pairwise velocity dispersion] \hfill \\
  Bhattacharya \& Kosowsky \cite{BK} give an expression for the 
  dispersion about the mean pairwise velocity $v_{12}$ (Eq. 10),
  \be
  \sigma_{12}^2(r) = 2\frac{\langle \sigma_v^2(R)\rangle + \xi^\mathrm{lin}(r)\langle b \sigma_v^2(R)\rangle}{1 + b^2 \xi^\mathrm{lin}},
  \ee
  where $R$ is the characteristic smoothing scale for clusters (down to 
  some lower mass limit). This can be measured from the same datasets 
  as the mean pairwise velocity, and is essentially the error on 
  $v_{12}(r)$ in each $r$ bin. This statistic is (probably) more 
  sensitive to $P(k)$ on larger scales, since the power spectrum is 
  weighted by additional inverse powers of $k$ in the integrand of 
  $\sigma_v^2$.
  
  \item[Line-of-sight velocity pdf] \hfill \\
  An expression for the redshift-dependent pdf of the line-of-sight 
  peculiar velocity is given by Bhattacharya \& Kosowsky 
  \cite{BK} (Eq. 1),
  \be
  f(v, z) = \frac{\int dM M n(M|\delta) p(v|M, \delta) }{\int dM M n(M|\delta) }.
  \ee
  This expression can be rewritten in terms of the halo mass function 
  $n(M)$ and a Gaussian model for the velocities (\cite{BK} Eq. 6),
  \be
  p(v|M,\delta) \sim \frac{1}{\sigma_v(M)} \exp \left (-\frac{(3 v)^2}{\sigma^2_v(M)} \right ).
  \ee
  $f(v, z)$ can be measured from a velocity catalogue by plotting the 
  relative frequency of catalogue objects in bins of velocity and 
  redshift. For high-mass cluster samples, this statistic should 
  inherit some of the sensitivity of $n(M)$ to (e.g.) $\sigma_8$ and 
  $\Omega_m$ due to its exponential cut-off at high masses. (This is 
  borne out by Table 4 of \cite{BK}.)
  
  \item[Velocity extreme value statistics] \hfill \\
  It might be possible to apply extreme value statistics to velocity 
  catalogues, just as they have been applied to the density field (e.g. 
  Harrison \& Coles \cite{Harrison}). This could be used to put 
  constraints on non-Gaussianity, for example.
  
\end{description}


\begin{thebibliography}{999}
 
 \bibitem{Strauss} M.A. Strauss \& J.A. Willick, {\it Physics Reports} {\bf 261}, 271-431 (1995).
 
 \bibitem{Nusser} A. Nusser, E. Branchini \& M. Feix, arXiv:1207.5800 (2012).
 
 %\bibitem{Shaw} L.D. Shaw, D.H. Rudd \& D. Nagai, arXiv:1109.0553
 
 \bibitem{Dunkley} ACT Collaboration; J. Dunkley et al., arXiv:1009.0866 (2010).
 
 \bibitem{Lavaux} G. Lavaux, N. Afshordi \& M.J. Hudson, arXiv:1207.1721 (2012).
 
 \bibitem{Kashlinsky} A. Kashlinsky, F. Atrio-Barandela, D. Kocevski \& H. Ebeling, arXiv:0809.3734 (2008).
 
 \bibitem{Hand} N. Hand et al., arXiv:1203.4219 (2012).
 
 \bibitem{Draper} P. Draper, S. Dodelson, J. Hao \& E. Rozo, arXiv:1106.2185 (2011).
 
 \bibitem{Shao} J. Shao, P. Zhang, W. Lin, Y. Jing, J. Pan, arXiv:1004.1301 (2010).
 
 
 \bibitem{Watkins} R. Watkins, arXiv:astro-ph/9710170 (1997).
 
 \bibitem{Macaulay} E. Macaulay et al., arXiv:1111.3338 (2011).
 
 \bibitem{Ferreira} P.G. Ferreira, R. Juszkiewicz, H.A. Feldman, M. Davis \& A.H. Jaffe, arXiv:astro-ph/9812456 (1998).
 
 \bibitem{BK} S. Bhattacharya \& A. Kosowsky, {\it ApJ} {\bf 659}, L83–L86 (2007).
 
 \bibitem{Harrison} I. Harrison \& P. Coles, arXiv:1108.1358 (2011).

 
\end{thebibliography}

\end{document}
