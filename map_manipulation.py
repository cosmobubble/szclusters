#!/usr/bin/python
"""Mask or unmask pixels on a HealPix map."""

import sys
sys.path.append("/home/phil/postgrad/lib") # HealPy path
import numpy as np
import healpy as hp


def mask_ptsrcs(mask, cat, opts):
	"""Mask-out a list of point source pointings. See clean_mcxc.py for 'opts'
	   variable definitions."""
	
	# Pointings of masked catalogue, in radians, with angular shift applied
	# 0 < l < 2pi; -pi/2 < b < pi/2, but need b = [0, pi] for Healpix
	new_l = cat.l[mask] * np.pi/180.
	new_b = 0.5*np.pi - (cat.b[mask] * np.pi/180.)
	
	# Construct new, empty mask map
	hpmask = np.ones(hp.nside2npix(opts['NSIDE']))

	# Loop through sources, getting their pixel indices and masking them out on the map
	for i in range(new_l.size):
		vec = hp.pixelfunc.ang2vec(theta=new_b[i], phi=new_l[i])
		idxs = hp.query_disc(opts['NSIDE'], vec, opts['PS_MASK_RAD'])
		hpmask[idxs] = 0.0 # Mask pixels (0=masked, 1=unmasked)
	
	# Output mask map
	hp.write_map(opts['FNAME_MASK'], hpmask)
	return hpmask



def unmask_wmap_clusters(mask, cat, opts):
	"""Unmask a list of pointings that are clusters mis-identified as WMAP 
	   point sources. Loads the full WMAP point source map, then unmasks 
	   cluster pointings. Note that the 'mask' arg should be True only for 
	   clusters in the ptsrc catalogue."""
	
	# Pointings of clusters in catalogue, in radians, with angular shift applied
	# 0 < l < 2pi; -pi/2 < b < pi/2, but need b = [0, pi] for Healpix
	new_l = cat.l[mask] * np.pi/180.
	new_b = 0.5*np.pi - (cat.b[mask] * np.pi/180.)

	# Load the original WMAP full point source mask
	hpmask = hp.read_map(opts['FNAME_WMAP_PTSRC_MASK'])

	# Loop through sources, getting their pixel indices and unmasking them on 
	# the map. query_disc() is set to be inclusive, which means that it will 
	# unmask any pixels that overlap even a tiny bit with the disc radius. This 
	# is good for handling single pixels left masked due to slight pointing 
	# errors (due to coord. conversions etc.), but means that overlapping 
	# masked regions may be liberally unmasked.
	for i in range(new_l.size):
		vec = hp.pixelfunc.ang2vec(theta=new_b[i], phi=new_l[i])
		idxs = hp.query_disc( opts['NSIDE'], vec, 
							  opts['PS_MASK_RAD']*opts['PS_MASK_RAD_PADDING'],
							  inclusive=True )
		hpmask[idxs] = 1.0 # Unmask pixels (0=masked, 1=unmasked)
	
	# Output mask map
	hp.write_map(opts['FNAME_MASK'], hpmask)
	return hpmask
	
