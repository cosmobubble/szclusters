#!/usr/bin/python
"""kSZ temperature profile, as a function of radius of the cluster."""

import numpy as np
import pylab as P
from arnaud_profile import ArnaudProfile
from load_rexcess import load_rexcess_catalogue

fname = "Tx_Pratt_Fig_2007_TempProf.dat"
r_T, T = np.genfromtxt(fname, delimiter="  ").T
p = load_rexcess_catalogue()


def ne(r, CL):
	x = r*(CL.c500 / CL.r500)
	#return x**(-1.) * (1. + x)**(-1.5*CL.gamma)
	##return x**(-CL.gamma) * (1. + x**(CL.alpha))**(-2.)
	#return x**(-CL.gamma) * (1. + x**(CL.alpha))**(-(CL.beta - CL.gamma)/CL.alpha) * np.exp(0.12*CL.beta*x)
	return (1. + x**(CL.alpha))**(-(CL.beta - CL.gamma)/CL.alpha) * np.exp(0.12*CL.beta*x)
	#return (1. + x**2.)**(-1.5*1.0)

def beta(r, CL):
	x = r*(CL.c500 / CL.r500)
	return (1. + x**2.)**(-1.5*1.0)

def broken(x, a, b):
	return x**(-a) * (1. + x)**(-b)
	
def TT(r, CL):
	x = r*(CL.c500 / CL.r500)
	return (1.19 - 0.74*1.7*x)/1.19

x = np.linspace(1e-2, 50., 1e3)

"""
_tt = []
for _p in p:
	C = ArnaudProfile(_p)
	pp = C.P(x*C.r500)
	nn = ne(x*C.r500, C)
	tt = pp / nn
	_tt.append(tt)
"""

"""
P.subplot(111)
for tt in _tt:
	P.plot(x, tt/tt[0], alpha=0.5)
P.plot(r_T*2., T, 'k-', lw=2.5)
P.plot(r_T*2., (1.19 - 0.74*2.*r_T)/1.19, 'r-', lw=2.5)
P.yscale('log')
P.xscale('log')
P.ylim((0.05, 5.))
"""


P.subplot(111)
for _p in p:
	C = ArnaudProfile(_p)
	P.plot(x, ne(x*C.r500, C), 'r-', alpha=0.5)
	P.plot(x, beta(x*C.r500, C), 'b-', alpha=0.5)
P.yscale('log')
P.xscale('log')
#P.ylim((0.05, 5.))

P.show()
