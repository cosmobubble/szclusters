#!/usr/bin/python
"""Plot Table 6 r200 vs r500 from Locuss paper."""

import numpy as np
import pylab as P
import scipy.optimize

# Load data
M200, eM200, M500, eM500 = np.genfromtxt("data/locuss-table6-MT200-MT500.dat", delimiter="\t").T


def leastsq(p, x, y, xerr, yerr):
	#c = p[0]
	c = 0
	m = p[0]
	# sigma_i^2 = sigma_y^2 + m^2 sigma_x^2
	err = np.sqrt(yerr**2. + (m*xerr)**2.)
	return (m*x + c - y) / err

# Fit straight line
#p0 = [0.0, 1.0] # c, m
p0 = [1.0]
p = scipy.optimize.leastsq(leastsq, p0, args=(M500, M200, eM500, eM200))[0]
print p


P.subplot(111)
P.errorbar(M500, M200, xerr=eM500, yerr=eM200, fmt="b.", alpha=0.5)
x = np.linspace(0.85*np.min(M500), 1.1*np.max(M500), 200)
#P.plot(x, p[0] + p[1]*x, 'r-')
P.plot(x, p[0]*x, 'r-')
P.xlabel("M_500 [h_100^-1 Mpc]")
P.ylabel("M_200 [h_100^-1 Mpc]")
P.show()
