#!/usr/bin/python
"""Find fitting function for the correlation function."""

import numpy as np
import pylab as P
import scipy.integrate
import scipy.optimize
import cosmolopy as cp


# Define default cosmology
cosmo = {
 'omega_M_0' : 0.27,
 'omega_lambda_0' : 0.73,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'N_nu' : 0,
 'h' : 0.7,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': False
}

def corrfn_fit(p, r, Er):
	"""Do least-squares fit to correlation function."""
	A, r0, gamma, w = p
	return A*(r/r0)**(-gamma) * np.exp(-r/w) - Er*r**2.

def corrfn(r, A, r0, gamma, w):
	"""Fitting function for the correlation function * r^2."""
	return A*(r/r0)**(-gamma) * np.exp(-r/w)

# Calculate correlation function
r = np.linspace(1., 200., 500)
k = np.logspace(-4, 2., 5e4)
pk = cp.perturbation.power_spectrum(k, z=0., **cosmo)
Er = np.array( [scipy.integrate.simps(k*np.sin(k*rr)*pk, k) for rr in r] )
Er *= 1. / (2.*np.pi**2. * r)

# Do least-squares fit
p0 = [1.0, 5.06, 1.862, 1.0] # A, r0, gamma, w
p, stat = scipy.optimize.leastsq(corrfn_fit, p0, args=(r, Er))
print "A, r0, gamma, w =", p

# Plot results
P.subplot(311)
P.axhline(0.0, ls="dashed", color="k")
P.plot(r, Er, 'r-')
P.plot(r, corrfn(r, p[0], p[1], p[2], p[3])/r**2., 'b--')
P.xlabel("r [Mpc]")
P.ylabel(r"$\xi(r)$")

P.subplot(312)
P.plot(r, Er*r**2., 'r-')
P.plot(r, corrfn(r, p[0], p[1], p[2], p[3]), 'b--')
P.xlabel("r [Mpc]")
P.ylabel(r"$r^2 \xi(r)$")

P.subplot(313)
P.axhline(0.0, ls="dashed", color="k")
P.plot(r, (corrfn(r, p[0], p[1], p[2], p[3]) - Er*r**2.) / (Er*r**2.), 'r-')
P.xlabel("r [Mpc]")
P.ylabel(r"$\Delta\xi(r)$")
P.ylim((-1.01, 1.01))

P.show()
