#!/usr/bin/python
"""f_nm(k) statistic, from Macaulay et al."""

import numpy as np
import pylab as P
import scipy.special
import scipy.integrate
import scipy.optimize
import scipy.interpolate
import cosmolopy as cp
from units import *
import copy
import time
ts = time.time()

# Power spectrum resolution parameters
KMIN = -4. # in log space
KMAX = 2.
KSTEPS = 1e4

# Define default cosmology
DEFAULT_COSMO = {
 'omega_M_0' : 0.27,
 'omega_lambda_0' : 0.73,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'N_nu' : 0,
 'h' : 0.7,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': False,
 'gamma': 0.55
}


def rho_m(z=0.0, cosmo=DEFAULT_COSMO):
	"""Background matter density at a given redshift."""
	return (1.+z)**3. * 3.*(cosmo['h']*fH0)**2. * cosmo['omega_M_0'] / (8.*np.pi*G)

def correlation_fn(r, z=0.0, cosmo=DEFAULT_COSMO):
	"""Matter correlation function, xi(r)."""
	k = np.logspace(KMIN, KMAX, KSTEPS*2)
	Pk = cp.perturbation.power_spectrum(k, z, **cosmo)
	integ = k**2. * Pk * np.sinc(k*r/np.pi)
	corr = scipy.integrate.simps(integ, k) / (2.*np.pi**2.)
	return corr

def correlation_fn_fastarray(r, z=0.0, cosmo=DEFAULT_COSMO):
	"""Calculate the matter correlation function efficiently for a 
	   range of r."""
	k = np.logspace(KMIN, KMAX, KSTEPS*2)
	Pk = cp.perturbation.power_spectrum(k, z, **cosmo)
	ff = k**2. * Pk / (2.*np.pi**2.)
	corr = [scipy.integrate.simps(ff*np.sinc(k*rr/np.pi), k) for rr in r]
	return np.array(corr)

def dn_dlogM_hu(M, z, cosmo):
	"""Get halo number counts as a function of mass scale and redshift; 
	   see Hu Eq. 1 (astro-ph/0301416) for fitting fn. Uses a simple 
	   finite difference scheme to find a derivative."""
	rho = rho_m(z, cosmo)
	
	# Get masses for finite differencing
	dlnM = 1e-2 * np.log(M)
	M2 = np.exp( np.log(M) + dlnM )
	
	# Get variance of density field
	R1 = cp.perturbation.mass_to_radius(M, **cosmo) / (1.+z)
	R2 = cp.perturbation.mass_to_radius(M2, **cosmo) / (1.+z)
	sm, err = cp.perturbation.sigma_r(R1, z, **cosmo)
	sm2, err = cp.perturbation.sigma_r(R2, z, **cosmo)

	# Calculate derivative dln(sigma^-1)/dlnM
	dd = -1. * ( np.log(sm2) - np.log(sm) ) / dlnM
	dn = (0.3*rho / M) * dd * np.exp(-np.abs(0.64 - np.log(sm))**3.82)
	return dn


def n_generic(A, a, p, dc, M, z, cosmo):
	"""Generic halo mass function in the Sheth-Tormen form of 
	   Sheth & Diaferio Eq. 2, arXiv:astro-ph/0009166 (2001).
	   N.B. In the literature, 'dn/dM' is sometimes written 
	   just as 'n(M)', which is confusing."""
	
	# Get masses for finite differencing
	dlnM = 1e-2 * np.log(M)
	M2 = np.exp( np.log(M) + dlnM )
	
	# Get variance of density field
	R1 = cp.perturbation.mass_to_radius(M, **cosmo) / (1.+z)
	R2 = cp.perturbation.mass_to_radius(M2, **cosmo) / (1.+z)
	sm, err = cp.perturbation.sigma_r(R1, z, **cosmo)
	sm2, err = cp.perturbation.sigma_r(R2, z, **cosmo)
	
	# Other parameters
	nu = dc / sm
	nu2 = dc / sm2
	rho = rho_m(z, cosmo)
	
	# Finite difference derivative from Eq. 2
	# d log(nu^2) / d log M = -2 delta_c nu . d(sigma(M)) / d log M
	dlognu2 = -2.*dc*nu * (sm2 - sm) / dlnM
	
	# Function
	nn = (A*rho/M**2.) * (1. + 1./(a*nu**2.)**p)
	nn *= np.sqrt(a*nu**2. / (2.*np.pi))
	nn *= np.exp(-0.5*a*nu**2.) * dlognu2
	return nn

def n_sheth_tormen(M, z, cosmo=DEFAULT_COSMO):
	"""Sheth-Tormen halo mass function (see Sheth & Diaferio Eq. 2, 
	   arXiv:astro-ph/0009166 (2001), or Sheth & Tormen Eqs. 9, 10, 
	   arXiv:astro-ph/9901122 (1999)."""
	A = 0.322; a = 0.707; p = 0.3; dc = 1.686
	return n_generic(A, a, p, dc, M, z, cosmo)

def n_press_schechter(M, z, cosmo=DEFAULT_COSMO):
	"""Press-Schechter halo mass function (see Sheth & Diaferio Eq. 2, 
	   arXiv:astro-ph/0009166 (2001)."""
	A = 0.5; a = 1.0; p = 0.0; dc = 1.686
	return n_generic(A, a, p, dc, M, z, cosmo)

def omegaM(z, cosmo):
	"""Evolving matter fraction, Omega_M(a), from Linder (nr. Eq. 5),
	   arXiv:astro-ph/0507263 (2005)."""
	a = 1. / (1. + z)
	return cosmo['omega_M_0'] * a**(-3.) / cp.distance.e_z(z, **cosmo)**2.

def fg(z, cosmo=DEFAULT_COSMO):
	"""Linear growth rate, parameterised as in Linder."""
	if 'gamma' in cosmo.keys():
		gamma = cosmo['gamma']
	return omegaM(z, cosmo)**gamma

def f_nm(k, r1, r2, alpha):
	"""Correlated window function for peculiar velocity pairs, from 
	   Ma et al. (arXiv:1010.4276) Eq. A11."""
	A = np.sqrt(r1**2. + r2**2. - 2.*r1*r2*np.cos(alpha))
	j0 = scipy.special.jn(0, k*A)
	j2 = scipy.special.jn(2, k*A)
	fnm = (np.cos(alpha)/3.)*(j0 - 2.*j2) + j2*r1*r2*(np.sin(alpha) / A)**2.
	return fnm

def vv_nm(r1, r2, alpha, cosmo):
	"""Mean line-of-sight velocity correlation for a given pair of 
	   points, from Ma et al. (arXiv:1010.4276) Eq. 2.
	   Defined at z=0. May be negative (i.e. off-diagonal component of 
	   covariance matrix)."""
	k = np.logspace(KMIN, KMAX, KSTEPS*2) # Need higher res., oscillatory
	Pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
	fnm = f_nm(k, r1, r2, alpha)
	I = scipy.integrate.simps(Pk*fnm, k)
	return I * cosmo['omega_M_0']**(1.1) * (100.*cosmo['h'])**2. / (2.*np.pi**2.)

def b_m(M, z=0.0, cosmo=DEFAULT_COSMO, Pk=None):
	"""Bias b(M), from Eq. 4 of Bhattacharya & Kosowsky (2007), 
	   http://iopscience.iop.org/1538-4357/659/2/L83, based on 
	   Sheth et al. (2001)."""
	dcrit = 1.686
	k = np.logspace(KMIN, KMAX, KSTEPS)
	sig0sq = sigma2_j(0., M, k, z, cosmo, Pk)
	D = cp.perturbation.fgrowth(z, cosmo['omega_M_0'])
	return 1. + ( (dcrit**2. - sig0sq) / (sig0sq*dcrit*D) )

def b_halo(z, cosmo=DEFAULT_COSMO):
	"""(FIXME) b_halo, defined near Eq. 8 of Bhattacharya & Kosowsky 
	   (2007). This looks like a divergent integral, so setting to 
	   constant for now."""
	"""
	# FIXME: No sensible choice for M_MIN - the integral appears to be divergent!
	M_MIN = 0.
	M_MAX = 19.
	M_SAMP = 500
	M = np.logspace(M_MIN, M_MAX, M_SAMP)
	nn = n_sheth_tormen(M, z, cosmo=DEFAULT_COSMO)
	I1 = scipy.integrate.simps(M*b(M)*nn, M)
	I2 = scipy.integrate.simps(nn, M)
	return I1 / I2
	"""
	return 1.

def sigma2_j(j, M, k=None, z=0.0, cosmo=DEFAULT_COSMO, Pk=None):
	"""j'th moment, sigma_j^2, of the filtered power spectrum, from 
	   Eq. 3 of Bhattacharya & Kosowsky 2007."""
	if k==None:  k = np.logspace(KMIN, KMAX, KSTEPS)
	if Pk==None: Pk = cp.perturbation.power_spectrum(k, z, **cosmo)
	R = cp.perturbation.mass_to_radius(M, **cosmo) / (1.+z)
	W = cp.perturbation.w_tophat(k, R)
	integ = k**(2. + 2.*j) * Pk * W**2.
	I = scipy.integrate.simps(integ, k)
	return (1./(2.*np.pi**2.)) * I

def sigma_vlocal(M, z, delta, cosmo=DEFAULT_COSMO):
	"""Velocity dispersion in a locally overdense region, from Eq. 7 of 
	   Bhattacharya & Kosowsky (2007). Note that this is *not* 
	   sigma^2, it's just sigma^1."""
	k = np.logspace(KMIN, KMAX, KSTEPS)
	
	# Peculiar density factor
	Rlocal = 10. # FIXME: What should this actually be!?
	Mlocal = cp.perturbation.radius_to_mass(Rlocal, **cosmo)*(1.+z)**3.
	M10 = cp.perturbation.radius_to_mass(10./cosmo['h'], **cosmo)*(1.+z)**3.
	mu = 0.6 * sigma2_j(0., Mlocal, k, z, cosmo) / sigma2_j(0., M10, k, z, cosmo)
	fac = (1. + delta)**(2.*mu)
	
	# Background evolution
	a = 1./(1.+z)
	H = 100. * cosmo['h'] * cp.distance.e_z(z, **cosmo)
	D = cp.perturbation.fgrowth(z, cosmo['omega_M_0'])
	f = fg(z, cosmo)
	
	# Moments of filtered power spectrum
	sig0sq = sigma2_j(0., M, k, z, cosmo)
	sig1sq = sigma2_j(1., M, k, z, cosmo)
	sigm1sq = sigma2_j(-1., M, k, z, cosmo)
	
	# FIXME: Check this w.r.t. linear growth rate factor
	sig = np.sqrt( 1. - ( (sig0sq**2.) / (sig1sq*sigm1sq) ) ) * np.sqrt(sigm1sq)
	return a * H * D * f * sig * fac

def pv_m(v, M, z, delta, cosmo=DEFAULT_COSMO):
	"""p(v|m,delta), given in Eq. 6 of Bhattacharya & Kosowsky (2007), 
	   taken from Sheth & Diaferio 2001."""
	sigv = sigma_vlocal(M, z, delta, cosmo)
	# FIXME: Corrected factor of 2 in Eq. 6 to be in the exponential.
	return np.exp(-0.5 * (3.*v/sigv)**2.) / np.sqrt(2.*np.pi*(sigv**2.)/3.)

def v12(r, z, gam, cosmo=DEFAULT_COSMO):
	"""Pairwise streaming velocity statistic, from Eq. 11 of 
	   Juszkiewicz et al., arXiv:astro-ph/9812387 (1998), but with 
	   added bias factors from Bhattacharya & Kosowsky (2007). 
	   (B&H only quote the linear part of v12(r), and miss out the 
	   non-linear correction term of Juszkiewicz.)"""
	
	# Cosmology factors
	H = 100.*cosmo['h']*cp.distance.e_z(z, **cosmo)
	f = fg(z, cosmo)
	bhalo = b_halo(z, cosmo)
	
	# Slope of correlation function, defined in Juszkiewicz Eq. 8,9
	# This is a fitting form. Must have 0 < gam < 3 (gam=='gamma' in paper)
	if gam < 0. or gam > 3.: print "WARNING: v12(r) gamma must be in range 0 < gamma < 3."
	alpha = 1.2 - 0.65 * gam
	
	# Correlation function and its integral
	rr = np.linspace(1e-3, r, 200)
	xi_r = correlation_fn_fastarray(rr, z, cosmo)
	xi = xi_r[-1]
	xibar = scipy.integrate.simps(xi_r*rr**2., rr)
	xibb = 3. * bhalo * xibar / ( r**3. * (1. + bhalo**2. * xi) )
	
	return (-2./3.) * H * r * f * xibb*(1. + alpha*xibb)


def v12_fastarray(r, z, gam, cosmo=DEFAULT_COSMO):
	"""Efficiently calculate v12(r) statistic for a range of r."""
	
	# Cosmology factors
	H = 100.*cosmo['h']*cp.distance.e_z(z, **cosmo)
	f = fg(z, cosmo)
	bhalo = b_halo(z, cosmo)
	
	# Slope of correlation function, defined in Juszkiewicz Eq. 8,9
	# This is a fitting form. Must have 0 < gam < 3 (gam=='gamma' in paper)
	if gam < 0. or gam > 3.: print "WARNING: v12(r) gamma must be in range 0 < gamma < 3."
	alpha = 1.2 - 0.65*gam
	
	# Calculate corr. fn. for fine-grained range of r (for integration)
	dr = 2.0
	rsamp = np.linspace(1e-3, np.max(r), int(np.max(r)/dr))
	xi_r = correlation_fn_fastarray(rsamp, z, cosmo)
	xi_interp = scipy.interpolate.interp1d(rsamp, xi_r)
	xi = xi_interp(r)
	
	# Cumulative integration of xi*r^2, with interpolation of result
	I = scipy.integrate.cumtrapz(xi_r*rsamp**2., rsamp) # initial=0.0)
	I = np.concatenate(([0.],I))
	xibar = scipy.interpolate.interp1d(rsamp, I)
	xibb = 3. * bhalo * xibar(r) / ( r**3. * (1. + bhalo**2. * xi) )
	
	return (-2./3.) * H * r * f * xibb*(1. + alpha*xibb)

def sigma12(r, z, M1, M2, cosmo=DEFAULT_COSMO):
	"""Dispersion of pairwise velocity statistic quoted in Eq. 10 of 
	   Bhattacharya & Kosowsky (2007), which was originally taken from 
	   Sheth et al. arXiv:astro-ph/0009167 (2000) Eqs. 20--26 (see for 
	   useful discussion). This statistic must be evaluated over a 
	   given bin in mass."""
	xi = correlation_fn(r, z, cosmo)
	bhalo = b_halo(z, cosmo)
	
	# Get variables at either end of mass bin (crude integral approx.)
	s1 = sigma_vlocal(M1, z, delta=0., cosmo=cosmo)**2.
	s2 = sigma_vlocal(M2, z, delta=0., cosmo=cosmo)**2.
	b1 = b_m(M1, z, cosmo)
	b2 = b_m(M2, z, cosmo)
	n1 = n_sheth_tormen(M1, z, cosmo)
	n2 = n_sheth_tormen(M2, z, cosmo)
	
	# Get (crude) arithmetic average
	f1 = (s1*n1 + s2*n2) / (n1 + n2)
	f2 = (b1*s1*n1 + b2*s2*n2) / (n1 + n2)
	
	return 2. * (f1 + xi*f2) / (1. + bhalo**2. * xi)

# Define variant cosmology, for comparison
cosmo = DEFAULT_COSMO
cosmo2 = copy.copy(DEFAULT_COSMO)
#cosmo2['omega_M_0'] = 0.32
#cosmo2['omega_lambda_0'] = 0.68
#cosmo2['h'] = 0.75
#cosmo2['n'] = 1.0
#cosmo2['sigma_8'] = 0.9
#cosmo2['gamma'] = 0.6

r = np.linspace(1e-3, 200., 20)
vv = v12_fastarray(r, z=0.0, gam=1., cosmo=cosmo)

s12 = [np.sqrt(sigma12(rr, z=0., M1=1e11, M2=1e14, cosmo=cosmo)) for rr in r]

P.subplot(111)
P.errorbar(r, vv, yerr=s12)
#P.xscale('log')
#P.yscale('log')
#P.legend(loc='lower left')
print "Run took", round(time.time() - ts, 1), "sec."
P.show()


"""
# Sheth bias b(M)
k = np.logspace(KMIN, KMAX, KSTEPS)
Pk = cp.perturbation.power_spectrum(k, z=0.0, **cosmo)
bb = [b_m(m, z=0.0, cosmo=DEFAULT_COSMO, Pk=Pk) for m in M]
"""
"""
# Integral P(k) f_nm(k)
r1 = [5., 10., 50.] #np.logspace(0., 3., 4)
r2 = 10.
alpha = np.linspace(0.001, 2.*np.pi, 100)
vv = np.array( [[vv_nm(rr, r2, a, cosmo) for a in alpha] for rr in r1] )
vv2 = np.array( [[vv_nm(rr, r2, a, cosmo2) for a in alpha] for rr in r1] )

P.subplot(111)
#cols = ['k', 'g', 'b', 'c', 'y', 'm', 'r']
cols = ['r', 'k', 'b']
P.title("r2 = 10 Mpc, gamma=0.55 (blue) / 0.6 (red)")
for i in range(len(vv)):
	P.plot(alpha*180./np.pi, np.sqrt(vv[i]), color="b", alpha=(1.0-i*0.3), label="r1="+str(int(r1[i])))
	P.plot(alpha*180./np.pi, np.sqrt(-vv[i]), color="b", alpha=(1.0-i*0.3), ls='dashed')

for i in range(len(vv2)):
	P.plot(alpha*180./np.pi, np.sqrt(vv2[i]), alpha=(1.0-i*0.3), color="r")
	P.plot(alpha*180./np.pi, np.sqrt(-vv2[i]), alpha=(1.0-i*0.3), color="r", ls='dashed')
	
P.legend(loc='upper center', prop={'size':'x-small'})
P.xlabel("alpha [deg]")
P.ylabel("sqrt <v_m . v_n> (LOS) [km/s]")
print "Run took", round(time.time() - ts, 1), "sec."
P.show()
"""

"""
k = np.logspace(KMIN, KMAX, KSTEPS)
#r1 = 100.
r1 = np.logspace(-1, 3., 5)
r2 = 10.
alpha = np.pi
#alpha = np.linspace(0., np.pi, 5)
ff = [f_nm(k, rr, r2, alpha) for rr in r1]

P.subplot(111)
P.title("alpha=180deg, r2=10 Mpc")
for i in range(len(ff)):
	P.plot(k, ff[i], label="r1="+str(round(r1[i], 0))+" Mpc")

P.xscale('log')
P.xlabel("k (Mpc^-1)")
P.ylabel("f_nm(k)")
P.legend(loc='upper right', prop={'size':'small'})
P.show()
"""

"""
### Plotting b(M)
M = np.logspace(12.5, 16.5, 30)
jj = 2
b  = [b_m(m, cosmo=cosmo) for m in M]
b2  = [b_m(m, cosmo=cosmo2) for m in M]

P.subplot(111)
P.plot(M, b,  label="gamma=0.55")
P.plot(M, b2, label="gamma=0.60")
P.legend(loc='upper right', prop={'size':'small'})
P.xscale('log')
P.xlabel("log(M) [Msun]")
P.ylabel("Bias b(M)")

P.show()
"""

"""
### Plotting f(v|M,delta) as a function of (v,M) [contour plots and imshow]
v = np.linspace(0., 500., 30)
M = np.logspace(12.5, 16.5, 30)
pp = [pv_m(v, m, z=0., delta=0., cosmo=cosmo) for m in M]
pp = np.array(pp)
pp2 = [pv_m(v, m, z=0., delta=0., cosmo=cosmo2) for m in M]
pp2 = np.array(pp2)

lvls = np.linspace(np.min((pp, pp2)), np.max((pp, pp2)), 10)

P.subplot(121)
P.suptitle("p(v|m,delta=1)")

#ext = (np.min(np.log10(M)), np.max(np.log10(M)), np.min(v), np.max(v))
#im = P.imshow(pp.T, extent=ext, origin='lower', aspect=0.006)
#im.set_cmap('hot')

P.contourf(np.log10(M), v, pp.T, levels=lvls)
P.title("h=0.7")
P.xlabel("log(M)")
P.ylabel("v [km/s]")

P.subplot(122)
P.title("h=0.75")
P.contourf(np.log10(M), v, pp2.T, levels=lvls)
P.xlabel("log(M)")
#P.ylabel("v [km/s]")

# sigma_v(M)
#sig = [sigma_vlocal(m, z=0.0, delta=0.) for m in M]
#sig2 = [sigma_vlocal(m, z=0.0, delta=0., cosmo=cosmo2) for m in M]
"""
