#!/usr/bin/python
"""Plot correlation function for Bertacca's Unified Dark Matter model, as set 
   out in Raccanelli et al. (2012), arXiv:1207.0500."""

import numpy as np
import pylab as P
import scipy.integrate
import scipy.special
import cosmolopy as cp

# Define default cosmology
cosmo = {
 'omega_M_0' : 0.27,
 'omega_lambda_0' : 0.73,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'N_nu' : 0,
 'h' : 0.7,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': True
}

# UDM model parameters
cinf = 1e-2 # c_infinity, asymptotic sound speed

def cs(a):
	"""Sound speed, from Eq. 14."""
	# cs->0 as a->0
	Ol = cosmo['omega_lambda_0']
	Om = 1. - Ol
	cs2 = Ol * cinf**2. / (Ol + (1. - cinf**2.)*Om*a**(-3.) )
	return np.sqrt(cs2)

def csa(a):
	"""Sound speed, from Eq. 14, divided by 'a' (integrand in Eq. 13)."""
	# (cs/a)->0 as a->0
	Ol = cosmo['omega_lambda_0']
	Om = 1. - Ol
	cs2 = Ol * cinf**2. / (a**2. * Ol + (1. - cinf**2.)*Om*a**(-1.) )
	return np.sqrt(cs2)

def A(a):
	"""Argument of transfer function, defined in Eq. 13."""
	return scipy.integrate.quad(csa, 1e-5, a)[0]

def T_UDM(k, a):
	"""UDM transfer function, from Eq. 12."""
	AA = A(a)
	return scipy.special.jn(0, AA*k)


def gamma(k):
	"""Estimate of gamma, from Eq. 15, at a=1"""
	a1 = 1.; a2 = 1.01
	T1 = T_UDM(k, a1); T2 = T_UDM(k, a2)
	Om = cosmo['omega_M_0']
	fLCDM = Om**0.55
	dlnT = np.log(T2) - np.log(T1)
	dlna = np.log(a2) - np.log(a1)
	return np.log(dlnT/dlna + fLCDM) / np.log(Om)

# Calculate correlation function
r = np.linspace(10., 250., 250)
k = np.logspace(-4, 3., 1e4)
pk = cp.perturbation.power_spectrum(k, z=0., **cosmo)
TT = T_UDM(k, a=0.7)
# FIXME: Oscillatory integral, should integrate in log-space maybe?
corrUDM = np.array( [scipy.integrate.simps(k*np.sin(k*rr)*pk*TT**2., k) for rr in r] )
corrUDM *= 1. / (2.*np.pi**2. * r)

corrLCDM = np.array( [scipy.integrate.simps(k*np.sin(k*rr)*pk, k) for rr in r] )
corrLCDM *= 1. / (2.*np.pi**2. * r)

# Plot results
P.subplot(211)
P.plot(r, corrUDM, 'r-')
P.plot(r, corrLCDM, 'k-')
P.yscale('log')

P.subplot(212)
P.plot(k, gamma(k), 'r-')
P.axhline(0.55, ls="solid", color="k")
P.xscale('log')

"""
P.subplot(212)
P.plot(k, k*np.sin(k*10.)*pk*TT**2., 'r-')
P.plot(k, k*np.sin(k*10.)*pk, 'k-')
P.yscale('log')
P.xscale('log')
"""

P.show()
