#!/usr/bin/python
"""Plot v_12 statistic."""

import numpy as np
import pylab as P
import scipy.integrate
import scipy.optimize
import cosmolopy as cp


# Define default cosmology
cosmo = {
 'omega_M_0' : 0.27,
 'omega_lambda_0' : 0.73,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'N_nu' : 0,
 'h' : 0.7,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': False
}

def Er(r):
	"""Fitting function for the correlation function, for LCDM
	   (see fitting_fn_corrfn.py)."""
	A, r0, gamma, w = [24.79, 2.358, -0.7979, 36.16]
	return A*(r/r0)**(-gamma) * np.exp(-r/w) / r**2.

def Er_r2(r):
	"""Fitting function for the correlation function * r^2, for LCDM."""
	A, r0, gamma, w = [24.79, 2.358, -0.7979, 36.16]
	return A*(r/r0)**(-gamma) * np.exp(-r/w)

def integ_Ebar(f, r):
	"""Integrand of Eq. 3 in Ferreira et al."""
	return Er_r2(r)

def v12(r, fgamma=0.55, cgamma=1.75):
	"""Calculate v12(r). fgamma is the exponent of Omega_m in the linear 
	   growth rate, and cgamma is the (negative) exponent of the correlation 
	   function."""
	
	# Find Ebar, Eq. 3 in Ferreira et al.
	y = scipy.integrate.odeint(integ_Ebar, 0.0, r).T[0]
	Ebar = (3. / r**3.) * y
	Ebbar = Ebar / (1. + Er(r))

	# Calculate v_12(r), Eq. 2 in Ferreira.
	Om = cosmo['omega_M_0']
	H0 = cosmo['h'] * 100. # km/s/Mpc
	f = Om**fgamma # Linear growth rate for LCDM
	alpha = 1.2 - 0.65*cgamma # Depends on log slope of corr. fn. (see Eq. 5)
	v12 = -(2./3.) * H0 * f * r * Ebbar * (1. + alpha*Ebbar)
	return v12

# Calculate correlation function
r = np.linspace(0.1, 200., 500)
k = np.logspace(-4, 2., 5e4)
"""
pk = cp.perturbation.power_spectrum(k, z=0., **cosmo)
#Er = np.array([scipy.integrate.simps(k*np.sin(k*rr)*pk, k) for rr in r] )/(2.*np.pi**2. * r)
"""



# Plot results
P.subplot(211)
P.axhline(0.0, ls="dashed", color="k")
P.plot(r, v12(r), 'k-')
P.plot(r, v12(r, fgamma=0.6), 'r-')
P.plot(r, v12(r, cgamma=1.2), 'b-')
P.xlabel("r [Mpc]")
P.ylabel("$v_{12}(r)$")

P.subplot(212)
P.axhline(0.0, ls="dashed", color="k")
P.plot(r, np.log(Er(r)), 'k-')
P.xlabel("r [Mpc]")
P.ylabel(r"$log \xi(r)$")

P.show()
