#!/usr/bin/python

import numpy as np
import cosmolopy as cp
import pylab as P
from scipy.special import jn
import scipy.integrate


# Define cosmology
DEFAULT_COSMO = {
    'omega_M_0': 0.27, 
    'omega_lambda_0': 0.73,
    'h': 0.70,
    'omega_b_0': 0.045, 
    'omega_n_0': 0.0,
    'N_nu': 0,
    'n': 1.0,
    'sigma_8': 0.8,
    'baryonic_effects': True
}
cosmo = DEFAULT_COSMO

KMIN = -5.
KMAX = 4.
KSTEPS = 5e4

def fnm(k, r1, r2, alpha):
    """Correlated window function f_nm(k) for a separated pair of objects.
    See Ma, Gordon and Feldman, arXiv:1010:4276, Eq. A11."""
    cosa = np.cos(alpha)
    A = np.sqrt( r1**2. + r2**2. - 2.*r1*r2*cosa )
    j0 = jn(0, k*A)
    j2 = jn(2, k*A)
    fnm = (1./3.)*cosa*(j0 - 2.*j2) + j2*r1*r2*(np.sin(alpha) / A)**2.
    return fnm


def sigma_vrms(M, z=0.0, cosmo=DEFAULT_COSMO):
    """RMS peculiar velocity, sigma_v, from Watkins arXiv:astro-ph/9710170 (1997) Eq. 1."""
    k = np.logspace(KMIN, KMAX, KSTEPS)
    Pk = cp.perturbation.power_spectrum(k, z, **cosmo)

    R = cp.perturbation.mass_to_radius(M, **cosmo) / (1.+z)
    W = cp.perturbation.w_tophat(k, R)
    
    integ = Pk * W**2.
    I = scipy.integrate.simps(integ, k)
    return I * (100.*cosmo['h'])**2. * cosmo['omega_M_0']**1.2 / (6. * np.pi**2.)
    

def sigma_rho(M, z=0.0, cosmo=DEFAULT_COSMO):
    """RMS of density field at given mass scale."""
    R = cp.perturbation.mass_to_radius(M, **cosmo) / (1.+z)
    return cp.perturbation.sigma_r(R, z, **cosmo)[0] # [1] is the error on this value



r1 = 100.
r2 = 100.
alpha = np.pi/4.

k1 = 2.*np.pi/r1
k2 = 2.*np.pi/r2

"""
k = np.logspace(-4., 1.5, 1e4)
f = fnm(k, r1, r2, alpha)
#fint = scipy.integrate.cumtrapz(f, k, initial=0.0) # 'initial' keyword in later version of SciPy?
fint = np.concatenate( ([0.0], scipy.integrate.cumtrapz(f, k)) )

P.subplot(211)
P.plot(k, f, 'b-')
P.axvline(k1, ls="dashed", color="r")
P.axvline(k2, ls="dashed", color="r")
P.axhline(0.0, ls="dotted", color='k')
P.xscale('log')

P.subplot(212)
P.plot(k, fint, 'g-')
P.axhline(0.0, ls="dotted", color='k')
P.xscale('log')
"""

M = np.logspace(10.2, 23., 20)
R = [cp.perturbation.mass_to_radius(m, **cosmo) for m in M]
sig = [np.sqrt( sigma_vrms(m, z=0., cosmo=cosmo) ) for m in M]
sig_dens = [np.sqrt( sigma_rho(m, z=0., cosmo=cosmo) ) for m in M]


P.subplot(211)
P.plot(R, sig, 'r-')
#P.plot(R, sig_dens, 'b-')
P.xscale('log')
#P.xlabel('Mass [Msun]')
P.xlabel('Scale radius R [Mpc]')
P.ylabel('$\sigma_v(M)$ [km/s]')

P.subplot(212)
P.plot(R, sig_dens, 'b-')
P.xscale('log')
P.show()
