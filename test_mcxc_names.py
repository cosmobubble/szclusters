#!/usr/bin/python
"""Experiment with using names in MCXC to identify duplicates."""

import numpy as np
import difflib
import time
tstart = time.time()



################################################################################
# Load data
################################################################################

fname = "data/MCXC_processed.dat"
fname2 = "data/MCXC_names.txt"
fout = "data/MCXC_gal_trimmed_dup.dat"
fout2 = "data/MCXC_gal_trimmed_dup_th500.dat"
FNAME_PRECOMPUTED = "data/precomputed_angular_distances" # .npy appended on save

ids, z, l, b, da, th500, r500, m500, l500, scale = np.genfromtxt(fname, delimiter="\t").T
n1, n2, n3 = np.genfromtxt(fname2, dtype="a", delimiter="\t", usecols=(1,2,3)).T


################################################################################
# Print names of all catalogue entries
################################################################################

# Combine all names for each cluster into a list
names = []
for i in range(n2.size):
	# Ignore column n1, since they are unique names in this catalogue
	nn = [n2[i].replace(" ", ""), n3[i].replace(" ", "")]
	nn = [item for item in nn if item != ''] # Weed-out blanks
	names.append(nn) # Add to names list


################################################################################
# Do string similarity comparison
################################################################################

# Find distance between strings
def str_distance(a, b):
	seq = difflib.SequenceMatcher(a=a.lower(), b=b.lower())
	return seq.ratio()


# For each cluster, check if any other clusters have similar names
name = names[0][0]
for i in range(10):
	print "\t", names[i][0], "\t", round(  str_distance(name, names[i][0]),  3)

print "\nNAME:", name, "\n"
