#!/usr/bin/python
"""Test healpy."""

import sys
sys.path.append("/home/phil/postgrad/lib")

import numpy as np
import healpy as hp
import pylab as P

# Load a sky map
#clmap = hp.read_map('cludata/bareterm_a100_Q1_clustermap.fits')
#dmap = hp.read_map('cludata/bareterm_a100_Q1_dmap.fits')
#wmap_v = hp.read_map('data/wmap_band_imap_r9_7yr_V_v4.fits')
wmap_fgsub_v = hp.read_map('data/wmap_band_forered_iqumap_r9_7yr_V_v4.fits')
wmap_ptsrc_mask = hp.read_map('data/wmap_point_source_catalog_mask_r9_7yr_v4.fits')

# Mask some pixels
th = np.pi/2.; phi = 0.
vec = hp.pixelfunc.ang2vec(th, phi)
rad = 10. * np.pi/180.
nside = hp.pixelfunc.get_nside(wmap_fgsub_v)


#idxs = hp.query_disc(nside, vec, rad) #
#wmap_fgsub_v[idxs] = hp.UNSEEN

# Apply foreground mask to WMAP data
assert hp.pixelfunc.get_nside(wmap_fgsub_v) == hp.pixelfunc.get_nside(wmap_ptsrc_mask)
wmap_fgsub_v[np.where(wmap_ptsrc_mask == 0.0)] = hp.UNSEEN
#wmap_ptsrc_mask[np.where(wmap_ptsrc_mask == 0.0)] = hp.UNSEEN


# Plot map
wmap_fgsub_v = hp.smoothing(wmap_fgsub_v, fwhm=1.4*np.pi/180.)
hp.mollview(wmap_fgsub_v, title="WMAP V", unit='mK', norm='hist')
#hp.mollview(wmap_ptsrc_mask, title="WMAP V") #, unit='mK', norm='hist')
hp.graticule()

"""
# Calculate spherical harmonics
LMAX = 1024
wmap_smoothed = hp.smoothing(wmap_fgsub_v, fwhm=0.1*np.pi/180.)
cl = hp.anafast(wmap_smoothed, lmax=LMAX)
ells = np.arange(len(cl))
P.subplot(111)
P.plot(ells, cl * ells * (ells + 1.) , 'r-')
"""

P.show()
