#!/usr/bin/python
"""Clean WMAP point source catalogue of clusters mis-identified as 
   contaminating point sources."""

import sys
sys.path.append("/home/phil/postgrad/lib")
import numpy as np
import healpy as hp
import pylab as P
import coordinates
import catalogues
import time
tstart = time.time()


print "**** OBSOLETE ***"
print "Exiting..."
exit()

################################################################################
# Cut and mask output settings
################################################################################

USE_PRECOMPUTED_DISTS = True

# Radius about cluster pointing within which a point source is identified as a 
# cluster (in multiples of theta500)
RADIUS_IDENTIFY = 1.

# Radius about cluster pointing within which a point source is identified as a 
# contaminant, and so the whole cluster is masked out (if rad > RADIUS_IDENTIFY)
# (in multiples of theta500)
RADIUS_CONTAM = 5.

NSIDE = 512 # Nside of output Healpix mask
PS_MASK_RAD = 0.5 * np.pi/180. # Point source mask radius, in rad. (0.5deg for now)

FNAME_MASK = "data/WMAP_cleaned_ptsrc_mask.fits" # Output filename for cleaned ptsrc mask
FNAME_PRECOMPUTED = "data/precomputed_ptsrc_angular_distances" # .npy appended on save
FNAME_WMAP_GALMASK = "data/wmap_point_source_catalog_mask_r9_7yr_v4.fits" # Galaxy mask


################################################################################
# Load data
################################################################################

cps = catalogues.CatalogueWmapPointSource(galcoords=True)
ccl = catalogues.CatalogueCleanMCXC(std_lrange=True, with_th500=True)



################################################################################
# Calculate angular distances between all clusters
################################################################################



# Get cluster-cluster angular distances
if USE_PRECOMPUTED_DISTS:
	# Access previously-saved distance data (~10x faster than recalculating, but still slow)
	print "\n\t*** Using precomputed angular distances..."
	angdists = np.load(FNAME_PRECOMPUTED+".npy")
else:
	# Calculate distances between all clusters (will take a while!)
	print "\n\t*** Computing angular distances..."
	angdists = coordinates.angdists_2catalogues(cps.l, cps.b, ccl.l, ccl.b)
	np.save(FNAME_PRECOMPUTED, angdists)
assert (ccl.l.size, cps.l.size) == np.shape(angdists)




################################################################################
# Identify candidates for clusters identified as point sources
################################################################################
"""
# Generate 2D array of cut radii, based on cluster radius
cuts = np.array( [ccl.theta500 * 180./np.pi] * cps.l.size ).T
idx_identify = np.where(angdists <= RADIUS_IDENTIFY*cuts, True, False)
##idx_contam = np.where(angdists < RADIUS_CONTAM*cuts and angdists > RADIUS_IDENTIFY*cuts, True, False)
idx_contam = np.where(angdists < RADIUS_CONTAM*cuts, True, False)
idx_contam = np.logical_xor(idx_contam, idx_identify) # Only take contam. clusters that aren't identified

# Get i,j indices of the elements that were cut, by dividing indexes by num. 
# rows/cols and finding whole part or remainder
idxs = np.arange(cuts.size).reshape(np.shape(angdists))

# Point sources identified as clusters
id_icl = np.floor(idxs[idx_identify] / np.shape(idx_identify)[1]).astype("i") # Cluster index
id_ips = np.mod(idxs[idx_identify], np.shape(idx_identify)[1]) # Ptsrc index
identified_pairs = [(id_icl[k], id_ips[k]) for k in range(id_icl.size)] # (cl_index, ps_index)

# Clusters contaminated by point sources
con_icl = np.floor(idxs[idx_contam] / np.shape(idx_contam)[1]).astype("i") # Cluster index
con_ips = np.mod(idxs[idx_contam], np.shape(idx_contam)[1]) # Ptsrc index
contam_pairs = [(con_icl[k], con_ips[k]) for k in range(con_icl.size)] # (cl_index, ps_index)


print "\tIdentified pairs (Cl,PtSrc):\n", len(identified_pairs), identified_pairs, "\n"
print "\tContaminated pairs (Cl,PtSrc):\n", len(contam_pairs), contam_pairs
"""


################################################################################
# Generate updated point source list, and construct new ptsrc catalogue
################################################################################

# Create mask for point source catalogue
m = np.array([True]*cps.l.size) # All True values
m[id_ips] = False # Set masked point sources

# New catalogue (pointings in rad.)
# 0 < l < 2pi; -pi/2 < b < pi/2, but need b = [0, pi] for Healpix
new_l = cps.l[m] * np.pi/180.
new_b = (cps.b[m] * np.pi/180.) + 0.5*np.pi



################################################################################
# Construct Healpix mask for point sources
################################################################################

# Construct new, empty map
mask = np.ones(hp.nside2npix(NSIDE)) # Mask 0 where data masked, so 1 where unmasked

# Loop through all point sources in catalogue, masking them out
for i in range(new_l.size):
	th = new_b[i] # FIXME: Is this the correct way around for the angular variables?
	phi = new_l[i]
	
	# Get indices of pixels to be masked
	vec = hp.pixelfunc.ang2vec(th, phi)
	idxs = hp.query_disc(NSIDE, vec, PS_MASK_RAD)
	
	# Mask pixels
	mask[idxs] = 0.0 # (0=masked, 1=unmasked)

# Output mask
hp.write_map(FNAME_MASK, mask)

# Get WMAP galactic mask and add pointsources to it (0=masked, 1=unmasked)
wmap_gal_mask = hp.read_map(FNAME_WMAP_GALMASK)
sum_mask = mask + wmap_gal_mask
sum_mask = np.where(sum_mask < 2., 0., 1.)

# Display mask
hp.mollview(sum_mask, title="Point source mask", norm='hist')
hp.graticule()


# Timing information
print "\nRun took", round(time.time() - tstart, 2), "sec."
P.show()
