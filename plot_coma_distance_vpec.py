#!/usr/bin/python
"""Plot Coma distances and peculiar velocities from the literature."""

import numpy as np
import pylab as P

# Distance, error+, error- (for different methods)
dist = 	[	[103, 42, 42], 	# SZ
			[96, 4, 5], 	# SZ
			#[235, 218, 98],	# SZ
			[102, 14, 14],	# SBF
			[85, 10, 10],	# SBF
			[100, 10, 10],	# SBF
			[105, 11, 11]]	# EARLY-TYPE
cols = ['r', 'r', 'b', 'b', 'b', 'y']
lbls = ["SZ", None, "SBF", None, None, "Early-type"]

# Plot distances
ax = P.subplot(211)
for i in range(len(dist)):
	P.errorbar(i, dist[i][0], yerr=dist[i][1], fmt=cols[i]+"x", label=lbls[i])
P.axhline(85., ls="dotted", color="k")
P.legend(prop={'size':'small'})
P.xlim((-1, 6))
P.ylim((50., 160.))
P.ylabel("Distance [Mpc]")
ax.get_xaxis().set_visible(False)


# Peculiar velocity (vpec [km/s], error)
vel = 	[	[170, 210], 	# TF
			[212, 186], 	# TF
			[-29, 299], 	# FP
			[-300, 500]]	# KSZ
vcols = ["r", "r", "b", "y"]
vlbls = ["TF", None, "FP", "KSZ"]

# Plot peculiar velocities
ax2 = P.subplot(212)
for i in range(len(vel)):
	P.errorbar(i, vel[i][0], yerr=vel[i][1], fmt=vcols[i]+"x", label=vlbls[i])
P.axhline(1000., ls="dotted", color="k")
P.axhline(0., ls="dotted", color="k", alpha=0.3)
P.legend(prop={'size':'small'})
P.xlim((-1, 4))
P.ylim((-1200., 1200.))
P.ylabel("Peculiar velocity [km/s]")
ax2.get_xaxis().set_visible(False)

P.show()
