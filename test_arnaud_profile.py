#!/usr/bin/python
"""Test my Arnaud profile code against published results."""

import numpy as np
from arnaud_profile import *
from load_rexcess import load_rexcess_catalogue
import pylab as P
import time
tstart = time.time()

# Take parameters of some cluster from Planck data (see arXiv:1204.2743, Tables A.1, D.1)
# and also Arnaud et al. 2010 (Table C.1)
# Cluster A68 (Planck, 1); Cluster RXC J2048.1-1750 (Arnaud, 2); Cluster RXC J2129.8-5048 (Arnaud, 3)
# Had to get z and M500 for (2) from Pratt et al. 0909.3776
# params: [alpha, beta, gamma, c500, P0, M500, r500, z]
##p1 = [1.31, 5.49, 0.0, 1.54, 24.01, 10.0e14, 1.030, 0.255]
#p2 = [1.76, 5.49, 0.0, 1.33, 4.34, 4.32e14/0.7, 1.095, 0.1475]
#p3 = [1.00, 5.49, 0.0, 0.94, 9.21, 2.26e14/0.7, 0.903, 0.0796]

# Load best-fit REXCESS pressure profiles
p = load_rexcess_catalogue()
C = [ArnaudProfile(pp) for pp in p]

# Plot pressure profile
r = np.logspace(-2., 1., 30)
P.subplot(221)
for i in range(len(C)):
	pp = C[i].P(r)
	P.plot(r/p[i][6], pp)
P.xlabel("r/r500")
P.ylabel("P(r)")
P.xscale('log')
P.yscale('log')
P.xlim((1e-2, 1e1))



# Plot SZ intensity profile
j = 0
rr = np.linspace(0., 5., 40)
dI = map( lambda x: C[j].dI_SZ(x), rr)
xx = np.concatenate((-rr[::-1]/p[j][6], rr/p[j][6]))
dIsym = np.concatenate((dI[::-1], dI))

P.subplot(222)
P.plot(xx, -dIsym)
P.xlabel("r/r500")
P.ylabel("-dI/I")
P.yscale('log')



# Plot Ysph as a function of radius

P.subplot(223)
for i in range(len(C)):
	P.plot(  r/p[i][6], map(lambda x: C[i].Ysph(x)/1e-5, r)  )
P.xlabel("r/r500")
P.ylabel("Ysph(r) [10^-5 Mpc^2]")



# Output timing information
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
