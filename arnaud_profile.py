#!/usr/bin/python
"""Arnaud et al. cluster pressure profile (arXiv:0910.1234)."""

import numpy as np
import scipy.integrate
from szunits import *

class ArnaudProfile(object):
	
	def __init__(self, params, precalc=False):
		"""Arnaud et al. cluster pressure profile (arXiv:0910.1234). Parameters 
		   are: [alpha, beta, gamma, c500, P0, M500, r500, z]
		"""
		
		# Set default cosmological parameters
		self.h70 = 0.7 # Hubble parameter, H0 = 100h km/s/Mpc
		self.Om = 0.3 # Omega_matter
		self.Ol = 0.7 # Omega_Lambda
		
		# Set mass scaling, alpha_p, defined in Arnaud Eq. 7.
		# alpha_p = 1/alpha_MYx - 5/3, alpha_MYx = 0.561 (X-ray best-fit)
		self.alpha_p =  0.12
		
		# Set pressure profile parameters
		self.alpha = params[0]
		self.beta = params[1]
		self.gamma = params[2]
		self.c500 = params[3]
		self.P0 = params[4]
		self.M500 = params[5]
		self.r500 = params[6]
		self.z = params[7]
		
		# Set SZ profile integration/interpolation limits (units of R500)
		self.bmax = 24. # Interpolation limit
		self.bmaxc = 25. # Integration limit
		
		# Pre-calculate some values
		self._P500 = self.P500(self.z)
		
		#self.profile_arnaud_bestfit()
		"""
		self.alpha = alpha # Intermediate slope (r ~ r_s)
		self.beta = beta # Outer slope (r >> r_s)
		self.gamma = gamma # Central slope (r << r_s)
		
		self.c500 = c500 # Concentration parameter
		self.r500 = r500 # Standard density scale radius, in Mpc
		self.rs = r500 / c500 # Scale radius
		
		self.P0 = P0 # Pressure scale
		self.P500 = P500 # Pressure scale at delta=500
		
		self.M0 = M0 # Mass scale
		self.M500 = M500 # Mass scale at delta=500 (units of Msun)
		# FIXME: Can't we calculate M500 from the NFW profile?
		"""
		
	
	############################################################################
	# Parameter handling
	############################################################################
	
	def profile_arnaud_bestfit(self):
		"""Use best-fit universal profile parameters from Arnaud Eq. 12."""
		
		# Universal profile parameters
		self.P0 = 8.403 * (self.h70)**(-1.5)
		self.c500 = 1.177
		self.gamma = 0.3081
		self.alpha = 1.0510
		self.beta = 5.4905
	
	
	############################################################################
	# Pressure profiles
	############################################################################
	
	def alpha_pp(self, x):
		"""Running of scaling with mass, from Arnaud Eq. 9."""
		y = (2.*x)**3.
		return 0.10 - ( (self.alpha_p + 0.10) * y / (1. + y) )
	
	def p(self, x):
		"""Dimensionless cluster pressure profile (GNFW), from Arnaud Eq. 11."""
		y = self.c500*x
		pp = y**self.gamma * (1. + y**self.alpha)**((self.beta - self.gamma)/self.alpha)
		return self.P0 / pp
	
	def P500(self, z):
		"""Pressure at radius where delta=500, from Arnaud Eq. 5."""
		# FIXME: Factor of h70^2 in Eq. 5 needs to be removed to match their 
		# results in Table C.1. Why is this?
		# Also note that, according to their Figs. 5--8, P(R500) \neq P500!
		pp = 1.65e-3 * (self.h(z))**(8./3.) #* self.h70**2.
		return pp * (self.M500 * self.h70 / 3e14)**(2./3.)
	
	def P(self, r):
		"""Extended cluster pressure profile, assuming standard cosmological 
		   evolution, from Arnaud Eq. 13. Units: keV cm^-3"""
		x = r/self.r500
		p500 = self.P500(self.z)
		aa = self.alpha_p + self.alpha_pp(x)
		return p500 * self.p(x) * (self.M500 * self.h70 / 3e14)**aa
	
	def xP(self, r):
		"""Extended cluster pressure profile, assuming standard cosmological 
		   evolution, from Arnaud Eq. 13. Dimensionless; multiply by P500(z)."""
		x = r/self.r500
		aa = self.alpha_p + self.alpha_pp(x)
		return self.p(x) * (self.M500 * self.h70 / 3e14)**aa
	
	############################################################################
	# Cosmology
	############################################################################
	
	def set_cosmology(self, h, Om, Ol):
		"""Set the cosmological parameters to use when evolving the model with 
		   redshift."""
		self.h70 = h70
		self.Om = Om
		self.Ol = Ol
	
	def h(self, z):
		"""Relative Hubble rate as a function of redshift."""
		# See definition at end of Section 1, p2 of Arnaud et al.
		return np.sqrt(self.Om*(1.+z)**3. + self.Ol)
	
	
	############################################################################
	# SZ observables
	############################################################################
	
	def Ysph(self, R):
		"""Integrate pressure profile over cluster volume, to give total SZ 
		   Y-parameter, Arnaud Eq. 14."""
		pp, err = scipy.integrate.quad(lambda x: self.xP(x)*x**2., 0., R)
		y = 4. * np.pi * Y_SCALE * pp * self._P500
		return y

	def Ycyl(self, R):
		"""Integrate pressure profile along a pencil beam through the cluster to 
		   give SZ Y-parameter, Arnaud Eq. 15."""
		return 0.
	
	def dI_SZ(self, l):
		"""SZ surface brightness profile perturbation, from Rephaeli Eqs. 6, 8."""
		# Valid in RJ limit, x << 1
		
		# Get dimensionless frequency
		x = X_SCALE * NU_WMAP_W / ( T_CMB * (1. + self.z) )
		
		# Integrate pressure profile at fixed radius
		xmax = 5. * self.r500; xmin = -xmax
		pp, err = scipy.integrate.quad( lambda x: self.xP(np.sqrt(x**2. + l**2.)), 
										xmin, xmax )
		y = Y_SCALE * pp * self._P500
		dI = -2. * y * (1. + 0.5*x)
		return dI
	
	
	############################################################################
	# SZ profiles
	############################################################################
	
	def Tloken(self, x):
		"""Loken universal temperature profile, in keV."""
		return 11.2 * (self.r500*0.7)**2. * ( 1. + 0.75*x)**(-1.6)
	
	def _ig_tsz(self, x, b):
		"""Integrand for tSZ profile."""
		return self.P(x*self.r500) * (x / np.sqrt(x**2. - b**2.))
	
	def _ig_ksz(self, x, b):
		"""Integrand for kSZ profile."""
		return self.P(x*self.r500) * (x / np.sqrt(x**2. - b**2.)) / self.Tloken(x)

	def tsz_profile(self, nu=1.):
		"""Return interpolation fn. for tSZ profile as a function of r."""
		bb = np.linspace(0.0, self.bmax, 100) # Range of impact parameters
		rr = bb * self.r500
		
		fac_ysz = (2. * 2. * 2.051 / 511.) * self.r500
		x = NU_SCALE * nu # Frequency/temperature
		g_nu = ( x*(np.exp(x) + 1.) / (np.exp(x) - 1.) ) - 4. # tSZ spectral dependence
		ysz = map(lambda b: scipy.integrate.quad(self._ig_tsz, b+0.0001,
											self.bmaxc, args=(b,))[0], bb)
		ysz = g_nu * fac_ysz * np.array(ysz)
		interp = scipy.interpolate.interp1d(rr, ysz, bounds_error=False, fill_value=0.0)
		return interp


	def ksz_profile(self, v, nu=1.):
		"""Return interpolation fn. for tSZ profile as a function of r."""
		bb = np.linspace(0.0, self.bmax, 100) # Range of impact parameters
		rr = bb * self.r500
	
		fac_ksz = 2. * 2.051 * v * self.r500 / 3e5
		ksz = map(lambda b: scipy.integrate.quad(self._ig_ksz, b+0.0001, 
											self.bmaxc, args=(b,))[0], bb)
		ksz = fac_ksz * np.array(ksz)
		interp = scipy.interpolate.interp1d(rr, ksz, bounds_error=False, fill_value=0.0)
		return interp
		
