#!/usr/bin/python
"""Routines for loading various data catalogues."""

import numpy as np
from pyltb import FLRW
import coordinates


# Catalogue locations
CAT_WMAP_POINTSOURCE = "data/wmap_ptsrc_catalog_cmb_free_p4_7yr_v4.txt"
CAT_PLANCK_ESZ = "data/PlanckESZ.dat"
CAT_MCXC_CLUSTERS = "data/MCXC_full_reformatted.dat"
CAT_MCXC_CLEAN = "data/MCXC_gal_trimmed_dup.dat"
CAT_MCXC_CLEAN_TH500 = "data/MCXC_gal_trimmed_dup_th500.dat"
CAT_REXCESS_ARNAUD = "data/Arnaud_ProfileFit_TblC1.dat"
CAT_REXCESS_PRATT = "data/Pratt_Basic_Tbl1.dat"



################################################################################
# WMAP Point Source catalogue
################################################################################

class CatalogueWmapPointSource(object):

	# Columns (see http://lambda.gsfc.nasa.gov/product/map/dr4/ptsrc_catalog_info.cfm)
	# Cols 1 -- 2	Right Ascension and Declination (2000) (deg)
	# Cols 3	Source Id
	# Cols 4	Optical Identification from NASA Extragalactic Database
	# Cols 5 -- 6	WMAP Q-band Flux and error (Jy)
	# Cols 7 -- 8	WMAP V-band Flux and error (Jy)
	# Cols 9 -- 10	WMAP W-band Flux and error (Jy)
	# Cols 11 -- 12	5 Ghz Identification
	# Col 13	Distance to the 5 GHz counterpart
	# Col 14	Source note
	
	def __init__(self, galcoords=False):
		"""Load the WMAP point source catalogue."""
		
		# Load the data
		self.fname = CAT_WMAP_POINTSOURCE
		delim = [7, 8, 15, 9, 4, 4, 5, 5, 4, 4, 22, 4] # Column widths
		dat = np.genfromtxt(self.fname, delimiter=delim, dtype=None, autostrip=True)
		
		# Populate variables
		self.ra, self.dec, self.srcid, self.opttype, self.fq, self.fqerr, self.fv, self.fverr, self.fw, self.fwerr, self.id5, self.cpdist = zip(*dat)
		
		# Convert to NumPy arrays
		self.ra = np.array(self.ra)
		self.dec = np.array(self.dec)
		
		# Convert to galactic coordinates
		if galcoords:
			self.l, self.b = coordinates.convert_eq_gal(self.ra, self.dec)
		else:
			# Set l,b variables to None type
			self.l = None
			self.b = None




################################################################################
# Planck ESZ Cluster catalogue
################################################################################

class CataloguePlanckESZ(object):
	
	# Table columns
	# (0) source_number, (1) name, (2) alt_name, (3) ra, (4) dec, (5) ra_xray, 
	# (6) dec_xray, (7) mmf_snr, (8) redshift, (9) lii, (10) bii, (11) xray_angular_size, 
	# (12) xray_int_compton_y, (13) xray_int_compton_y_error, (14) mmf_angular_size,
	# (15) mmf_angular_size_error, (16) mmf_int_compton_y, (17) mmf_int_compton_y_error
	
	def __init__(self):
		"""Load the Planck ESZ cluster database, from:
		http://heasarc.gsfc.nasa.gov/db-perl/W3Browse/w3table.pl?tablehead=name%3Dplanckeszc&Action=More+Options"""
		
		print "WARNING: CataloguePlanckESZ() column names needs checking."
		
		# Column types
		fcols = (0, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17) # Floats
		scols = (1, 2, 3, 4, 5, 6) # Strings
		
		# Load the data
		self.fname = CAT_PLANCK_ESZ
		# Float valued
		self.mmf_snr, self.redshift, self.lii, self.bii, self.xray_angular_size, self.xray_int_compton_y, self.xray_int_compton_y_error, self.mmf_angular_size, self.mmf_angular_size_error, self.mmf_int_compton_y, self.mmf_int_compton_y_error = np.genfromtxt(self.fname, delimiter=",", usecols=fcols, skip_header=1).T
		# String valued
		self.name, self.alt_name, self.ra, self.dec, self.ra_xray, self.dec_xray = np.genfromtxt(self.fname, delimiter=",", usecols=scols, skip_header=1).T
		
		self.ra = map(lambda x: coordinates.ra2float(x), self.ra)
		self.dec = map(lambda x: coordinates.dec2float(x), self.dec)
		self.ra_xray = map(lambda x: coordinates.ra2float(x), self.ra_xray)
		self.dec_xray = map(lambda x: coordinates.ra2float(x), self.dec_xray)




################################################################################
# MCXC X-ray cluster catalogue (original)
################################################################################

class CatalogueMCXC(object):
	
	# Semicolon separated (commas in text fields)
	#0: Column	MCXC	(A12)	MCXC name (JHHMM.m+DDMM)	[ucd=meta.id,meta.main]
	#1: Column	OName	(a18)	Other name	[ucd=meta.id]
	#2: Column	AName	(a54)	Alternative name	[ucd=meta.id]
	#3: Column	RAJ2000	(A10)	Right ascension (J2000)	[ucd=pos.eq.ra,meta.main]
	#4: Column	DEJ2000	(A9)	Declination (J2000)	[ucd=pos.eq.dec,meta.main]
	#5: Column	z	(F7.4)	Redshift	[ucd=src.redshift]
	#6: Column	Scale	(F6.3)	Scale	[ucd=instr.scale]
	#7: Column	L500	(F9.6)	X-ray luminosity in 10^44^erg/s (1)	[ucd=phys.luminosity,em.X-ray]
	#8: Column	M500	(F7.4)	Total mass (1)	[ucd=phys.mass]
	#9: Column	R500	(F7.4)	Characteristic radius (1)	[ucd=phys.size.radius]
	#10: Column	Notes	(a42)	Notes (losStr = line of sight structure)	[ucd=meta.note]
	
	def __init__(self):
		"""Load the original MCXC X-ray cluster catalogue."""
		
		self.fname = CAT_MCXC_CLUSTERS
		fcols = (5, 6, 7, 8, 9) # Data columns (float)
		scols = (0, 1, 2, 3, 4, 10) # Data columns (strings)

		# Load data and convert coordinates
		self.z, self.scale, self.L500, self.M500, self.R500 = np.genfromtxt(self.fname, delimiter=";", usecols=fcols).T
		self.name1, self.name2, self.name3, self.ra, self.dec, self.notes = np.genfromtxt(self.fname, dtype="a", delimiter=";", usecols=scols).T
		
		# Convert coordinates
		self.ra = np.array(  map(lambda x: coordinates.ra2float(x), self.ra)  )
		self.dec = np.array(  map(lambda x: coordinates.dec2float(x), self.dec)  )
		self.l, self.b = coordinates.convert_eq_gal(self.ra, self.dec)

		# Assign IDs
		self.ids = np.arange(0, self.z.size)

		# Get LOS substructure flags
		flag_substr = map(lambda x: "losstr" in x.lower() and True or False, self.notes)

		# Calculate angular size at R500
		model = FLRW(h=0.7, Om=0.3, Ol=0.7) # Define reference cosmological model
		self.dA = np.array(  map(lambda x: model.dA(x), self.z)  )
		self.th500 = self.R500 / self.dA
	
	def save_catalogue(self, fname):
		"""Save the catalogue in a tab-separated text format."""
		# FIXME: Write this function
		print "WARNING: save_catalogue() not implemented. Catalogue not saved."



################################################################################
# Processed MCXC catalogue (generated by clean_mcxc.py)
################################################################################

class CatalogueCleanMCXC(object):

	# Columns (see clean_mcxc.py)
	## fmtstr = "%+10.5f\t%9.5f\t%6.4f\t%6.3f\t%9.6f\t%7.4f\t%7.4f   "
	# (0) l NOTE: Range of l taken to be [-180 < l < +180]
	# (1) b
	# (2) z
	# (3) scale
	# (4) L500
	# (5) M500
	# (6) R500
	
	def __init__(self, std_lrange=False, with_th500=False):
		"""Load the cleaned MCXC cluster X-ray catalogue."""
		
		# Load the data
		if with_th500:
			# Load version of file required by clean_pointsource_map.py (with theta500)
			self.fname = CAT_MCXC_CLEAN_TH500
			dat = np.loadtxt(self.fname, delimiter="\t").T
			self.l, self.b, self.z, self.scale, self.L500, self.M500, self.R500, self.theta500 = dat
		else:
			# Load version of file required by Commander (without theta500)
			self.fname = CAT_MCXC_CLEAN
			dat = np.loadtxt(self.fname, delimiter="\t").T
			self.l, self.b, self.z, self.scale, self.L500, self.M500, self.R500 = dat
			self.theta500 = None
		
		# Use standard range for galactic coordinates, [0 < l < 360]
		if std_lrange:
			self.l = np.where(self.l < 0., self.l+360., self.l)



################################################################################
# REXCESS cluster catalogue
################################################################################

class CatalogueREXCESS(object):
	
	# Table columns (Arnaud)
	# (beta fixed to 5.49; fiducial LCDM cosmology with h=0.7, Om=0.3, Ol=0.7)
	# (0) Cluster name; (1) R500 [Mpc]; (2) YX [10^14 Msun kev]; (3) err;
	# (4) Ysph(R2500) [10^-5 Mpc^2]; (5) err; (6) Ysph(R500) [10^-5 Mpc^2];
	# (7) err; (8) P500 [10^-3 kev cm^-3]; (9) P0; (10) c500; (11) alpha;
	# (12) gamma; (13) chi^2; (14) NDF;

	# Table columns (Pratt)
	# (0) Cluster name; (1) z; (2) kT [kev]; (3) err-; (4) err+;
	# (5) M500 [h70^-1 10^14 Msun]; (6) err-; (7) err+;
	
	def __init__(self):
		"""Load the REXCESS cluster database, from Arnaud et al. (2010), 
		   Table C.1, and Pratt et al. (2009), Table 1."""
		
		# Load data
		self.fname1 = CAT_REXCESS_ARNAUD
		self.fname2 = CAT_REXCESS_PRATT
		self.fname = [self.fname1, self.fname2]
		arncols = (1,2,3,4,5,6,7,8,9,10,11,12,13,14)
		prattcols = (1,2,3,4,5,6,7)
		dat1 = np.genfromtxt(self.fname1, delimiter=",", usecols=arncols, skip_header=1)
		dat2 = np.genfromtxt(self.fname2, delimiter=",", usecols=prattcols, skip_header=1)
		
		# Parameters
		# FIXME: Should load cluster names too.
		print "FIXME: CatalogueREXCESS() should load cluster names too."
		# Arnaud:
		self.r500 = []; self.YX = []; self.YX_err = []
		self.Ysph2500 = []; self.Ysph2500_err = []; self.Ysph500 = []
		self.Ysph500_err = []; self.P500 = []; self.P0 = []; self.c500 = []
		self.alpha = []; self.beta = []; self.gamma = []; self.chi2 = []
		self.ndf = []
		# Pratt:
		self.z = []; self.kT = []; self.kT_errhigh = []
		self.kT_errlow = []; self.M500 = []; self.M500_errhigh = []
		self.M500_errlow = []
		
		# Construct parameter lists (for use in cluster profile code)
		# [alpha, beta, gamma, c500, P0, M500, r500, z]
		self.params = []
		for i in range(len(dat1)):
			# Construct each row in [params] list
			tmp = [0.0 for j in range(8)]
			tmp[0] = dat1[i][10]
			tmp[1] = 5.49
			tmp[2] = dat1[i][11]
			tmp[3] = dat1[i][9]
			tmp[4] = dat1[i][8]
			tmp[5] = dat2[i][4] * 1e14 / 0.7 # Measured in units of h70
			tmp[6] = dat1[i][0]
			tmp[7] = dat2[i][0]
			self.params.append(tmp)
			
			# Construct columns for individual variables
			# Arnaud:
			#self.clname.append(dat1[i][])
			self.r500.append(dat1[i][0])
			self.YX.append(dat1[i][1])
			self.YX_err.append(dat1[i][2])
			self.Ysph2500.append(dat1[i][3])
			self.Ysph2500_err.append(dat1[i][4])
			self.Ysph500.append(dat1[i][5])
			self.Ysph500_err.append(dat1[i][6])
			self.P500.append(dat1[i][7])
			self.P0.append(dat1[i][8])
			self.c500.append(dat1[i][9])
			self.alpha.append(dat1[i][10])
			self.beta.append(5.49)
			self.gamma.append(dat1[i][11])
			self.chi2.append(dat1[i][12])
			self.ndf.append(dat1[i][13])
			# Pratt:
			#self.clname_pratt.append(dat2[i][])
			self.z.append(dat2[i][0])
			self.kT.append(dat2[i][1])
			self.kT_errhigh.append(dat2[i][2])
			self.kT_errlow.append(dat2[i][3])
			self.M500.append(dat2[i][4] * 1e14/0.7) # Measured in units of h70
			self.M500_errhigh.append(dat2[i][5] * 1e14/0.7)
			self.M500_errlow.append(dat2[i][6] * 1e14/0.7)
		
		
