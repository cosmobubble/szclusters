#!/usr/bin/python
"""Equatorial->Galactic coordinate convertor, from Durham.
   Modified by Phil Bull, May 2012.
   http://www.dur.ac.uk/physics.astrolab/py_source/conv.py_source
"""

import numpy

def eq2gal(s_ra, s_dec):
    """Convert from equatorial string to decimal galactic coordinates."""
    
    # Convert ra,dec strings
    try:
    	hh, mm, ss = s_ra.split(" ")
    	deg, amin, asec = s_dec.split(" ")
    except:
    	print "ERROR: Failed to process strings. Format not expected."
    	print s_ra.split(" ")
    	print s_dec.split(" ")
    	return None, None
    
    ra = float(hh) * 
    
    """
	# Convert from decimal ra,dec to galactic coords
    rmat = numpy.array([
     [-0.054875539726,-0.873437108010,-0.483834985808],
     [+0.494109453312,-0.444829589425,+0.746982251810],
     [-0.867666135858,-0.198076386122,+0.455983795705]],dtype='d')
    cosb = math.cos(dec)
    v1 = numpy.array([math.cos(ra)*cosb,math.sin(ra)*cosb,math.sin(dec)])
    v2=numpy.multiply(rmat,v1)
    x = v2[0]
    y = v2[1]
    z = v2[2]
    r=math.sqrt(x*x+y*y)
    if r == 0.0:
        l = 0.0
    else:
        l = math.atan2(y,x)
    if z == 0.0:
        b = 0.0
    else:
        b = math.atan2(z,r)
    ll = math.fmod(l,2.*math.pi)
    if ll < 0.0: ll = ll + 2.*math.pi

    bb = math.fmod(b,2*math.pi)
    if abs(bb) >= math.pi: print "Ugh!" 
    
    return ll, bb
    """
