#!/usr/bin/python
"""Plot Table 6 r200 vs r500 from Locuss paper."""

import numpy as np
import pylab as P
import scipy.optimize

# Load data
r200, er200, r500, er500 = np.genfromtxt("data/locuss-table6-r200-r500.dat", delimiter=" ").T

# Get r200, r500 into same units
r500 *= 0.1
er500 *= 0.1


def leastsq(p, x, y, xerr, yerr):
	c = 0.0#p[0]
	m = p[0]
	# sigma_i^2 = sigma_y^2 + m^2 sigma_x^2
	err = np.sqrt(yerr**2. + (m*xerr)**2.)
	return (m*x + c - y) / err

# Fit straight line
#p0 = [0.0, 1.0] # c, m
p0 = [1.0]
p = scipy.optimize.leastsq(leastsq, p0, args=(r500, r200, er500, er200))[0]
print p


P.subplot(111)
P.errorbar(r500, r200, xerr=er500, yerr=er200, fmt="b.", alpha=0.5)
x = np.linspace(0.85*np.min(r500), 1.1*np.max(r500), 200)
#P.plot(x, p[0] + p[1]*x, 'r-')
P.plot(x, p[0]*x, 'r-')
P.xlabel("R_500 [h_100^-1 Mpc]")
P.ylabel("R_200 [h_100^-1 Mpc]")
P.show()
