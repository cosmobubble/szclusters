#!/usr/bin/python
"""Find random pointings that are not within N*theta_500 of some cluster.
"""

import numpy as np
import pylab as P
from astropysics.coords.coordsys import GalacticCoordinates
#gc = coords.coordsys.GalacticCoordinates
import time
tstart = time.time()

np.random.seed(10)


"""
def dist(l1, b1, l2, b2):
	"" "Find distance between two points in galactic coordinates."" "
	c1 = coords.coordsys.GalacticCoordinates(l1, b1)
	angsep = [c1 - coords.coordsys.GalacticCoordinates(l2[i], b2[i]) for i in range(l2.size)]
	return np.array([abs(a.d) * np.pi/180. for a in angsep])
"""

# Load data
fname = "data/MCXC_processed.dat"
ids, z, l, b, da, th500, r500, m500, l500, scale = np.genfromtxt(fname, delimiter="\t").T

# Define l, b range and flag radius nrad
lrange = (0., 360.)
brange = (-90., 90.)
nrad = 15.
Ncl = 10 # No. clusters to get

goodl = []; goodb = []; goodmin = []
ii = -1; good = 0

while (good < Ncl) and (ii<100):
	ii += 1
	
	# Get random coords
	ll = np.random.uniform(low=lrange[0], high=lrange[1])
	bb = np.random.uniform(low=brange[0], high=brange[1])
	cc = GalacticCoordinates(ll, bb)
	
	print "TRIAL ID:", ii
	print "\tl =", ll, " / b =", bb

	# Get angular separations
	angsep = [cc - GalacticCoordinates(l[i], b[i]) for i in range(l.size)]
	angsep = np.array([abs(a.d)*np.pi/180. for a in angsep])
	
	# Check if minimum distance is low enough
	print "\tMin. ang. sep.:", np.min(angsep/th500)
	print "\tMax. ang. sep.:", np.max(angsep/th500)
	print "\tAvg. ang. sep.:", np.mean(angsep/th500)
	if np.min(angsep/th500) > nrad:
		goodl.append(ll); goodb.append(bb)
		goodmin.append( np.min(angsep/th500) )
		good += 1
		print "\tACCEPTED"
	else:
		print "\tREJECTED"
	print ""

# Output results
print "ID\tl\tb\tmin(n*th_500)"
for i in range(len(goodl)):
	print i, goodl[i], goodb[i], goodmin[i]

exit()


# Output catalogue data in format used by Commander code
# Need to rejig the range of gal. coord. "l", so it's between [-180, 180], 
# rather than [0, 360]. This should be done by taking any l > 180 and 
# subtracting 360
l2 = np.where(l > 180., l-360., l)

# The template file I have (from H.K. Eriksen, 2012-05-07) appears to be in the 
# following format:
# l, b, z, scale, L500, M500, R500
fmtstr = "%+10.5f\t%9.5f\t%6.4f\t%6.3f\t%9.6f\t%7.4f\t%7.4f   "
m = i5
dat = np.column_stack((l2[m], b[m], z[m], scale[m], l500[m], m500[m], r500[m]))
np.savetxt("data/MCXC_Coma_5.dat", dat, fmt=fmtstr, delimiter="\t")

m = i15
dat = np.column_stack((l2[m], b[m], z[m], scale[m], l500[m], m500[m], r500[m]))
np.savetxt("data/MCXC_Coma_15.dat", dat, fmt=fmtstr, delimiter="\t")


# Timing information
print "Run took", round(time.time() - tstart, 2), "sec."
