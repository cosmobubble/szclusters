#!/usr/bin/python
"""Plot an Arnaud pressure profile for some example cluster parameters."""

import numpy as np
import pylab as P
from arnaud_profile import ArnaudProfile

# Cluster parameters
R500 = 0.6296
M500 = 0.7373e14
z = 0.0396

# Arnaud universal pressure profile parameters
h = 0.7
P0 = 8.403 * h**(-1.5)
c500 = 1.177
gamma = 0.3081
alpha = 1.0510
beta = 5.4905

# Initialise pressure profile
params = [alpha, beta, gamma, c500, P0, M500, R500, z]
profile = ArnaudProfile(params)

# Plot the profile
P.subplot(111)
x = np.logspace(-3, 1., 200)
P.plot(x, profile.P(x*R500), 'b-')

P.yscale('log')
P.xscale('log')
P.ylabel("P(r) [keV/cm^3]")
P.xlabel("r / R_500")
P.show()
