#!/usr/bin/python
"""Convert between different systems and formats of coordinates, and calculate 
   angular distances."""

import numpy as np
import astropysics.coords as coords


################################################################################
# Convert between coordinate systems and formats
################################################################################

def convert_eq_gal(ra_, dec_):
	"""Convert arrays from FK5/J2000 equatorial coords to galactic."""
	c = [coords.coordsys.FK5Coordinates((ra_[i], dec_[i])) for i in range(ra_.size)]
	c = [cc.convert(coords.GalacticCoordinates) for cc in c]
	l = [cc.l.degrees for cc in c]
	b = [cc.b.degrees for cc in c]
	return np.array(l), np.array(b)

def ra2float(sra):
	"""Convert the string form of equatorial coordinate RA to float."""
	_ra = [float(num) for num in sra.split(" ")]
	ra = (_ra[0] * 360./24.) + (_ra[1] * 360./(24.*60.)) + (_ra[2] * 360./(24.*3600.))
	return ra

def dec2float(sdec):
	"""Convert the string form of equatorial coordinate RA to float."""
	_dec = [float(num) for num in sdec.split(" ")]
	if _dec[0] < 0.0:
		sgn = -1.0
	else:
		sgn = 1.0
	dec =  sgn * (  abs(_dec[0]) + (_dec[1] / 60.) + (_dec[2] / 3600.)  )
	return dec



################################################################################
# Calculate angular distances
################################################################################

def gal_angdist_pairs(l, b):
	"""Find angular distance between all pairs of points, in galactic coordinates."""
	
	# Get array of galactic coordinate objects
	c = [coords.coordsys.GalacticCoordinates(l[i], b[i]) for i in range(l.size)]
	
	# Get angular distance between pairs of coordinates
	# (only for i<j, to avoid calculating twice)
	fac = np.pi/180.
	NN = len(c)
	delta = [[fac*(c[i] - c[j]).d for i in range(j+1,NN)] for j in range(0, NN)]
	
	return delta # Ragged list!


def angdists_catalogue(l, b):
	"""Find distance between all entries (degrees, gal. coords.) for a single catalogue."""
	NN = range(l.size)
	
	# Get array of galactic coordinate objects
	c = [coords.coordsys.GalacticCoordinates(l[i], b[i]) for i in NN]
	
	# Get angular distance between pairs of coordinates
	delta = np.zeros((l.size, l.size)) # Empty array
	for i in NN:
		for j in NN:
			if i < j:
				delta[i][j] = (c[i] - c[j]).d
			elif i > j:
				delta[i][j] = delta[j][i]
	return delta


def angdists_2catalogues(l1, b1, l2, b2):
	"""Find distance between two points in degrees, in galactic coordinates, 
	   from two catalogues."""
	N1 = range(l1.size)
	N2 = range(l2.size)
	
	# Get arrays of galactic coordinate objects
	c1 = [coords.coordsys.GalacticCoordinates(l1[i], b1[i]) for i in N1]
	c2 = [coords.coordsys.GalacticCoordinates(l2[i], b2[i]) for i in N2]
	
	# Get angular distance between pairs of coordinates
	delta = [[(c1[i] - c2[j]).d for i in N1] for j in N2]
	return np.array(delta)



def gal_close_pairs(l, b, cut):
	"""Get pairs closer than some angular separation 'cut', in galactic coordinates."""
	
	delta = gal_angdist_pairs(l, b) # Get angular distances
	
	# Find indices of pairs with distances below the cut
	cut_pairs = []
	for i in range(len(delta)):
		for j in range(len(delta[i])):
			if delta[i][j] < cut:
				cut_pairs.append((i, j))
	return cut_pairs


def gal_find_neighbours(l1, b1, l2, b2, cut):
	"""Find items in one catalogue which neighbour each item in another 
	   catalogue, within some angular distance 'cut' (in degrees)."""
	# Get coordinates
	c1 = [coords.coordsys.GalacticCoordinates(l1[i], b1[i]) for i in range(l1.size)]
	c2 = [coords.coordsys.GalacticCoordinates(l2[i], b2[i]) for i in range(l2.size)]
	
	# Get angular distances
	N1 = len(c1); N2 = len(c2)
	delta = [[(c1[i] - c2[j]).d for i in range(0, N1)] for j in range(0, N2)]
	delta = np.array(delta)
	
	# Test distances
	close_pairs = delta < cut
	print close_pairs


def eq_catalogue_distances(ra1, dec1, ra2, dec2):
	"""Find items in one catalogue which neighbour each item in another 
	   catalogue, within some angular distance 'cut' (in degrees).
	   (Equatorial coordinates.)"""
	# Get coordinates
	c1 = [coords.coordsys.FK5Coordinates((ra1[i], dec1[i])) for i in range(ra1.size)]
	c2 = [coords.coordsys.FK5Coordinates((ra2[i], dec2[i])) for i in range(ra2.size)]
	
	# Get angular distances
	N1 = len(c1); N2 = len(c2)
	delta = [[(c1[i] - c2[j]).d for i in range(0, N1)] for j in range(0, N2)]
	delta = np.array(delta)
	return delta



