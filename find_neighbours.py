#!/usr/bin/python
"""Load the reprocessed data from the MCXC catalogue.
   (Catalogue processed with process_mcxc.py)
"""

import numpy as np
import pylab as P
import astropysics.coords as coords
import time
tstart = time.time()

# Cluster cut settings
cut = 0.011 # in radians; Coma is 0.011828
n = 5. # Cut out anything within n*r500 of a flagged cluster

# Load data
fname = "data/MCXC_processed.dat"
ids, z, l, b, da, th500, r500, m500, l500, scale = np.genfromtxt(fname, delimiter="\t").T

def dist(l1, b1, l2, b2):
	"""Find distance between two points in galactic coordinates."""
	c1 = coords.coordsys.GalacticCoordinates(l1, b1)
	angsep = [c1 - coords.coordsys.GalacticCoordinates(l2[i], b2[i]) for i in range(l2.size)]
	return np.array([abs(a.d) * np.pi/180. for a in angsep])

# Find nearest neighbours for largest clusters
y, edges = np.histogram(th500, bins=25., range=(0.0, 0.05))
x = [edges[i] for i in range(edges.size-1)] # Centroids
#for i in range(len(x)):
#	print x[i], "\t", y[i]


# Make list of all clusters which fail the cut
thcut = th500 > cut
idxs = ids[thcut] # Indexes of these clusters

# Loop through clusters to be masked and find nearest neighbours
flagged_nearby = []; dist_flagged_nearby = []
for i in idxs:
	dists = dist(l[i], b[i], l, b)
	neighbours = np.where( dists < n*th500[i] )[0]
	flagged_nearby.append( list(ids[neighbours]) )
	dist_flagged_nearby.append( list(dists[neighbours]) )


################################################################################
# Output cluster cut report
################################################################################

print "="*20
print "CLUSTER CUT REPORT"
print "="*20
print "Cut radius:", n, "x R500"

for j in range(len(flagged_nearby)):
	print ""
	print "-"*30
	print "("+str(int(idxs[j]))+")"
	print "-"*30
	
	print "R500:\t", r500[idxs[j]], "Mpc"
	print "z:\t", z[idxs[j]]
	print "th500:\t", round(th500[idxs[j]], 4), "rad."
	print "th_cut:\t", round(n*th500[idxs[j]], 4), "rad."
	print " ", "-"*30
	print " ", "ID\tDist.\tz"
	print " ", "-"*30
	for k in range(len(flagged_nearby[j])):
		print " ", int(flagged_nearby[j][k]), "\t", round(dist_flagged_nearby[j][k], 4),
		print "\t", z[int(flagged_nearby[j][k])]
	print " ", "-"*30

total_cut = sum([len(f) for f in flagged_nearby])
print "\nClusters removed (initial cut):", idxs.size
print "Clusters removed (inc. overlap):", total_cut, "("+str(round(100.*total_cut/ids.size, 1))+"%)\n"


################################################################################
# Construct new arrays, without the cut clusters
################################################################################

# Get list of cut clusters
cut_ids = [int(cl) for row in flagged_nearby for cl in row] # There may be duplicate indexes. This is allowed and will be handled.

# Create a mask which excludes cut clusters
_idxs = np.arange(0, ids.size) # Array of indices
for val in cut_ids: _idxs = np.ma.masked_values(_idxs, val, copy=False) # Apply mask to cut clusters
uncut_mask = np.where(_idxs.mask, False, True) # Get inverted cut mask


################################################################################
# Output data in format used by Commander code
################################################################################

# Need to rejig the range of gal. coord. "l", so it's between [-180, 180], 
# rather than [0, 360]. This should be done by taking any l > 180 and 
# subtracting 360
l2 = np.where(l > 180., l-360., l)

# The template file I have (from H.K. Eriksen, 2012-05-07) appears to be in the 
# following format:
# l, b, z, scale, L500, M500, R500
fmtstr = "%+10.5f\t%9.5f\t%6.4f\t%6.3f\t%9.6f\t%7.4f\t%7.4f   "
m = uncut_mask
dat = np.column_stack((l2[m], b[m], z[m], scale[m], l500[m], m500[m], r500[m]))
np.savetxt("data/MCXC_gal_trimmed.dat", dat, fmt=fmtstr, delimiter="\t")

"""
################################################################################
# Plot the results
################################################################################

# Get relative sizes
thmax = max(th500); thmin = min(th500)
frac = (th500-thmin)/(thmax - thmin)
sz = 1.0 + 24.*frac

# Get colours
cols = ["y" for i in range(z.size)] # All black by default
for i in range(len(flagged_nearby)): # Blue if they are too close to a cluster
	for ii in flagged_nearby[i]:
		cols[int(ii)] = "k"
for ii in idxs:
	cols[int(ii)] = "r" # Red if they are the parent cluster


# Plot information
#ax = P.subplot(111, projection="mollweide")
ax = P.subplot(111)
for i in range(ids.size):
	#P.plot(l[i]*(np.pi/180.) - np.pi, b[i]*(np.pi/180.), marker=".", mec=cols[i], mfc=cols[i], ms=5.) # sz[i]
	P.plot(l[i], b[i], marker=".", mec=cols[i], mfc=cols[i], ms=5.) # sz[i]

for label in ax.get_xticklabels(): label.set_visible(False)
for label in ax.get_yticklabels(): label.set_visible(False)

################################################################################
"""

"""
# Histogram of angular sizes
y, edges = np.histogram(th500, bins=50., range=(0.0, 0.05))
x = [edges[i] for i in range(edges.size-1)] # Centroids
P.subplot(111)
P.plot(x, y, 'r+', mew=1.)
P.yscale('log')
P.ylim((0.1, 600.))
P.show()
"""


# Timing information
print "Run took", round(time.time() - tstart, 2), "sec."
##P.show()
