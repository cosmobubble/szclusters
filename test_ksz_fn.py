#!/usr/bin/python
"""kSZ temperature profile, as a function of radius of the cluster."""

import numpy as np
import pylab as P
import scipy.integrate
import scipy.interpolate
from arnaud_profile import ArnaudProfile

def Tloken(x, r500):
	return 11.2 * (r500*0.7)**2. * ( 1. + 0.75*x)**(-1.6)


################################################################################
# Cluster 1 from MCXC
# Set params
################################################################################
R500 = 0.6296
M500 = 0.7373e14
z = 0.0396
# Universal pressure profile
P0 = 8.403 * (0.7)**(-1.5)
c500 = 1.177
gamma = 0.3081
alpha = 1.0510
beta = 5.4905
params = [alpha, beta, gamma, c500, P0, M500, R500, z]
CL = ArnaudProfile(params)
print "h(z) =", CL.h(z)

################################################################################
# tSZ and kSZ integrands
################################################################################
bmax = 10.
bmaxc = 25.
bb = np.linspace(0.0, bmax, 150)
ig_tsz = lambda x, b, r500, CL: CL.P(x*r500) * (x/np.sqrt(x**2. - b**2.))
ig_ksz = lambda x, b, r500, CL: CL.P(x*r500) * (x/np.sqrt(x**2. - b**2.)) / Tloken(x, r500)

"""
################################################################################
# Thermal SZ
ysz = map(lambda b: scipy.integrate.quad(ig_tsz, b+0.0001, bmaxc, args=(b,R500,CL))[0], bb)
ysz = 2. * 2. * 2.051 * R500 * np.array(ysz) / 511. # me.c^2
# sigma_T [cm^2] * 1 Mpc/cm = 2.051

# Kinetic SZ
# Factor of 2 comes from integration, factor of sigma_T, and conversion from cm->Mpc
tau = map(lambda b: scipy.integrate.quad(ig_ksz, b+0.0001, bmaxc, args=(b,R500,CL))[0], bb)
ksz = 2. * 2.051 * (3e2 / 3e5) * R500 * np.array(tau) # using v = 300 km/s

print "Max. Tau =", round(np.max(tau), 5)
print "Y_(R500) =", round(CL.Ysph(R500) / 1e-5, 5), "10^-5 Mpc^2"
"""

"""
##################################

# Cylindrical integration (tSZ)
fn_tsz = lambda x: R500*(R500*x)*scipy.integrate.quad(ig_tsz, x+0.00001, bmaxc, args=(x,R500,CL))[0]
tszsamp = map(lambda b: fn_tsz(b), bb)
tsz_cyl = []
for i in range(len(bb)-1):
	tsz_cyl.append(  scipy.integrate.simps(tszsamp[:i+1], bb[:i+1])  )
tsz_cyl = 2.*2.*np.pi * R500 * 2. * 2.051 * np.array(tsz_cyl) / 511.

# Cylindrical integration (kSZ)
fn_ksz = lambda x: R500*R500*x*scipy.integrate.quad(ig_ksz, x+0.00001, bmaxc, args=(x,R500,CL))[0]
kszsamp = map(lambda b: fn_ksz(b), bb)
ksz_cyl = []
for i in range(len(bb)-1):
	ksz_cyl.append(  scipy.integrate.simps(kszsamp[:i+1], bb[:i+1])  )
ksz_cyl = 2.*2.*np.pi * R500 * 2.051 * (3e2 / 3e5) * np.array(ksz_cyl)


# Spherical integration
int_sph = lambda x: CL.P(x*R500) * (x*R500)**2.
tsz_sph = map(lambda b: 4.*np.pi * scipy.integrate.quad(int_sph, 1e-5, b)[0], bb)
tsz_sph = 2. *R500 * 2.051 * np.array(tsz_sph) / 511.

int_ksph = lambda x: CL.P(x*R500) * (x*R500)**2. / Tloken(x, R500)
ksz_sph = map(lambda b: 4.*np.pi * scipy.integrate.quad(int_ksph, 1e-5, b)[0], bb)
ksz_sph = 2.051 * R500 * (3e2 / 3e5) * np.array(ksz_sph)

P.subplot(211)
P.plot(bb[1:], tsz_cyl*1e5, 'r-', label="tSZ")
P.plot(bb[1:], ksz_cyl*1e5, 'b-', label="kSZ")
P.plot(bb, tsz_sph*1e5, 'r--', label="tSZ sph")
P.plot(bb, ksz_sph*1e5, 'b--', label="kSZ sph")
#P.yscale('log')
P.ylabel(r"$\int (\Delta T/T)$ $2 \pi r dr$ $\times 10^{-5}$")
P.xlabel("r / R500")
#P.xlim((0., 15.))
P.legend(loc="lower right", prop={'size':'small'})
P.grid(True)


P.subplot(212)
P.plot(bb[1:], ksz_cyl/tsz_cyl, 'k-', label="kSZ/tSZ (cyl.)")
P.plot(bb, ksz_sph/tsz_sph, 'g-', label="kSZ/tSZ (sph.)")
#P.yscale('log')
P.ylabel("Ratio (kSZ/tSZ)")
P.xlabel("r / R500")
P.legend(loc="lower right", prop={'size':'small'})
#P.xlim((0., 15.))
P.grid(True)

P.show()

exit()
"""

"""
################################################################################
# Plot profiles
# dT/T for tSZ and kSZ
################################################################################
ax = P.subplot(111)
P.plot(bb, ysz, 'r-', label=r"tSZ ($g(\nu)=2$)")
P.plot(bb, ksz, 'b-', label="kSZ (vp=300km/s)")

#P.xlim((0., 3.))
P.xlabel("Impact parameter b / R500")
P.ylabel("$\Delta T / T$(b)")
P.yscale('log')
ax.xaxis.set_major_locator(P.MaxNLocator(7))

P.grid(True)
P.legend(loc="upper right", prop={'size':'small'})
"""

################################################################################
# Pressure and temperature profiles
################################################################################

"""
P.subplot(212)
xx = np.logspace(-3, np.log10(5.), 200)
P.plot(xx, Tloken(xx, R500), 'k-', label="T(r) [keV]")
P.plot(xx, CL.P(xx*R500), 'r-', label="P(r) [keV/cm^3]")

# Ingunn
P.plot(pdat[0], pdat[1], 'b--')
P.plot(tdat[0], tdat[1], 'g--')
"""


# RATIO: Ingunn P(r) vs. Phil P(r)
# Load Ingunn's profiles
pdat = np.genfromtxt("ingunn_profile_pressure.dat").T
tdat = np.genfromtxt("ingunn_profile_temperature.dat").T

P.subplot(111)
xx = np.logspace(np.log10(np.min(pdat[0])*1.0001), np.log10(np.max(pdat[0])*0.9999), 200)
IIp = scipy.interpolate.interp1d(pdat[0], pdat[1], kind='linear')
IIt = scipy.interpolate.interp1d(tdat[0], tdat[1], kind='linear')

P.plot(xx, IIp(xx)/CL.P(xx*R500), 'r-', label="P(r)_Ingunn / P(r)_Phil")
P.plot(xx, IIt(xx)/Tloken(xx,R500), 'b-', label="T(r)_Ingunn / T(r)_Phil")

#P.yscale('log')
P.xscale('log')
P.xlabel("r/R500")
P.grid(True)
P.legend(loc="center right", prop={'size':'small'})

P.show()
