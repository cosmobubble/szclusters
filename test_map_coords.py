#!/usr/bin/python
"""Clean-up the MCXC catalogue by applying various cuts."""

import sys, time
sys.path.append("/home/phil/postgrad/lib") # HealPy path
import numpy as np
import pylab as P
import healpy as hp
import catalogue_cuts as cuts
import coordinates, catalogues
tstart = time.time()


################################################################################
# Settings and file names
################################################################################

opts = {
	'USE_PRECOMPUTED_DISTS':True, # Whether to compute ang. dists. or use saved versions
	
	'ANG_SIZE_CUT':0.63, # in degrees
	'EXCLUSION_RADIUS':5., # Cut out anything within E_R * r500 of a flagged cluster
	'DUP_ANG_CUT':0.20, # Angular distance cut, in degrees, for duplicate candidates
	'DUP_Z_CUT':0.0051, # Redshift difference for duplicates
	'RADIUS_IDENTIFY':1., # Radius within which ptsrcs identified as cls. (in theta500)
	'RADIUS_CONTAM':5., # Radius within which ptsrcs identified as contam. (in theta500)
	
	'NSIDE':512, # Nside of output Healpix mask
	'PS_MASK_RAD':0.6 * np.pi/180., # Point source mask radius, in rad. (0.6deg)
	
	'FNAME_CL_TRIMMED':"data/MCXC_trimmed.dat", # Fully cleaned MCXC catalogue
	'FNAME_CL_DUPONLY':"data/MCXC_nodups.dat", # MCXC cleaned only of duplicates
	'FNAME_MASK':"data/WMAP_cleaned_ptsrc_mask.fits", # Cleaned ptsrc mask
	'FNAME_WMAP_GALMASK':"data/wmap_point_source_catalog_mask_r9_7yr_v4.fits", # Galaxy mask
	'FNAME_PRECOMPUTED_PTCL':"data/precomputed_ptsrc_angular_distances",
	'FNAME_PRECOMPUTED_CLCL':"data/precomputed_angular_distances",
	}


# Load point source catalogue
catPS = catalogues.CatalogueWmapPointSource(galcoords=True)


# MAKE POINT SOURCE CATALOGUE MAP
# Pointings to be masked, in radians, with angular shift applied
# 0 < l < 2pi; -pi/2 < b < pi/2, but need b = [0, pi] for Healpix
new_l = catPS.l * np.pi/180.
new_b = 0.5*np.pi - (catPS.b * np.pi/180.)

print "No. point sources:", new_b.size

print "\n>>>>", np.max(new_b)*180./np.pi, np.min(new_b)*180./np.pi
print ">>>>", np.max(catPS.b), np.min(catPS.b), "\n"


# Loop through point sources, masking them out on the map
hpmask = np.zeros(hp.nside2npix(opts['NSIDE']))
for i in range(new_l.size):
	# Get indices of pixels to be masked
	vec = hp.pixelfunc.ang2vec(theta=new_b[i], phi=new_l[i])
	idxs = hp.query_disc(opts['NSIDE'], vec, opts['PS_MASK_RAD'])
	hpmask[idxs] = 1.0 # Mask pixels (0=masked, 1=unmasked)


# LOAD WMAP/LAMBDA POINT SOURCE MAP
FNAME_WMAP_PTSRC_MASK = "data/wmap_temperature_source_mask_r9_7yr_v4.fits"
map_ptsrc = hp.read_map(FNAME_WMAP_PTSRC_MASK)
map_ptsrc = -1.*map_ptsrc + 1.

# ADD MAPS TOGETHER
sum_mask = 2.0*map_ptsrc + 1.0*hpmask
print np.unique(sum_mask)

# GET DIFFERENCE MASKS
mboth = np.where(sum_mask==3., 1., 0.)
mwmap = np.where(sum_mask==2., 1., 0.)
mme = np.where(sum_mask==1., 1., 0.)

# Display mask
hp.mollview(hpmask, title="Point source mask (me)")
#hp.mollview(sum_mask, title="Point source mask")
#hp.mollview(mboth, title="Point source mask (Both)")
#hp.mollview(mwmap, title="Point source mask (WMAP only)")
#hp.mollview(mme, title="Point source mask (Me only)")
hp.graticule()


# Timing information
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
