#!/usr/bin/python
"""Plot Planck and AMI cluster data (Table 6)."""

import numpy as np
import pylab as P
from scipy.optimize import leastsq

# Load data file
# (0) Name, (1) z, (2) SNR, (3) PlanckTheta500, (4) PlanckTheta500Err, 
# (5) PlanckY500, (6) PlanckY500Err, (7) AMITheta500, (8) AMITheta500Err, 
# (9) AMIY500, (10) AMIY500Err, (11) JointTheta500, (12) JointTheta500Err, 
# (13) JointY500, (14) JointY500Err
fname = "Planck_AMI_1204.1318_Table6.dat"
z, PTheta, PThetaErr, ATheta, AThetaErr = np.genfromtxt(fname, usecols=(1,3,4,7,8), delimiter=",").T
PY, PYErr, AY, AYErr = np.genfromtxt(fname, usecols=(5,6,9,10), delimiter=",").T

# Mask used to remove clusters which were not used in the Planck correlation 
# analysis in Figure 6 of arXiv:1204.1318v1 (see Fig. 5, no red x-ray arrow => False).
fig6mask = np.ma.make_mask([True, True, True, True, True, True, True, True, False, False, False])
PY = PY[fig6mask]
PYErr = PYErr[fig6mask]
AY = AY[fig6mask]
AYErr = AYErr[fig6mask]

def chi2(p, x, ex, y, ey):
	"""Chi-squared over 2 dimensions."""
	# epsilon = x - y = 0 (null hyp.)
	m = p[0]
	c = p[1]
	s2 = ex**2. + ey**2. # (y*ex)**2. + (x*ey)**2.
	csq = (m*x + c - y)**2. / s2
	return csq

def chi2_cfixed(p, x, ex, y, ey):
	"""Chi-squared over 2 dimensions."""
	# epsilon = x - y = 0 (null hyp.)
	m = p[0]
	s2 = ex**2. + ey**2. # (y*ex)**2. + (x*ey)**2.
	csq = (m*x - y)**2. / s2
	return csq

def pearson_r(x, ex, y, ey):
	"""Find Pearson correlation coefficient of sample."""
	ux = np.mean(x); uy = np.mean(y)
	r1 = sum(  (x - ux) * (y - uy)  )
	r2 = np.sqrt( sum((x - ux)**2.) * sum((y - uy)**2.) )
	return r1/r2

# Fit a line to the Theta500 data
p0 = np.array([1.0, 0.]) # m, c
p, stat = leastsq(chi2, p0, args=(PTheta, PThetaErr, ATheta, AThetaErr))
xx = np.linspace(0.,15., 200)
yy = p[0]*xx + p[1]

# Fit a line to the Y500 data
#q0 = np.array([1.0, 0.]) # m, c
q0 = np.array([1.2]) # m
q, stat = leastsq(chi2_cfixed, q0, args=(AY, AYErr, PY, PYErr))
xx2 = np.linspace(0.,40., 200)
#yy2 = q[0]*xx + q[1]
yy2 = q[0]*xx2
print q, stat
print "Pearson correlation r =", pearson_r(AY, AYErr, PY, PYErr)

"""
# Plot results
P.subplot(121)
P.errorbar(ATheta, PTheta, xerr=AThetaErr, yerr=PThetaErr, fmt="r.")
P.plot(xx, xx, 'k-')
P.plot(xx, yy, 'b--')
P.xlabel(r"AMI $\theta_{500}$")
P.ylabel(r"Planck $\theta_{500}$")
P.xlim((0.,15.))
P.ylim((0.,15.))
P.figtext(  0.2, 0.8, "y = "+str(round(p[0],2))+"x + "+str(round(p[1],2))  )
"""

# Check chi2
#mm = np.linspace(0., 2., 100)
#x2 = [sum(chi2_cfixed([mval], AY, AYErr, PY, PYErr)) for mval in mm]

P.subplot(111)
P.errorbar(AY, PY, xerr=AYErr, yerr=PYErr, fmt="r.")
P.plot(xx2, xx2, 'k-')
P.plot(xx2, yy2, 'b--')
P.xlabel(r"AMI Y")
P.ylabel(r"Planck Y")
P.xlim((0.,40.))
P.ylim((0.,40.))

P.show()
