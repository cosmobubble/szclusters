#!/usr/bin/python
"""Load the reprocessed data from the MCXC catalogue.
   (Catalogue processed with process_mcxc.py)
"""

import numpy as np
import pylab as P
import astropysics.coords as coords
import pickle
import time
tstart = time.time()

PICKLE_FNAME = "data/find_duplicates.pickle"

# Load data
fname = "data/MCXC_processed.dat"
ids, z, l, b, da, th500, r500, m500, l500, scale = np.genfromtxt(fname, delimiter="\t").T

# Load names
fname2 = "data/MCXC_names.txt"
n1, n2, n3 = np.genfromtxt(fname2, dtype="a", delimiter="\t", usecols=(1,2,3)).T


################################################################################
# Find distances between pairs
################################################################################

def dist2(l, b):
	"""Find distance between two points in degrees, in galactic coordinates."""
	
	# Get array of galactic coordinate objects
	c = [coords.coordsys.GalacticCoordinates(l[i], b[i]) for i in range(l.size)]
	
	# Get angular distance between pairs of coordinates
	# (only for i<j, to avoid calculating twice)
	NN = len(c)
	delta = [[(c[i] - c[j]).d for i in range(j+1,NN)] for j in range(0, NN)]
	return delta


# Find distances between all clusters (will take a while!)
# Save the data for quick access later
delta = dist2(l, b)
f = open(PICKLE_FNAME, 'w'); pickle.dump(delta, f); f.close()

# Access previously-saved distance data (~10x faster than recalculating, but still slow)
#f = open(PICKLE_FNAME, 'r'); delta = pickle.load(f); f.close()



################################################################################
# Find all pairs within some cut radius
################################################################################

# Search through data, finding distance pairings that are less than some cut
ANG_CUT = 0.20 # In degrees
Z_CUT = 0.0051 # Redshift difference

idxs = []
rej = []
# Loop through pairs, looking for close pairings
for i in range(len(delta)):
	for j in range(len(delta[i])):
		i1 = i; i2 = i + j + 1
		dz = z[i1] - z[i2]
		if delta[i][j] < ANG_CUT and abs(dz) < Z_CUT: # and j != 0:
			dM = (m500[i1] - m500[i2]) / m500[i1]
			dL = (l500[i1] - l500[i2]) / l500[i1]
			dS = (scale[i1] - scale[i2]) / scale[i1]
			print i1, "\t", i2, "\t", round(delta[i][j], 3), "\t", round(dz, 3), 
			#print "\t", round(dM, 3), "\t", round(dL, 3), "\t", round(dS, 3)
			print "\t<<", n3[i1], "||", n3[i2], ">>"
			idxs.append((i1, i2))
			rej.append(i2)

print "\nFound", len(idxs), "pairs."



################################################################################
# Remove duplicates and write out to file
################################################################################

"""
l2 = np.where(l > 180., l-360., l)
print len(l)
good_idxs = np.array([i not in rej for i in ids])
fmtstr = "%+10.5f\t%9.5f\t%6.4f\t%6.3f\t%9.6f\t%7.4f\t%7.4f   "
m = good_idxs
dat = np.column_stack((l2[m], b[m], z[m], scale[m], l500[m], m500[m], r500[m]))
np.savetxt("data/MCXC_gal_trimmed_dup.dat", dat, fmt=fmtstr, delimiter="\t")
"""


# Timing information
print "Run took", round(time.time() - tstart, 2), "sec."
