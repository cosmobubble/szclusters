#!/usr/bin/python
"""Display the WMAP point source mask."""

import sys
import numpy as np
import pylab as P
sys.path.append("/home/phil/postgrad/lib") # HealPy path
import healpy as hp

FNAME_WMAP_PTSRC_MASK = "data/wmap_temperature_source_mask_r9_7yr_v4.fits"
map_ptsrc = hp.read_map(FNAME_WMAP_PTSRC_MASK)

# Display mask
hp.mollview(map_ptsrc, title="WMAP original point source mask", norm='hist')
hp.graticule()

P.show()
