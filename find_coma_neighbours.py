#!/usr/bin/python
"""Find all clusters within 15xR_500 of Coma. Also check the amount of overlap 
   between Coma and Virgo.
"""

import numpy as np
import pylab as P
from astropysics.coords.coordsys import GalacticCoordinates
import time
tstart = time.time()


def dist(l1, b1, l2, b2):
	"""Find distance between two points in galactic coordinates."""
	c1 = GalacticCoordinates(l1, b1)
	angsep = [c1 - GalacticCoordinates(l2[i], b2[i]) for i in range(l2.size)]
	return np.array([abs(a.d) * np.pi/180. for a in angsep])


################################################################################
# Find clusters surrounding Coma and save catalogue
################################################################################

# Cut out anything within n*r500 of a flagged cluster
n = 15.

# Load data
fname = "data/MCXC_processed.dat"
ids, z, l, b, da, th500, r500, m500, l500, scale = np.genfromtxt(fname, delimiter="\t").T

# Identify Coma and Virgo, and print out their sizes to double-check
icoma = 942; ivirgo = 883
print "Coma:  idx =", icoma, " /  th500 =", round(th500[icoma]*180./np.pi, 2), "deg."
print "Virgo: idx =", ivirgo, " /  th500 =", round(th500[ivirgo]*180./np.pi, 2), "deg."

# Get angular distance between the two
ccoma = GalacticCoordinates(l[icoma], b[icoma])
cvirgo = GalacticCoordinates(l[ivirgo], b[ivirgo])
delta = (ccoma - cvirgo).d
print "Ang. Dist. (Coma->Virgo):", delta, "deg."
print "\t" + str(round(delta*np.pi/(th500[icoma]*180.), 1)) + " x theta_500 (Coma)"
print "\t" + str(round(delta*np.pi/(th500[ivirgo]*180.), 1)) + " x theta_500 (Virgo)"
print "\t" + str(round( delta*np.pi / (180.*(th500[icoma]+th500[ivirgo])) ,2)) + " x theta_500 (equal factors)"

# Make a list of clusters within N x theta_500 of Coma
d = dist(l[icoma], b[icoma], l, b)
i5 = np.where(d <= 5.*th500[icoma])[0]
i15 = np.where(d <= 15.*th500[icoma])[0]
print ""
print "Clusters within 5 x theta_500 (Coma):"
print i5.size, i5
print "\nClusters within 15 x theta_500 (Coma):"
print i15.size, i15


# Output catalogue data in format used by Commander code
# Need to rejig the range of gal. coord. "l", so it's between [-180, 180], 
# rather than [0, 360]. This should be done by taking any l > 180 and 
# subtracting 360
l2 = np.where(l > 180., l-360., l)

"""
# The template file I have (from H.K. Eriksen, 2012-05-07) appears to be in the 
# following format:
# l, b, z, scale, L500, M500, R500
fmtstr = "%+10.5f\t%9.5f\t%6.4f\t%6.3f\t%9.6f\t%7.4f\t%7.4f   "
m = i5
dat = np.column_stack((l2[m], b[m], z[m], scale[m], l500[m], m500[m], r500[m]))
np.savetxt("data/MCXC_Coma_5.dat", dat, fmt=fmtstr, delimiter="\t")

m = i15
dat = np.column_stack((l2[m], b[m], z[m], scale[m], l500[m], m500[m], r500[m]))
np.savetxt("data/MCXC_Coma_15.dat", dat, fmt=fmtstr, delimiter="\t")
"""

################################################################################
# Find random pointings which are free from clusters
################################################################################

# Define l, b range and flag radius nrad
lrange = (0., 360.)
brange = (-90., 90.)
nrad = 15.

goodl = []; goodb = []; goodmin = []
ii = -1; good = 0

for k in i15:
	print "-"*30
	found = False
	while not found:
		ii += 1
		
		# Get random coords
		ll = np.random.uniform(low=lrange[0], high=lrange[1])
		bb = np.random.uniform(low=brange[0], high=brange[1])
		cc = GalacticCoordinates(ll, bb)
		
		print "CLUSTER", k, "TRIAL ID:", ii
		print "\tl =", ll, " / b =", bb

		# Get angular separations
		angsep = [cc - GalacticCoordinates(l[i], b[i]) for i in range(l.size)]
		angsep = np.array([abs(a.d)*np.pi/180. for a in angsep])
	
		# Check if minimum distance is low enough
		print "\tMin. ang. sep.:", round(np.min(angsep/th500[k]), 3)
		
		#if np.min(angsep/th500) > nrad:
		if np.min(angsep) > nrad*th500[k]:
			goodl.append(ll); goodb.append(bb)
			goodmin.append( np.min(angsep)*180./np.pi )
			found = True
			print "\tACCEPTED"
		else:
			print "\tREJECTED"
		print ""

# Output results
print "ID\tl\tb\tmin(n*th_500)"
for i in range(len(goodl)):
	print i, goodl[i], goodb[i], goodmin[i]



# Timing information
print "Run took", round(time.time() - tstart, 2), "sec."
