
import numpy as np

dat1 = np.arange(0, 12)
dat1 *= 2

print dat1

# Use masking
badidxs = [0, 3, 4]
idxs = np.arange(0, dat1.size)

for val in badidxs: idxs = np.ma.masked_values(idxs, val, copy=False)
good_idx_mask = np.where(idxs.mask, False, True) # Invert mask

#print dat1[idx]
