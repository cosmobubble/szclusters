#!/usr/bin/python
"""kSZ temperature profile, as a function of radius of the cluster."""

import numpy as np
import pylab as P
import scipy.integrate
from arnaud_profile import ArnaudProfile
from catalogues import CatalogueREXCESS

def beta(x):
	# Beta profile, with beta=0.6
	beta = 0.6 # See Fig. 14, Komatsu and Seljak
	return (1. + x**2.)**(-1.5*beta)

def ne(x):
	# Empirical best fit ne(x)
	return (1. + x**(1.41))**(-(5.49 - 0.567)/1.41) * np.exp(0.12*5.49*x)

def nfw(x):
	# NFW profile
	return x**(-1.) * (1. + x)**(-2.)

def nloken(x, r500):
	# Rough (unnormalised) density profile, using Loken et al. 2002. T(r) 
	# profile and Anraud best-fit GNFW pressure profile.
	p = x**(-0.567) * (1. + x**(1.41))**(-(5.49 - 0.567)/1.41)
	T = 11.2 * (r500*0.7)**2. * ( 1. + 0.75*x)**(-1.6)
	return p/T

def Tloken(x, r500):
	return 11.2 * (r500*0.7)**2. * ( 1. + 0.75*x)**(-1.6)

# Calculate normalised kST T(th) profile
def integrand_beta(x, th, c500):
	# x = r/ r500, th = b/r500
	return beta(c500*x) * x / np.sqrt(x**2. - th**2.)

def integrand_ne(x, th, c500):
	return ne(c500*x) * x / np.sqrt(x**2. - th**2.)

def integrand_nfw(x, th, c500):
	return nfw(c500*x) * x / np.sqrt(x**2. - th**2.)

def integrand_loken(x, th, r500):
	return nloken(x, r500) * x / np.sqrt(x**2. - th**2.)


def y_nfw(bx, c500=1.0):
	y = map( lambda x: scipy.integrate.quad(integrand_nfw, x, 5., args=(x, c500))[0] , bx)
	return np.array(y)

def y_beta(bx, c500=1.0):
	y = map( lambda x: scipy.integrate.quad(integrand_beta, x, 5., args=(x, c500))[0] , bx)
	return np.array(y)

def y_loken(bx, r500=1.0):
	y = map( lambda x: scipy.integrate.quad(integrand_loken, x, 5., args=(x, r500))[0] , bx)
	return np.array(y)





# Load best-fit Arnaud profile, for some cluster
cat = CatalogueREXCESS()
prof = ArnaudProfile(cat.params[0])
prof.profile_arnaud_bestfit()


#ysz = map( lambda _th: scipy.integrate.quad(integrand_sz, _th, 5.0, args=(_th,))[0] , th )
#th = np.logspace(-2.5, np.log10(4.9), 50)
th = np.linspace(0.001, 4.999, 200) # th = b / r500, b is impact param.

"""
P.subplot(111)
y1 = y_loken(th, cat.r500[0])
y2 = y_nfw(th, 1.0)
y3 = y_beta(th, 1.0)
P.plot(th, y1/y1[0], 'k-', label="Arnaud/Loken")
P.plot(th, y2/y2[0], 'r-', label="NFW")
P.plot(th, y3/y3[0], 'b-', label="Beta")
P.ylim((1e-4, 1.1))
P.legend(loc='lower left')
"""
"""
P.plot(th, y_nfw(th, 0.5), 'g-')
P.plot(th, y_nfw(th, 1.0), 'b-')
P.plot(th, y_nfw(th, 2.0), 'r-')

P.plot(th, y_beta(th, 0.5), 'g--')
P.plot(th, y_beta(th, 1.0), 'b--')
P.plot(th, y_beta(th, 2.0), 'r--')
"""
"""
#P.plot(th, nfw(th), 'k--')
#P.plot(th, np.array(ysz)/ysz[0], 'k-')
#P.xscale('log')
P.yscale('log')
P.ylabel("Integrated LOS density")
P.xlabel("$x_b$")
"""


P.subplot(111)
P.plot(th, Tloken(th, cat.r500[0]), 'k-')
#P.yscale('log')
P.ylabel("T(r)")
P.xlabel("r/R500")

P.show()
