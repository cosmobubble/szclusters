#!/usr/bin/python
"""Plot the angular diameter of the clusters in the MCXC sample."""

import numpy as np
import pylab as P
import catalogues

# Load MCXC catalogue
cat = catalogues.CatalogueMCXC()

# Plot histogram
"""
P.subplot(111)
P.hist(cat.th500*180./np.pi, bins=100, range=(0., 3.), fc='r')
P.xlabel(r"$\theta_{500}$ (deg)")
P.ylabel("Count")
"""

P.subplot(111)
P.xlabel(r"$\theta_{500}$ (deg)")
P.ylabel("Count")
P.hist(cat.th500*180./np.pi, bins=25, range=(0.5, 3.), fc='r')
P.ylim((0., 10.))

P.show()
