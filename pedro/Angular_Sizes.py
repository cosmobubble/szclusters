#!/usr/bin/python

import scipy.integrate
import scipy.interpolate
import numpy as np
import pylab as P
import math


Om=0.3
Ol=0.7
h=0.72
DH=3000/h
Nclust=1743
zvec=np.ones(Nclust)
davec=np.ones(Nclust)
scalevec=np.ones(Nclust)
angsizevec=np.ones(Nclust)
sizevec=np.ones(Nclust)


def da(Om,Ol,z):
    a=1./(1+z)
    da1=scipy.integrate.quad(daint,a,1,args=(Om,Ol))[0]
    return DH*da1/(1.+z)

def daint(a,Om,Ol):
    H=math.sqrt(Om/a**3+Ol)
    daint=1./a**2/H
    return daint

clusdata = np.genfromtxt("MCXC_Vizier.txt")

print np.shape(clusdata.T) # T means transpose

zvec=clusdata[:,2]
scalevec=clusdata[:,3]
sizevec=clusdata[:,6]
angsizevec=1000.*sizevec/60./scalevec

imin=0
imax=Nclust
i=imin
while i <imax:
    zz=zvec[i]
    davec[i]=da(Om,Ol,zz)
    i+=1


P.subplot(111)
P.yscale('log')
#P.xscale('log')
#P.ylim(0.3, 1.2) 
#P.xlim(0.,1.)
#P.plot(zvec,fvec)
#P.plot(zvec,fvec+dfvec)
#P.plot(zvec,fvec-dfvec)
P.plot(zvec,angsizevec,"o")
P.show()

print zvec # Prints 1st row

print np.shape(zvec)
print np.shape(davec)
#print clusdata.T[1] # Prints 2nd column


np.savetxt("MCXC_Vizier_ang.txt", clusdata )


