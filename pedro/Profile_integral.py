#!/usr/bin/python

import scipy.integrate
import scipy.interpolate
import numpy as np
import pylab as P
import math



def dens(z,R):
    r=(z**2.+R**2.)**(0.5)
    alpha=1.0510
    beta=5.4905
    gamma=0.3081
    gamma2=(beta-gamma)/alpha
    dd=r**(-gamma)*(1+r**alpha)**(-gamma2)
    return dd

def dens2(z,R):
    r=(z**2.+R**2.)**(0.5)
    alpha=1.0510
    beta=5.4905
    gamma=0.3081
    gamma2=(beta-gamma)/alpha
    dd=r**(-gamma)*(1+r**alpha)**(-gamma2)
    return dd

def projdens(R):
    zlim=20.
    pp=scipy.integrate.quad(dens,-zlim,zlim,args=(R,))[0]
    #    pp=dens(0.01,R)
    return pp

def projdensfit(R):
    alpha=1.0510
    beta=5.4905
    gamma=0.3081
    gamma2=(beta-gamma)/alpha
    dd=R**(-beta)
    return dd


nrmax=100
Rvec = np.logspace(-3,3,nrmax)
pdvec = np.zeros(nrmax)
pdvecfit = np.zeros(nrmax)

imin=0
imax=nrmax
i=imin
while i <imax:
    rr=Rvec[i]
    pdvec[i]=projdens(rr)
    pdvecfit[i]=dens2(0.14,0.8*rr)
    i+=1

#pdvec= projdens(Rvec)
P.subplot(111)
P.yscale('log')
P.xscale('log')
#P.ylim(0.3, 1.2) 
#P.xlim(-2,2)
P.plot(Rvec,pdvec)
P.plot(Rvec,pdvecfit)
#P.plot(zvec,fvec+dfvec)
#P.plot(zvec,fvec-dfvec)
#P.errorbar(zvec,fvec,yerr=dfvec,fmt="o")
P.show()

