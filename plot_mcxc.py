#!/usr/bin/python
"""Load the reprocessed data from the MCXC catalogue.
   (Catalogue processed with process_mcxc.py)
"""

import numpy as np
import pylab as P
import matplotlib.projections
import matplotlib.cm
import time
tstart = time.time()

fname = "data/MCXC_processed.dat"
ids, z, l, b, da, th500, r500, m500, l500 = np.genfromtxt(fname, delimiter="\t").T



# Get relative sizes
zmax = max(z); zmin = min(z)
frac = (z-zmin)/(zmax - zmin)
sz = 1.0 + 24.*frac

# Get colours
colours = matplotlib.cm.get_cmap("cool")
cols = colours(frac)
for i in range(len(cols)): cols[i][3] = 0.5

# Plot information
ax = P.subplot(111, projection="mollweide")
for i in range(ids.size):
	P.plot(l[i]*(np.pi/180.) - np.pi, b[i]*(np.pi/180.), marker=".", mec=cols[i], mfc=cols[i], ms=sz[i])

for label in ax.get_xticklabels(): label.set_visible(False)
for label in ax.get_yticklabels(): label.set_visible(False)

# Output run time
print "Run took", round(time.time() - tstart, 2), "sec."
P.show()
