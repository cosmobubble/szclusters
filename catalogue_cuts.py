#!/usr/bin/python
"""Functions for applying cuts to various catalogues."""

import numpy as np

################################################################################
# Convenience functions
################################################################################

def idx_to_mask(idxs, N):
	"""Convert an array of indices into a [True/False] mask. Needs the number 
	   of elements in the final array."""
	idxs = np.array(idxs).astype("i") # Check that it's a numpy integer array
	mask = np.array([True]*N)
	mask[idxs] = False
	return mask

def mask_to_masked_idxs(mask):
	"""Convert a mask into an array of masked indices."""
	idxs = np.arange(0, mask.size)
	return idxs[-mask]

def mask_to_unmasked_idxs(mask):
	"""Convert a mask into an array of unmasked indices."""
	idxs = np.arange(0, mask.size)
	return idxs[mask]


################################################################################
# Functions for applying cuts
################################################################################

def mask_large_clusters(catCL, opts):
	"""Get mask for clusters which have large angular sizes."""
	return catCL.th500 < opts['ANG_SIZE_CUT'] * np.pi/180.


# FIXME: Modify these to get reports of which clusters are neighbours/duplicates etc.

def mask_large_cluster_neighbours(angdists, catCL, opts):
	"""Get mask for clusters which lie within some ang. radius of large clusters."""
	
	# Get masked clusters
	masked = mask_large_clusters(catCL, opts)
	masked_idxs = mask_to_masked_idxs(masked)
	
	# Loop through masked clusters and find their nearest neighbours
	idx_neighbours = []
	for i in masked_idxs:
		# Test distances of clusters in relation to this masked cluster
		neighb = np.where(angdists[i]*np.pi/180. < opts['EXCLUSION_RADIUS']*catCL.th500[i])
		
		# Add indexes of identified neighbours
		for ii in catCL.ids[ neighb[0] ]:
			if ii not in idx_neighbours and ii != i:
				idx_neighbours.append(ii)
				
	return idx_to_mask(idx_neighbours, masked.size)


def mask_duplicate_clusters(angdists, catCL, opts):
	"""Mask clusters which appear to be duplicate entries/close mergers."""
	
	N = catCL.ids.size
	
	# Search through data, finding distance pairings that are less than some cut
	idx_dups = []
	# Search through clusters such that j>i (to avoid picking up pairs twice)
	for i in range(N):
		for j in range(i+1, N):
			if (  angdists[i][j] < opts['DUP_ANG_CUT'] 
			and   abs(catCL.z[i]-catCL.z[j]) < opts['DUP_Z_CUT']   ):
				idx_dups.append(j)
				#pairs.append( (i,j) )
	
	# N.B. idx_dups may have the same index entered multiple times (safe to ignore)
	return idx_to_mask(idx_dups, N)
	

def mask_ptsrcs_are_clusters(angdists, catCL, catPS, opts):
	"""Mask pointsources identified as actually being clusters."""
	
	Nps = catPS.l.size
	Ncl = catCL.l.size
	
	# Get theta500 for each cluster pointing, and fold into array
	th500 = np.array( [catCL.th500 * 180./np.pi] * Nps ).T
	
	# Get mask of pointsources that are within some radius of cluster pointing
	mask2d = np.where(angdists <= opts['RADIUS_IDENTIFY'] * th500, True, False)

	# Get i,j indices of the elements that were cut, by constructing 2d array 
	# of indices, dividing them by num. columns, then finding whole part or 
	# remainder
	idxs = np.arange(th500.size).reshape(np.shape(angdists))
	idx_cl = np.floor(idxs[mask2d] / Nps).astype("i") # Cluster indexes
	idx_ps = np.mod(idxs[mask2d], Nps).astype("i") # Ptsrc indexes
	# identified_pairs = [(idx_cl[k], idx_ps[k]) for k in range(Ncl)]
	
	# Construct mask for pointsources
	mask = idx_to_mask(idx_ps, Nps)
	return mask


def mask_ptsrc_contam_clusters(angdists, catCL, catPS, opts):
	"""Mask clusters that are contaminated by pointsources."""
	
	Nps = catPS.l.size
	Ncl = catCL.l.size
	
	# Get theta500 for each cluster pointing, and fold into array
	th500 = np.array( [catCL.th500 * 180./np.pi] * Nps ).T
	
	# Get mask of pointsources that are within some radius of cluster pointing
	mask2d_id = np.where(angdists <= opts['RADIUS_IDENTIFY'] * th500, True, False)
	mask2d_contam = np.where(angdists <= opts['RADIUS_CONTAM'] * th500, True, False)
	# Only find sources > RADIUS_IDENTIFY and < RADIUS_CONTAM
	mask2d = np.logical_xor(mask2d_id, mask2d_contam)

	# Get i,j indices of the elements that were cut, by constructing 2d array 
	# of indices, dividing them by num. columns, then finding whole part or 
	# remainder
	idxs = np.arange(th500.size).reshape(np.shape(angdists))
	idx_cl = np.floor(idxs[mask2d] / Nps).astype("i") # Cluster indexes
	idx_ps = np.mod(idxs[mask2d], Nps).astype("i") # Ptsrc indexes
	# identified_pairs = [(idx_cl[k], idx_ps[k]) for k in range(Ncl)]
	
	# Construct mask for clusters
	mask = idx_to_mask(idx_cl, Ncl)
	return mask


