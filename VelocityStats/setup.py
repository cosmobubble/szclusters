#/usr/bin/python

from distutils.core import setup, Extension

VelocityStats = Extension('VelocityStats',
                    sources=['vpair.c'],
                    include_dirs=['/usr/share/pyshared/numpy/core/include/numpy'],
                    extra_compile_args=['-O3'])

setup (name = 'VelocityStats',
       version = '0.1',
       description = 'Calculate pairwise velocity statistics.',
       ext_modules = [VelocityStats]  )
