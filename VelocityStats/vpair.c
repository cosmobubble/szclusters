////////////////////////////////////////////////////////////////////////
// Calculate pairwise momenta for cluster catalogues.
////////////////////////////////////////////////////////////////////////

#include <Python.h>
#include "arrayobject.h"
#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"


////////////////////////////////////////////////////////////////////////
// Helper functions, based on those by Lou Pecora.
// See: http://www.scipy.org/Cookbook/C_Extensions/NumPy_arrays
////////////////////////////////////////////////////////////////////////

/* DEPRECATED: Issues with memory locations not being contiguous for 
 * individual arrays (multiple arrays get mixed in...).
double *pyvector_to_Carrayptrs(PyArrayObject *arr)  {
	// Assumes PyArray is contiguous in memory.
	return (double *) arr->data;  // Pointer to arr data as double
}

int *pyvector_to_Carrayptrs_int(PyArrayObject *arr)  {
	// Assumes PyArray is contiguous in memory.
	return (int *) arr->data;  // Pointer to arr data as int
}
*/

int not_dblvec(PyArrayObject *vec){
	// Check that PyArrayObject is a double (Float) type and a vector
	if (vec->descr->type_num != NPY_DOUBLE || vec->nd != 1){
		PyErr_SetString(PyExc_ValueError,
		 "ERROR: not_doublevector(): Not a vector of doubles.");
		return 1;}
	return 0;
}


////////////////////////////////////////////////////////////////////////
// Functions to calculate pairwise velocity statistics
////////////////////////////////////////////////////////////////////////

static PyObject* pairwise_momenta(PyObject *self, PyObject *args){
	// Calculate the pairwise momenta for every pair of clusters
	// WARNING: NBINS needs to be sufficiently large, otherwise an 
	// overflow can happen.
	
	// Pointers to arrays, and convenience variables
	PyArrayObject *pyM, *pyx, *pyy, *pyz, *pyvx, *pyvy, *pyvz;
	double *Mi, *xi, *yi, *zi, *vxi, *vyi, *vzi;
	double *Mj, *xj, *yj, *zj, *vxj, *vyj, *vzj;
	int i, j, k, NBINS;
	double pij, rij, dr;
	
	// Parse the list of arguments. 'O!' is a Python object;
	// for each object, it needs a Python type to expect (PyArray)
	// and then a C pointer
	if (!PyArg_ParseTuple( args, "idO!O!O!O!O!O!O!",
										 &NBINS, &dr,
										 &PyArray_Type, &pyM,
										 &PyArray_Type, &pyx,
										 &PyArray_Type, &pyy,
										 &PyArray_Type, &pyz,
										 &PyArray_Type, &pyvx,
										 &PyArray_Type, &pyvy,
										 &PyArray_Type, &pyvz ) )
		return NULL; // Return NULL (error condition) if args are wrong
	
	// Test array types
	if (not_dblvec(pyM)) return NULL; if (not_dblvec(pyx)) return NULL;
	if (not_dblvec(pyy)) return NULL; if (not_dblvec(pyz)) return NULL;
	if (not_dblvec(pyvx)) return NULL; if (not_dblvec(pyvy)) return NULL;
	if (not_dblvec(pyvz)) return NULL;
	
	// Get array length (NOTE: Only tests one array for length!)
	// Also, set bin properties
	int N = pyM->dimensions[0];
	
	// Initialise new NDArrays for storing final values, and get C versions
	// PyArray_SimpleNew(no. dims, array of dim. sizes, type)
	int dims[1]; dims[0] = NBINS; // Dimensions of array
	PyArrayObject *py_pbin, *py_Nbin;
	py_pbin = (PyArrayObject *) PyArray_SimpleNew(1, dims, NPY_DOUBLE);
	py_Nbin = (PyArrayObject *) PyArray_SimpleNew(1, dims, NPY_INT);
	
	// Convert to C arrays and initialise to zero
	for(i=0; i<NBINS; i++){
		*(double*)PyArray_GETPTR1(py_pbin, i) = 0.0;
		*(int*)PyArray_GETPTR1(py_Nbin, i) = 0;
	} // end init
	
	////////////////////////////////////////////////////////////////////
	// Do the pairwise momentum calculation
	////////////////////////////////////////////////////////////////////
	
	// Loop through all pairs of clusters
	for(i=0; i<N; i++){
	 
	 // Get i values
	 Mi = (double*)PyArray_GETPTR1(pyM, i);
	 xi = (double*)PyArray_GETPTR1(pyx, i);
	 yi = (double*)PyArray_GETPTR1(pyy, i);
	 zi = (double*)PyArray_GETPTR1(pyz, i);
	 vxi = (double*)PyArray_GETPTR1(pyvx, i);
	 vyi = (double*)PyArray_GETPTR1(pyvy, i);
	 vzi = (double*)PyArray_GETPTR1(pyvz, i);
	 
	 for(j=0; j<i; j++){
	   
	   // Get j values
	   Mj = (double*)PyArray_GETPTR1(pyM, j);
	   xj = (double*)PyArray_GETPTR1(pyx, j);
	   yj = (double*)PyArray_GETPTR1(pyy, j);
	   zj = (double*)PyArray_GETPTR1(pyz, j);
	   vxj = (double*)PyArray_GETPTR1(pyvx, j);
	   vyj = (double*)PyArray_GETPTR1(pyvy, j);
	   vzj = (double*)PyArray_GETPTR1(pyvz, j);
	   
	   // Calculate separation distance
	   rij =  (*xi - *xj) * (*xi - *xj)  // dx^2
			+ (*yi - *yj) * (*yi - *yj)  // dy^2
			+ (*zi - *zj) * (*zi - *zj); // dz^2
	   rij = sqrt(rij);
	   
	   // Calculate pairwise momenta
	   pij =  (*Mi * *vxi - *Mj * *vxj ) * (*xi - *xj)
			+ (*Mi * *vyi - *Mj * *vyj ) * (*yi - *yj)
			+ (*Mi * *vzi - *Mj * *vzj ) * (*zi - *zj);
	   pij /= rij; // Dotted with unit vector
	   
	   // Add data to bin
	   // WARNING: Overflow risk. Should make sure that k < NBINS
	   k = (int)(rij / dr);
	   *(double*)PyArray_GETPTR1(py_pbin, k) += pij;
	   *(int*)PyArray_GETPTR1(py_Nbin, k) += 1;
	   
	 } // end j loop
	} // end i loop
	
	// Return pbin and Nbin, and let a Python handler fn. do the rest
	return Py_BuildValue("OO", py_pbin, py_Nbin);
}


static PyObject* pairwise_momenta_los(PyObject *self, PyObject *args){
	// Calculate the pairwise momenta (along the LOS) for every pair of 
	// clusters. NOTE: The input position arrays should be with respect 
	// to the observer's location!
	
	// Pointers to arrays, and convenience variables
	PyArrayObject *pyM, *pyx, *pyy, *pyz, *pyvx, *pyvy, *pyvz;
	double *Mi, *xi, *yi, *zi, *vxi, *vyi, *vzi;
	double *Mj, *xj, *yj, *zj, *vxj, *vyj, *vzj;
	int i, j, k, NBINS;
	double ri, rj, pri, prj, costheta, cij, rij, dr;
	
	// Parse the list of arguments. 'O!' is a Python object;
	// for each object, it needs a Python type to expect (PyArray)
	// and then a C pointer
	if (!PyArg_ParseTuple( args, "idO!O!O!O!O!O!O!",
										 &NBINS, &dr,
										 &PyArray_Type, &pyM,
										 &PyArray_Type, &pyx,
										 &PyArray_Type, &pyy,
										 &PyArray_Type, &pyz,
										 &PyArray_Type, &pyvx,
										 &PyArray_Type, &pyvy,
										 &PyArray_Type, &pyvz ) )
		return NULL; // Return NULL (error condition) if args are wrong
	
	// Test array types
	if (not_dblvec(pyM)) return NULL; if (not_dblvec(pyx)) return NULL;
	if (not_dblvec(pyy)) return NULL; if (not_dblvec(pyz)) return NULL;
	if (not_dblvec(pyvx)) return NULL; if (not_dblvec(pyvy)) return NULL;
	if (not_dblvec(pyvz)) return NULL;
	
	/*
	// Convert NDArray types into Python C arrays
	M = pyvector_to_Carrayptrs(pyM);
	x = pyvector_to_Carrayptrs(pyx); y = pyvector_to_Carrayptrs(pyy);
	z = pyvector_to_Carrayptrs(pyz); vx = pyvector_to_Carrayptrs(pyvx);
	vy = pyvector_to_Carrayptrs(pyvy); vz = pyvector_to_Carrayptrs(pyvz);
	*/
	
	// Get array length (NOTE: Only tests one array for length!)
	// Also, set bin properties
	int N = pyM->dimensions[0];
	
	// Initialise new NDArrays for storing final values, and get C versions
	// PyArray_SimpleNew(no. dims, array of dim. sizes, type)
	int dims[1]; dims[0] = NBINS; // Dimensions of array
	PyArrayObject *py_ppair_num, *py_ppair_denom, *py_ppair, *py_Nbin;
	py_ppair_num = (PyArrayObject *) PyArray_SimpleNew(1, dims, NPY_DOUBLE);
	py_ppair_denom = (PyArrayObject *) PyArray_SimpleNew(1, dims, NPY_DOUBLE);
	py_ppair = (PyArrayObject *) PyArray_SimpleNew(1, dims, NPY_DOUBLE);
	py_Nbin = (PyArrayObject *) PyArray_SimpleNew(1, dims, NPY_INT);
	
	/*
	// Convert to C arrays and initialise to zero
	double *ppair_num = pyvector_to_Carrayptrs(py_ppair_num);
	double *ppair_denom = pyvector_to_Carrayptrs(py_ppair_denom);
	double *ppair = pyvector_to_Carrayptrs(py_ppair);
	int *Nbin = pyvector_to_Carrayptrs_int(py_Nbin);
	*/
	for(i=0; i<NBINS; i++){
		*(double*)PyArray_GETPTR1(py_ppair_num, i) = 0.0;
		*(double*)PyArray_GETPTR1(py_ppair_denom, i) = 0.0;
		*(int*)PyArray_GETPTR1(py_Nbin, i) = 0;
		//*(ppair_num+i) = 0.0;
		//*(ppair_denom+i) = 0.0;
		//*(Nbin+i) = 0;
	} // end init
	
	////////////////////////////////////////////////////////////////////
	// Do the LOS pairwise momentum calculation
	////////////////////////////////////////////////////////////////////
	
	// Loop through all pairs
	for(j=0; j<N; j++){
	 
	 // Get j values
	 Mj = (double*)PyArray_GETPTR1(pyM, j);
	 xj = (double*)PyArray_GETPTR1(pyx, j);
	 yj = (double*)PyArray_GETPTR1(pyy, j);
	 zj = (double*)PyArray_GETPTR1(pyz, j);
	 vxj = (double*)PyArray_GETPTR1(pyvx, j);
	 vyj = (double*)PyArray_GETPTR1(pyvy, j);
	 vzj = (double*)PyArray_GETPTR1(pyvz, j);
	
	 // LOS momentum dot product (j)
	 rj = sqrt( *xj * *xj + *yj * *yj + *zj * *zj );
	 prj = ( *vxj * *xj + *vyj * *yj + *vzj * *zj ) * *Mj / rj;
	 
	 // Begin i loop
	 for(i=0; i<j; i++){
	    
	    // Get i values
		Mi = (double*)PyArray_GETPTR1(pyM, i);
		xi = (double*)PyArray_GETPTR1(pyx, i);
		yi = (double*)PyArray_GETPTR1(pyy, i);
		zi = (double*)PyArray_GETPTR1(pyz, i);
		vxi = (double*)PyArray_GETPTR1(pyvx, i);
		vyi = (double*)PyArray_GETPTR1(pyvy, i);
		vzi = (double*)PyArray_GETPTR1(pyvz, i);
	   
	   // LOS momentum dot product (i)
	   ri = sqrt( *xi * *xi + *yi * *yi + *zi * *zi );
	   pri = ( *vxi * *xi + *vyi * *yi + *vzi * *zi ) * *Mi / ri;
	   
	   // Angle between LOS vectors
	   costheta = ( *xi * *xj + *yi * *yj + *zi * *zj ) / (ri*rj);
	   
	   // Projection weighting factor
	   cij =  0.5 * (ri - rj) * (1. + costheta)
			/ sqrt(ri*ri + rj*rj - 2.*ri*rj*costheta);
	   
	   // Calculate separation distance
	   rij =   (*xi-*xj)*(*xi-*xj)
			 + (*yi-*yj)*(*yi-*yj)
			 + (*zi-*zj)*(*zi-*zj);
	   rij = sqrt(rij);
	   
	   // Add data to appropriate bin
	   k = (int)(rij / dr);
	   *(double*)PyArray_GETPTR1(py_ppair_num, k) += (pri - prj) * cij;
	   *(double*)PyArray_GETPTR1(py_ppair_denom, k) += cij*cij;
	   *(int*)PyArray_GETPTR1(py_Nbin, k) += 1;
	   //*(ppair_num+k) += (pri - prj) * cij;
	   //*(ppair_denom+k) += cij*cij;
	   //*(Nbin+k) += 1;
	   
	 } // end j loop
	} // end i loop
	
	// Calculate final value
	for(i=0; i<NBINS; i++){
		*(double*)PyArray_GETPTR1(py_ppair, i) = 
			  *(double*)PyArray_GETPTR1(py_ppair_num, i)
			/ *(double*)PyArray_GETPTR1(py_ppair_denom, i);
		//*(ppair+i) = *(ppair_num+i) / *(ppair_denom+i);
	}
	
	// Return pbin and Nbin, and let a Python handler fn. do the rest
	return Py_BuildValue("OO", py_ppair, py_Nbin);
}


////////////////////////////////////////////////////////////////////////
// Initialisation code
////////////////////////////////////////////////////////////////////////

static PyMethodDef vstats_methods[] = {
	// Bind Python function names to our C functions
    {"pairwise_momenta", pairwise_momenta, METH_VARARGS},
    {"pairwise_momenta_los", pairwise_momenta_los, METH_VARARGS},
    {NULL, NULL}
};

void initVelocityStats(void){
    (void) Py_InitModule("VelocityStats", vstats_methods);
    import_array();
}
